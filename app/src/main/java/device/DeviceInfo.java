package device;

public class DeviceInfo {

    private String dev_name = "";
    private String dev_uuid = "";

    public String getDevName() {
        return dev_name;
    }

    public void setDevName(String inName) {
        dev_name = inName;
    }

    public String getDevUUID() {
        return dev_uuid;
    }

    public void setDevUUID(String inUUID) {
        dev_uuid = inUUID;
    }
}
