package network;


import com.solajet.tcskiosk.tcskiosk.BuildConfig;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import interfaces.CardConnectApi;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import utils.Constant;


public class RestApi {

    HttpLoggingInterceptor logg= new HttpLoggingInterceptor();
    // set your desired log level




    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();


    private static Retrofit.Builder retrofitBuilder = new Retrofit
            .Builder()
            .baseUrl(Constant.BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create());


    //for the update of the app
    private static Retrofit.Builder retrofitBuilderForUpdate = new Retrofit
            .Builder()
            .baseUrl(Constant.BASE_URL_UPDATE)
            .addConverterFactory(JacksonConverterFactory.create());


//    public static <S> S createService(Class<S> aClass){
//        Retrofit retrofit = retrofitBuilder.client(httpClient.build()).build();
//        return retrofit.create(aClass);
//    }


    public static <S> S createService(Class<S> aClass) {
        httpClient.sslSocketFactory(getSSLSocketFactory());
        httpClient.hostnameVerifier(new HostnameVerifier() {

            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        httpClient.addInterceptor(new Interceptor() {

            @Override
            public Response intercept(Chain chain) throws IOException {

                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .header(Constant.AccessToken, Constant.AccessToken_Value)
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = retrofitBuilder.client(httpClient.build()).build();
        return retrofit.create(aClass);
    }

    private static SSLSocketFactory getSSLSocketFactory() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            return sslSocketFactory;
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            return null;
        }

    }


    //for the update api

    public static <S> S createServiceForUpdate(Class<S> aClass) {

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request original = chain.request();
                Request.Builder requestBuilder =
                        original.newBuilder()
                                .method(original.method(), original.body());

                Request request = requestBuilder.build();
                Response response = chain.proceed(request);

                return response;


            }
        });

        Retrofit retrofit = retrofitBuilderForUpdate.client(httpClient.build()).build();
        return retrofit.create(aClass);
    }

    public static CardConnectApi createCardConnectService() {

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            httpClient.addInterceptor(loggingInterceptor);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create())
                .baseUrl(Constant.CARD_CONNECT_BASE_URL)
                .client(httpClient.build())
                .build();

        return retrofit.create(CardConnectApi.class);
    }

    public static RequestBody getRequestBody(String params) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), params);
    }

}
