package com.solajet.tcskiosk.activities;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.TouchScreenTextView;
import database.DbHelper;
import interfaces.ApiInterface;
import models.EmployeeLogin.EmployeeLoginBean;
import models.EmployeeLogin.EmployeeMassageCountBean;
import models.StartMassage.StartMassageBean;
import models.SubAdminLogin.SubAdminLoginModel;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;
import utils.NetworkConnectionIndicator;


public class ManagerLoginActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    @BindView(R.id.et_manager_id)
    EditText etManagerId;
    @BindView(R.id.et_passcode)
    EditText etPasscode;
    @BindView(R.id.iv_manager_id)
    ImageView ivMemberId;
    @BindView(R.id.rootView)
    RelativeLayout rootView;
    @BindView(R.id.iv_passcode)
    ImageView ivPasscode;
    @BindView(R.id.toolbar_title)
    TouchScreenTextView toolbarTitle;
    @BindView(R.id.iv_toolbar_left_icon)
    ImageView ivToolbarLeftIcon;
    @BindView(R.id.iv_cross)
    ImageView ivCross;
    @BindView(R.id.tv_login)
    TouchScreenTextView tvLogin;
    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    Resources res;
    private DbHelper dbHelper;
    private NetworkConnectionIndicator networkConnectionIndicator;
    private AppUtils appUtils;
    private ArrayList<EmployeeLoginBean> employeeList, employeeListResponse;
    private ArrayList<EmployeeMassageCountBean> employeeMassageCountList;
    private StartMassageBean startMassagemodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_login);
        ButterKnife.bind(this);
        initViews();
        setListeners();

//        createEmployeeTable();
//        employeeListResponse = dbHelper.fetchAllEmployeeInformation();//to get information of all the employee
    }


    //This method is used to initilize views
    private void initViews() {

//        dbHelper = DbHelper.getInstance(this);
        res = getResources();
        ivCross.setVisibility(View.GONE);
        toolbarTitle.setText(res.getString(R.string.login_admin));
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);
        appUtils = AppUtils.getInstance();
        employeeMassageCountList = new ArrayList<>();
        employeeList = new ArrayList<>();
        employeeListResponse = new ArrayList<>();
    }


    //This method is used to set listners for various views in the class
    private void setListeners() {

        etManagerId.setOnFocusChangeListener(this);
        etPasscode.setOnFocusChangeListener(this);
        etPasscode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    appUtils.hideNativeKeyboard(ManagerLoginActivity.this);
                    if (networkConnectionIndicator.isNetworkAvailable(true)) {
                        if (validateData()) {

//                            validateFromDb(etManagerId.getText().toString().trim(), etPasscode.getText().toString().trim());
                            hitSubAdminLoginApi();
                        }
                    } else {
                        appUtils.showToast(ManagerLoginActivity.this, res.getString(R.string.pls_check_internet));
                    }
                }
                return false;
            }
        });
    }


    /*
    * This method is used to hit the hit login API for Sub admin
    *
    * */
    private void hitSubAdminLoginApi() {

        appUtils.showProgressDialog(ManagerLoginActivity.this, res.getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.MANAGER_ID, etManagerId.getText().toString().trim());
        params.put(Constant.MEMBER_PASSCODE, etPasscode.getText().toString().trim());
        params.put(Constant.OWNER_ID, AppSharedPreference.getString(ManagerLoginActivity.this, Constant.OWNER_ID));
        params.put(Constant.ACTION, "subadmin");

        Call<ResponseBody> call = service.subAdminLogin(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> res) {
                appUtils.hideProgressDialog();
                String response = "";
                try {
                    if (res != null) {
                        response = res.body().string();
                        ObjectMapper objectMapper = new ObjectMapper();
                        SubAdminLoginModel subAdminLoginModel = objectMapper.readValue(response, SubAdminLoginModel.class);

                        if (subAdminLoginModel.getCODE().toString().equals("200")) {


                            AppSharedPreference.putString(ManagerLoginActivity.this, Constant.MANAGER_ID, subAdminLoginModel.getVALUE().getEmail());
                            AppSharedPreference.putString(ManagerLoginActivity.this, Constant.EMPLOYEE_ID, subAdminLoginModel.getVALUE().getSubAdminId());
                            AppSharedPreference.putString(ManagerLoginActivity.this, Constant.EMPLOEE_MASSAGE_DURATION, subAdminLoginModel.getVALUE().getSession());

//                        Intent intent = new Intent(EmployeeLogin.this, MassageStartActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
//                        finish();

                            validateEmployeeApi();


                        } else {
                            appUtils.showToast(ManagerLoginActivity.this, subAdminLoginModel.getMESSAGE());
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(ManagerLoginActivity.this, res.getString(R.string.something_went_wrong));
            }
        });


    }


    //This method is used to valiadte the entered data by the user
    private boolean validateData() {

        if (etManagerId.getText().toString().trim().equals("")) {
            appUtils.showToast(ManagerLoginActivity.this, getResources().getString(R.string.manager_id_invalid));
            return false;
        } else if (etManagerId.getText().toString().trim().length() < 4) {
            appUtils.showToast(ManagerLoginActivity.this, getResources().getString(R.string.manager_id_invalid));
            return false;
        }
//        else if (etManagerId.getText().toString().length() > 10 &&  !appUtils.isValifEmailId(etManagerId.getText().toString())) {
//            appUtils.showToast(ManagerLoginActivity.this, getResources().getString(R.string.manager_id_invalid));
//            return false;
//        }
        else if (etPasscode.getText().toString().trim().length() == 0) {
            appUtils.showToast(ManagerLoginActivity.this, getResources().getString(R.string.pls_enter_passcode));
            return false;

        } else if (etPasscode.getText().toString().trim().length() < 4) {
            appUtils.showToast(ManagerLoginActivity.this, getResources().getString(R.string.passcode_min_character));
            return false;

        }
        return true;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {

        switch (view.getId()) {
            case R.id.et_manager_id:
                if (hasFocus) {
                    ivMemberId.setImageResource(R.drawable.ic_tablet_signup_member_id_select);
                } else {
                    ivMemberId.setImageResource(R.drawable.ic_tablet_signup_member_id_deselect);
                }
                break;

            case R.id.et_passcode:
                if (hasFocus) {
                    ivPasscode.setImageResource(R.drawable.ic_tablet_signup_lock_select);

                } else {
                    ivPasscode.setImageResource(R.drawable.ic_tablet_signup_lock_deselect);
                }
                break;
        }

    }

    @OnClick({R.id.iv_toolbar_left_icon, R.id.tv_login, R.id.iv_logo})

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_toolbar_left_icon:
                appUtils.hideNativeKeyboard(ManagerLoginActivity.this);
                finish();
                break;


            case R.id.tv_login:
                appUtils.hideNativeKeyboard(ManagerLoginActivity.this);
                if (networkConnectionIndicator.isNetworkAvailable(true)) {
                    if (validateData()) {

//                        validateFromDb(etManagerId.getText().toString().trim(), etPasscode.getText().toString().trim());
                        hitSubAdminLoginApi();
                    }
                } else {
                    appUtils.showToast(ManagerLoginActivity.this, res.getString(R.string.pls_check_internet));
                }

                break;

            case R.id.iv_logo:

//                MassageCompleted(AppSharedPreference.getString(ManagerLoginActivity.this, Constant.MANAGER_ID));
        }
    }


    //This method is called when massage is been started hence insert 0
    private void insertDatainCountTable(String id) {

        EmployeeMassageCountBean emp1 = new EmployeeMassageCountBean();
        emp1.setEmpId(id);
        emp1.setEmpCompletion(0);//this shows that massage is started
        dbHelper.insertDataIntoManagerMassageCountTable(emp1);

    }


    //This method is used to validate cred from DB
    private void validateFromDb(String id, String passcode) {


    }
//        for (int i = 0; i < employeeListResponse.size(); i++) {
//
//            if (id.equals(employeeListResponse.get(i).getEmployeeId())) {
//
//                count = 1;
//                if (passcode.equals(employeeListResponse.get(i).getEmployeePascode())) {
//
//                    AppSharedPreference.putString(EmployeeLogin.this, Constant.MANAGER_ID, etManagerId.getText().toString().trim());
//                    insertDatainCountTable(etManagerId.getText().toString().trim());//we are assuming that massage started as soon he logs in ..will change it later
//                    Intent intent = new Intent(EmployeeLogin.this, MassageSplashActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                    this.finish();
//                    break;
//
//                } else {
//                    appUtils.showToast(this, res.getString(R.string.pass_did_not_match));
//                }
//            }
//        }
//
//        if (count ==0)
//            appUtils.showToast(this, res.getString(R.string.emp_id_does_not_exist));
//    }


    //This method is used to create Db for the EMPLOYEE_LOGIN_TBALE
    private void createEmployeeTable() {

        //inserting data in the employee table
        EmployeeLoginBean e1 = new EmployeeLoginBean();
        e1.setEmployeeId("12345");
        e1.setEmployeeMassageDuration("10");
        e1.setEmployeePascode("1234");
        employeeList.add(e1);


        EmployeeLoginBean e2 = new EmployeeLoginBean();
        e2.setEmployeeId("activity_package_details");
        e2.setEmployeeMassageDuration("10");
        e2.setEmployeePascode("activity_package_details");
        employeeList.add(e2);

        EmployeeLoginBean e3 = new EmployeeLoginBean();
        e3.setEmployeeId("abcde");
        e3.setEmployeeMassageDuration("10");
        e3.setEmployeePascode("activity_package_details");
        employeeList.add(e3);


        EmployeeLoginBean e4 = new EmployeeLoginBean();
        e4.setEmployeeId("prateek");
        e4.setEmployeeMassageDuration("10");
        e4.setEmployeePascode("1234");
        employeeList.add(e4);

        //got the array list the first time
//        coming fot the first time
        dbHelper.insertDataIntoEmployeeTable(employeeList);

    }


    //method called when massage complted
    private void MassageCompleted(String id) {
        if (id != null) {
            dbHelper.updateDataIntoEmployeeMassageCountTable(id);
        }
    }


    /*
    * Validate the Employee
    * */
    public void validateEmployeeApi() {

        final AppUtils appUtils = AppUtils.getInstance();
        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.EMPLOYEE_ID, AppSharedPreference.getString(this, Constant.EMPLOYEE_ID));
        params.put(Constant.ACTION, "sastatus");

        Call<ResponseBody> call = service.checkPackageStatus(params);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                int count = 0;
                String str = "";

                try {
                    if (response != null) {
                        str = response.body().string();

                        ObjectMapper objectMapper = new ObjectMapper();
                        startMassagemodel = objectMapper.readValue(str, StartMassageBean.class);

                        if (startMassagemodel.getCODE().toString().equalsIgnoreCase("200")) {

                            if (startMassagemodel.getVALUE().getIsUse() == 1) {
                                appUtils.showDialog(ManagerLoginActivity.this, getResources().getString(R.string.your_have_given_your_massages), getResources().getString(R.string.ok), 1);
                            } else if (startMassagemodel.getVALUE().getIsUse() == 0) {

                                AppSharedPreference.putBoolean(ManagerLoginActivity.this, Constant.IS_MANAGER, true);
                                AppSharedPreference.putBoolean(ManagerLoginActivity.this, Constant.IS_GUEST, false);
                                AppSharedPreference.putBoolean(ManagerLoginActivity.this, Constant.IS_USER, false);

//                                Intent intent = new Intent(ManagerLoginActivity.this, MassageSplashActivity.class);
////                                Intent intent = new Intent(ManagerLoginActivity.this, MassageStartActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                finish();
                            }
                        }
                    }
                } catch (Exception e1) {
                    appUtils.hideProgressDialog();
                    appUtils.showToast(ManagerLoginActivity.this, getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(ManagerLoginActivity.this, getResources().getString(R.string.something_went_wrong));

            }
        });
    }

}
