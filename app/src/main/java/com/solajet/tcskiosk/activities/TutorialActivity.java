package com.solajet.tcskiosk.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import com.solajet.tcskiosk.tcskiosk.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.TouchScreenTextView;

public class TutorialActivity extends AppCompatActivity {


    @BindView(R.id.tv_continue)
    TouchScreenTextView tvContinue;
    @BindView(R.id.b1)
    TouchScreenTextView b1;
    @BindView(R.id.b2)
    TouchScreenTextView b2;
    @BindView(R.id.b3)
    TouchScreenTextView b3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_new);
        ButterKnife.bind(this);


//        images = new ArrayList<>();
//        images.add(R.drawable.ic_tutorial_screen_one);
//        images.add(R.drawable.ic_tutorial_screen_two);
//        images.add(R.drawable.ic_tutorial_screen_three);
//        setUpViewPager();
    }

    //This method is used to set up view pager

//    private void setUpViewPager() {
//
//        TutorialAdapter tutorialAdapter = new TutorialAdapter(images, TutorialActivity.this);
//        vpTutorial.setAdapter(tutorialAdapter);
//        cpi.setViewPager(vpTutorial);
//
//        vpTutorial.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//                switch (position) {
//
//                    case 0:
//                        tvTutorialText.setText(R.string.tut1);
//                        tvContinue.setVisibility(View.GONE);
//
//                        break;
//
//                    case 1:
//                        tvTutorialText.setText(R.string.tut2);
//                        tvContinue.setVisibility(View.GONE);
//
//                        break;
//
//                    case 2:
//                        tvTutorialText.setText(R.string.tut2);
//                        tvContinue.setVisibility(View.VISIBLE);
//                        break;
//                }
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//    }

    @OnClick(R.id.tv_continue)
    public void onClick() {

        startActivity(new Intent(TutorialActivity.this, HomeActivity.class));
        finish();
    }

}
