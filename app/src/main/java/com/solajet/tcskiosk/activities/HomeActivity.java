package com.solajet.tcskiosk.activities;

import android.app.Dialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import adapter.UserCategoryAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import customviews.TouchScreenTextView;
import interfaces.ApiInterface;
import interfaces.GuestLoginAvailabilityListener;
import models.UserCategoryModel;
import models.VerifyOwneridModel.VerifyOwnerIdModel;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;
import utils.GuestAvailabilityThread;
import utils.NetworkConnectionIndicator;

/**
 * Created by admin1 on 27/10/16.
 */

public class HomeActivity extends AppCompatActivity implements GuestLoginAvailabilityListener {

    @BindView(R.id.rootView)
    RelativeLayout rootView;

    static final String TAG = "DevicePolicyActivity";
    static final int ACTIVATION_REQUEST = 47; // identifies our request id
    @BindView(R.id.tv_welcome_text)
    TouchScreenTextView tvWelcomeText;
    @BindView(R.id.rv_login_options)
    RecyclerView rvLoginOptions;
    DevicePolicyManager devicePolicyManager;
    ComponentName demoDeviceAdmin;
    private Dialog verifyDialog;
    private TextView tvOwnerId, tvSubmit;
    private ImageView ivCross;
    private AppUtils appUtils;
    private NetworkConnectionIndicator networkConnectionIndicator;
    private ArrayList<UserCategoryModel> categoryModelsList;
    private Handler availabilityHandler;
    private GuestAvailabilityThread availabilityThread;
    private UserCategoryAdapter userCategoryAdapter;
    private Dialog UnlockDialog;
    private CountDownTimer countDownTime;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initView();
        startTimer();
        setClick();

//        this.startLockTask();
//        startGuestAvailabilityThread();
//        Foreground.get().onActivityResumed(this);
        // Initialize Device Policy Manager service and our receiver class
//        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
//        demoDeviceAdmin = new ComponentName(this, DeviceAdminReceiver.class);


        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
//            getWindow().getDecorView().setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
//                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
//                            | View.SYSTEM_UI_FLAG_IMMERSIVE);

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLauncherDialog(0);
//                resetPreferedLauncher();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void resetPreferedLauncher() {

        PackageManager pm = this.getPackageManager();
        ComponentName mockupComponent = new  ComponentName(getPackageName(), HomeActivity.class.getName());
        pm.clearPackagePreferredActivities(getPackageName());

        pm.setComponentEnabledSetting(mockupComponent,  PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(startMain);

        pm.setComponentEnabledSetting(mockupComponent,  PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
        // or
        //pm.setComponentEnabledSetting(mockupComponent, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
    }


    public void makePreferred(Context c) {

        PackageManager p = c.getPackageManager();
        ComponentName cN = new ComponentName(c, HomeActivity.class);
        p.setComponentEnabledSetting(cN, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        p.clearPackagePreferredActivities(getPackageName());
        final Intent intent=new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        c.startActivity(intent);
        p.setComponentEnabledSetting(cN, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

//        String command = "adb shell\n" +
//                "dpm set-device-owner com.solajet.tcskiosk.tcskiosk/utils.MyAdmin";
//        try {
//            Process process = Runtime.getRuntime().exec(command);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    /*
                    This method is used to initialize the views
                     */
    private void initView() {
        appUtils = AppUtils.getInstance();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);
        String stringArray[] = getResources().getStringArray(R.array.user_category);
        if (AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID) == null || AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID).equalsIgnoreCase("")) {
            showConfirmOwnerIdentityDialog();
        } else {

            String club_name = AppSharedPreference.getString(HomeActivity.this, Constant.CLUB_NAME)
                    + " " + getResources().getString(R.string.massage_centre);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                tvWelcomeText.setText(Html.fromHtml(getResources().getString(R.string.welcome_to) + " <br> " + club_name, Html.FROM_HTML_MODE_LEGACY));
            else
                tvWelcomeText.setText(Html.fromHtml(getResources().getString(R.string.welcome_to) + " <br>" + club_name));
        }

        //Setting items in the recycler view on the Main Activity
        categoryModelsList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            UserCategoryModel model = new UserCategoryModel();
            model.setUserType(stringArray[i]);
            categoryModelsList.add(model);
        }

        userCategoryAdapter = new UserCategoryAdapter(categoryModelsList, this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);

        rvLoginOptions.setHasFixedSize(true);
        rvLoginOptions.setAdapter(userCategoryAdapter);
        rvLoginOptions.setLayoutManager(gridLayoutManager);
        findViewById(R.id.rootView).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }
                return false;
            }
        });

        rvLoginOptions.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }
                return false;
            }
        });
    }

    /*
    This method is used to verify the owner ID for the club member every time on using any session
    * */
    private void showConfirmOwnerIdentityDialog() {
        verifyDialog = new Dialog(HomeActivity.this);
        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifyDialog.setContentView(R.layout.dialog_takeownerid);
        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifyDialog.show();
        verifyDialog.setCancelable(false);

        tvOwnerId = (EditText) verifyDialog.findViewById(R.id.et_ownerid);

        tvOwnerId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_DONE) {

                    if (!tvOwnerId.getText().toString().trim().equals("")) {
                        if (networkConnectionIndicator.isNetworkAvailable(true)) {
                            verifyOwnerIdApi();
                        } else {
                            appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_check_internet));
                        }

                    } else {
                        appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_owner_id));
                    }
                }
                return false;
            }
        });

        tvSubmit = (TextView) verifyDialog.findViewById(R.id.tv_submitownerid);

        verifyDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                HomeActivity.this.finish();
            }
        });

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvOwnerId.getText().toString().trim().equals("")) {
                    if (networkConnectionIndicator.isNetworkAvailable(true)) {
                        verifyOwnerIdApi();
                    } else {
                        appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_check_internet));
                    }

                } else {
                    appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_owner_id));
                }
            }
        });


        ivCross = (ImageView) verifyDialog.findViewById(R.id.iv_cross);
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyDialog.dismiss();
                finish();
            }
        });
    }


    public void showLauncherDialog(final int position) {
        UnlockDialog = new Dialog(HomeActivity.this);
        UnlockDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        UnlockDialog.setContentView(R.layout.dialog_takeownerid);
        UnlockDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        UnlockDialog.show();
        UnlockDialog.setCancelable(false);

        tvOwnerId = (EditText) UnlockDialog.findViewById(R.id.et_ownerid);

        tvOwnerId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_DONE) {

                    if (!tvOwnerId.getText().toString().trim().equals("")) {
                        if (AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID) != null && !AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID).equalsIgnoreCase("")) {

                            if (AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID).equalsIgnoreCase(tvOwnerId.getText().toString().trim())) {
                                UnlockDialog.dismiss();
                                makePreferred(getApplicationContext());
                            } else {
                                appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_valid_owner));
                            }

                        }

                    } else {
                        appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_owner_id));
                    }
                }
                return false;
            }
        });

        tvSubmit = (TextView) UnlockDialog.findViewById(R.id.tv_submitownerid);

        UnlockDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                UnlockDialog.dismiss();
            }
        });

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvOwnerId.getText().toString().trim().equals("")) {
                    if (AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID) != null && !AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID).equalsIgnoreCase("")) {

                        if (AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID).equalsIgnoreCase(tvOwnerId.getText().toString().trim())) {
                            UnlockDialog.dismiss();
                            makePreferred(getApplicationContext());
                        } else {
                            appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_valid_owner));
                        }

                    }

                } else {
                    appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_owner_id));
                }
            }
        });


        ivCross = (ImageView) UnlockDialog.findViewById(R.id.iv_cross);
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnlockDialog.dismiss();
            }
        });
    }



    /*
   This method is used to verify the owner ID for the club member for the first time only
   * */
//    private void showConfirmOwnerIdentityDialog() {
//        verifyDialog = new Dialog(HomeActivity.this);
//        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        verifyDialog.setContentView(R.layout.dialog_takeownerid);
//        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        verifyDialog.show();
//        verifyDialog.setCancelable(false);
//
//        tvOwnerId = (EditText) verifyDialog.findViewById(R.id.et_ownerid);
//
//        tvOwnerId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//
//                if (i == EditorInfo.IME_ACTION_DONE) {
//
//                    if (!tvOwnerId.getText().toString().trim().equals("")) {
//                        if (networkConnectionIndicator.isNetworkAvailable(true)) {
//                            verifyOwnerIdApi();
//                        } else {
//                            appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_check_internet));
//                        }
//
//                    } else {
//                        appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_owner_id));
//                    }
//                }
//                return false;
//            }
//        });
//
//        tvSubmit = (TextView) verifyDialog.findViewById(R.id.tv_submitownerid);
//
//        verifyDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                HomeActivity.this.finish();
//            }
//        });
//
//        tvSubmit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!tvOwnerId.getText().toString().trim().equals("")) {
//                    if (networkConnectionIndicator.isNetworkAvailable(true)) {
//                        verifyOwnerIdApi();
//                    } else {
//                        appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_check_internet));
//                    }
//
//                } else {
//                    appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_owner_id));
//                }
//            }
//        });
//
//
//        ivCross = (ImageView) verifyDialog.findViewById(R.id.iv_cross);
//        ivCross.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                verifyDialog.dismiss();
//                finish();
//            }
//        });
//    }



    public void showUnlockDialog(final int position) {
        UnlockDialog = new Dialog(HomeActivity.this);
        UnlockDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        UnlockDialog.setContentView(R.layout.dialog_takeownerid);
        UnlockDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        UnlockDialog.show();
        UnlockDialog.setCancelable(false);

        tvOwnerId = (EditText) UnlockDialog.findViewById(R.id.et_ownerid);

        tvOwnerId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_DONE) {

                    if (!tvOwnerId.getText().toString().trim().equals("")) {
                        if(AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID) != null && !AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID).equalsIgnoreCase("")){

                            if(AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID).equalsIgnoreCase(tvOwnerId.getText().toString().trim())){
                                UnlockDialog.dismiss();
                                userCategoryAdapter.performAction(position);
                            }else {
                                appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_valid_owner));
                            }

                        }

                    } else {
                        appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_owner_id));
                    }
                }
                return false;
            }
        });

        tvSubmit = (TextView) UnlockDialog.findViewById(R.id.tv_submitownerid);

        UnlockDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                UnlockDialog.dismiss();
            }
        });

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvOwnerId.getText().toString().trim().equals("")) {
                    if(AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID) != null && !AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID).equalsIgnoreCase("")){

                        if(AppSharedPreference.getString(HomeActivity.this, Constant.OWNER_ID).equalsIgnoreCase(tvOwnerId.getText().toString().trim())){
                            UnlockDialog.dismiss();
                            userCategoryAdapter.performAction(position);
                        }else {
                            appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_valid_owner));
                        }

                    }

                } else {
                    appUtils.showToast(HomeActivity.this, getResources().getString(R.string.pls_enter_owner_id));
                }
            }
        });


        ivCross = (ImageView) UnlockDialog.findViewById(R.id.iv_cross);
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnlockDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
//        Intent intent = new Intent(
//                DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
//        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
//                demoDeviceAdmin);
//        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
//                "Your boss told you to do this");
//        startActivityForResult(intent, ACTIVATION_REQUEST);


//        Intent intentt = new Intent(ACTION_PROVISION_MANAGED_PROFILE);
//        intent.putExtra(EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME,
//                this.getApplicationContext().getPackageName());
//        if (intent.resolveActivity(this.getPackageManager()) != null) {
//            startActivityForResult(intent, 12356);
//            this.finish();
//        } else {
//            Toast.makeText(this, "Stopping.",Toast.LENGTH_SHORT).show();
//        }

        super.onResume();
//        Foreground.get().addListener(this);
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (requestCode) {
//            case ACTIVATION_REQUEST:
//                if (resultCode == Activity.RESULT_OK) {
//                    Log.i(TAG, "Administration enabled!");
////                    toggleButton.setChecked(true);
//
//                } else {
//                    Log.i(TAG, "Administration enable FAILED!");
////                    toggleButton.setChecked(false);
//                }
//                return;
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    //This method is used hit the Owner Verify API
    public void verifyOwnerIdApi() {

        appUtils.showProgressDialog(HomeActivity.this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.OWNER_ID, tvOwnerId.getText().toString().trim());

        Call<ResponseBody> call = service.checkOwner(params);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                tvOwnerId.setText("");
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {
                        str = response.body().string();
                        ObjectMapper mapper = new ObjectMapper();
                        VerifyOwnerIdModel verifyOwnerIdModel = mapper.readValue(str, VerifyOwnerIdModel.class);


                        if (verifyOwnerIdModel.getCODE().toString().equals("200")) {

                            AppSharedPreference.putString(HomeActivity.this, Constant.OWNER_ID, verifyOwnerIdModel.getVALUE().getOwnerId());
                            AppSharedPreference.putString(HomeActivity.this, Constant.CLUB_NAME, verifyOwnerIdModel.getVALUE().getCName());

                            String club_name = verifyOwnerIdModel.getVALUE().getCName()
                                    + " " + getResources().getString(R.string.massage_centre);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                                tvWelcomeText.setText(Html.fromHtml(getResources().getString(R.string.welcome_to) + " <br> " + club_name, Html.FROM_HTML_MODE_LEGACY));
                            else
                                tvWelcomeText.setText(Html.fromHtml(getResources().getString(R.string.welcome_to) + " <br> " + club_name));
                            verifyDialog.dismiss();

                        } else {
                            Toast toatsMessage = Toast.makeText(HomeActivity.this, verifyOwnerIdModel.
                                    getMESSAGE(), Toast.LENGTH_LONG);
                            toatsMessage.setGravity(Gravity.CENTER, 0, 0);
                            toatsMessage.show();

                            ViewGroup viewGroup = (ViewGroup) toatsMessage.getView();
                            TextView toastText = (TextView) viewGroup.getChildAt(0);
                            toastText.setTextSize(20);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(HomeActivity.this, getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(HomeActivity.this, getResources().getString(R.string.something_went_wrong));
            }
        });
    }

    @Override
    public void onCheckAvailability(int availability) {
        availabilityThread.updateInstructions();
        AppSharedPreference.putInt(HomeActivity.this, Constant.GUEST_LOCK_CODE, availability);
        Log.e("GuestAvailable", availability + "");
        if (availability == 1 && rvLoginOptions.getChildAt(2) != null && rvLoginOptions.getChildAt(2).getVisibility() == View.VISIBLE) {
            rvLoginOptions.getChildAt(2).setVisibility(View.GONE);

        } else if (availability == 0 && rvLoginOptions.getChildAt(2) != null && rvLoginOptions.getChildAt(2).getVisibility() == View.GONE) {
            rvLoginOptions.getChildAt(2).setVisibility(View.VISIBLE);
        } else if (availability == 2 && rvLoginOptions.getChildAt(2) != null && rvLoginOptions.getChildAt(2).getVisibility() == View.GONE) {
            rvLoginOptions.getChildAt(2).setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void onCheckAvailabilityOnFailure() {
        availabilityThread.updateInstructions();
    }


    public void startGuestAvailabilityThread() {
        availabilityHandler = new Handler();
        availabilityThread = new GuestAvailabilityThread(availabilityHandler, this, this);
        availabilityThread.start();
        availabilityThread.getLooper();
        availabilityThread.updateInstructions();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        countDownTime.cancel();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
//            availabilityThread.quitSafely();
//        } else {
//            availabilityThread.quit();
//        }
//        availabilityHandler.removeCallbacks(null);
    }




    private void startTimer() {

        countDownTime = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {


            }

            public void onFinish() {
                Intent intentMain = new Intent(HomeActivity.this, VideoHomeActivity.class);
                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentMain);
                finish();


            }

        }.start();

    }



    private void setClick(){

        userCategoryAdapter.SetonClickListner(new UserCategoryAdapter.categorySelecttion() {
            @Override
            public void onCategoriesClick(int position) {


                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }

            }
        });

    }


    @Override
    protected void onStop() {
        super.onStop();

        countDownTime.cancel();


    }
}
