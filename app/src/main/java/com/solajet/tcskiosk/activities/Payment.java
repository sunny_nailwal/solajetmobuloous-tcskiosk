package com.solajet.tcskiosk.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.solajet.tcskiosk.tcskiosk.R;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.TouchScreenTextView;
import dialogs.CardSwipeDialog;
import interfaces.ApiInterface;
import interfaces.CardConnectApi;
import models.CardConnectModel.AuthRequest;
import models.CardConnectModel.AuthResponse;
import models.CardConnectModel.CardSwipeData;
import models.StartMassage.StartMassageBean;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;

/**
 * Created by admin1 on 27/10/16.
 */

public class Payment extends AppCompatActivity {
    public static final String DIALOG_SWIPE_CARD_TAG = "DIALOG_SWIPE_CARD";
    private final String TAG = Payment.class.getSimpleName();

    private Dialog ShowDialog;
    private Dialog thankDialog;

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_first_name)
    ImageView ivUser;
    @BindView(R.id.rl_owner)
    RelativeLayout rlOwner;
    @BindView(R.id.iv_payment)
    ImageView ivPayment;
    @BindView(R.id.rl_gateway)
    RelativeLayout rlGateway;
    @BindView(R.id.rl_stripegateway)
    RelativeLayout rlstripegateway;
    @BindView(R.id.iv_googlewallet)
    ImageView ivGooglewallet;
    @BindView(R.id.rl_googlewallet)
    RelativeLayout rlGooglewallet;
    @BindView(R.id.toolbar_title)
    TouchScreenTextView toolbarTitle;
    @BindView(R.id.iv_toolbar_left_icon)
    ImageView ivToolbarLeftIcon;
    @BindView(R.id.iv_cross)
    ImageView ivCross;

    private String track1Data, track2Data;


    //PAYPAL payment request code
    private static final int REQUEST_CODE_PAYMENT = 1;

    public static final String EXTRA_RESULT_CONFIRMATION = "com.paypal.android.sdk.paymentConfirmation";
    public static final int RESULT_EXTRAS_INVALID = 2;

    private StartMassageBean startMassagemodel;
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "ATm_SvrgaLY3jegFj5Q4AAEwFYjKhh-o36SoHAajVkvFJM2CbHSY_qnKhiA2Im_TZOgQ9_1C_h82Y2lj";

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Solajet")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymentdetails);
        ButterKnife.bind(this);
        initViews();

        config.rememberUser(false);
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);


    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(AppSharedPreference.getString(this, Constant.PACKAGE_AMOUNT)), "USD", AppSharedPreference.getString(this, Constant.PACKAGE_NAME), paymentIntent);
    }

    public void onBuyPressed() {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

        Intent intent = new Intent(Payment.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null && confirm.getProofOfPayment().getState().equalsIgnoreCase("approved")) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4));

                    // TODO: send 'confirm' to your server for verification.
                    // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                    // for more details.
                    AppUtils.getInstance().showNewEnrollDialog(Payment.this);

                } catch (JSONException e) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        } else if (resultCode == Payment.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }

    //This method is used to initialize views
    private void initViews() {
        toolbarTitle.setText(R.string.payment_details);
        ivCross.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }


    @OnClick({R.id.iv_toolbar_left_icon, R.id.rl_owner, R.id.rl_gateway, R.id.rl_stripegateway, R.id.rl_googlewallet, R.id.rl_cardconnect})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_toolbar_left_icon:
                finish();
                break;
            case R.id.rl_owner:

//                Intent intent  = new Intent(Payment.this, MassageSplashActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);

                ValidateUserApi();
                /* This is for the case when when member of montly(example) package trying to take another massage
                 * by choosing pay at desk method
                 * */

                break;
            case R.id.rl_gateway:
                onBuyPressed();
                break;
            case R.id.rl_stripegateway: {
                Intent intent = new Intent(Payment.this, PaymentStripe.class);
                startActivity(intent);
                break;
            }
            case R.id.rl_cardconnect: {
                final Intent intent = new Intent(Payment.this, PaymentCardConnect.class);
                startActivity(intent);
                break;
            }

            case R.id.rl_googlewallet:
                Intent intentC = new Intent(Payment.this, UnderDevelopement.class);
//                intentC.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentC);
                break;
        }
    }


    @OnClick(R.id.rl_cardswipe)
    void onSwipeClicked() {
        //resetForm();
        //enableForm();



        final CardSwipeDialog cardSwipeDialog = CardSwipeDialog.newInstance();
        cardSwipeDialog.setCardSwipeListener(new CardSwipeDialog.CardSwipeListener() {
            @Override
            public void onCardSwipe(CardSwipeData cardSwipeData) {

                String card_holder_nme = cardSwipeData.getCardHolderName();
                String card_no = cardSwipeData.getCardNumber();
                String month = cardSwipeData.getExpirationMonth();
                String year = cardSwipeData.getExpirationYear();
                track1Data = cardSwipeData.getTrack1();
                track2Data = cardSwipeData.getTrack2();

                showMsgDialog(card_holder_nme, card_no, month, year);


                disableForm();
            }
        });

        if (getSupportFragmentManager().findFragmentByTag(DIALOG_SWIPE_CARD_TAG) == null) {
            cardSwipeDialog.show(getSupportFragmentManager(), DIALOG_SWIPE_CARD_TAG);
        }
    }


    private void disableForm() {
//        card_holder_nme.setEnabled(false);
//        card_no.setEnabled(false);
//        month_edt.setEnabled(false);
//        year_edt.setEnabled(false);
    }

    private void resetForm() {
//        track1Data = null;
//        track2Data = null;
//        card_holder_nme.setText("");
//        card_no.setText("");
//        cvc_edt.setText("");
//        month_edt.setSelection(0);
//        year_edt.setSelection(0);
    }

    private void enableForm() {
//        card_holder_nme.setEnabled(true);
//        card_no.setEnabled(true);
//        month_edt.setEnabled(true);
//        year_edt.setEnabled(true);
    }


    /*
     * This APi is used to validate user
     * */
    public void ValidateUserApi() {

        final AppUtils appUtils = AppUtils.getInstance();
        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.PACKAGE_ID, AppSharedPreference.getString(this, Constant.PACKAGE_ID));
        params.put(Constant.USER_ID, AppSharedPreference.getString(this, Constant.USER_ID));
        params.put(Constant.ACTION, "packagestatus");

        Call<ResponseBody> call = service.checkPackageStatus(params);
        call.enqueue(new Callback<ResponseBody>() {


            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                int count = 0;
                String str = "";

                try {
                    if (response != null) {
                        str = response.body().string();
                        ObjectMapper objectMapper = new ObjectMapper();
                        startMassagemodel = objectMapper.readValue(str, StartMassageBean.class);

                        if (startMassagemodel.getCODE().toString().equalsIgnoreCase("200")) {

                            if (startMassagemodel.getVALUE().getIsUse() == 1) {
                                showMassageCompletionDialog(getResources().getString(R.string.your_massage_has_been_taken));
                            } else if (startMassagemodel.getVALUE().getIsUse() == 0) {
                                AppUtils.getInstance().showNewEnrollDialog(Payment.this);


                            }
                        }
                    }
                } catch (Exception e1) {
                    appUtils.hideProgressDialog();
                    appUtils.showToast(Payment.this, getResources().getString(R.string.something_went_wrong));
                    e1.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(Payment.this, startMassagemodel.getMESSAGE() != null ? startMassagemodel.getMESSAGE() : getResources().getString(R.string.something_went_wrong));
            }
        });

    }


    public void showMsgDialog(final String cardHolderName, final String ccNumber, final String expMonth, final String expYear) {
        ShowDialog = new Dialog(Payment.this);
        ShowDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ShowDialog.setContentView(R.layout.swipe_card_details_layout);
        //ShowDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ShowDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ShowDialog.show();
        ShowDialog.setCancelable(false);

        //TextView show_msg = (TextView) ShowDialog.findViewById(R.id.touchScreenTextView);
        TextView no_btn = (TextView) ShowDialog.findViewById(R.id.tv_okclick);

        TextView packageName = (TextView) ShowDialog.findViewById(R.id.package_name);
        TextView totalAmaount = (TextView) ShowDialog.findViewById(R.id.total_amaount);
        TextView cardNo = (TextView) ShowDialog.findViewById(R.id.card_number);
        TextView month = (TextView) ShowDialog.findViewById(R.id.month);
        TextView year = (TextView) ShowDialog.findViewById(R.id.year);
        TextView cvv = (TextView) ShowDialog.findViewById(R.id.cvv);
        TextView cHolderName = (TextView) ShowDialog.findViewById(R.id.card_holder_name);


        packageName.setText(AppSharedPreference.getString(this, Constant.PACKAGE_NAME));
        totalAmaount.setText("$" + AppSharedPreference.getString(this, Constant.PACKAGE_AMOUNT));

        cardNo.setText(ccNumber);
        cHolderName.setText("Card Holder Name :"+cardHolderName);
        month.setText("Month : "+ expMonth);
        year.setText("Year : "+ expYear);

        final int month1 = Integer.parseInt(expMonth);
        final int year1 = Integer.parseInt(expYear);

        ///show_msg.setText("Thank you. Please take go to the massage bed and enter your phone number as member id and last 4 digits of phone number as password for a single massage.");

        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog.dismiss();
                //Send auth request to CardConnect
                sendAuth(ccNumber, month1, year1, "", cardHolderName);
            }
        });
    }



    void sendAuth(String cardNumber, int month, int year, String cvc, String cardHolderName) {
        final AppUtils appUtils = AppUtils.getInstance();
        final CardConnectApi cardConnectApi = RestApi.createCardConnectService();
        final String amount = AppSharedPreference.getString(this, Constant.PACKAGE_AMOUNT);

        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));

        //!!! Assumption: Amount is always a dollar amount with no cents portion, add 00 to make it a value in cents
        final AuthRequest authRequest = new AuthRequest.Builder()
                .setAccount(cardNumber)
                .setExpiry(String.format(Locale.getDefault(), "%02d%02d", month, year - 2000))
                .setAmount(amount + "00")
                .setCvv2(cvc)
                .setName(cardHolderName)
                .setTrack1(track1Data)
                .setTrack2(track2Data)
                .build();

        final Call<AuthResponse> authorizeCall = cardConnectApi.authorize(Constant.CARD_CONNECT_AUTH_HEADER, authRequest);

        authorizeCall.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                appUtils.hideProgressDialog();

                try {
                    if (response.isSuccessful()) {
                        final AuthResponse authResponse = response.body();

                        switch (authResponse.getResponseStatus()) {
                            case APPROVED:
                                AppSharedPreference.putBoolean(Payment.this, Constant.IS_PAYMENT, true);
                                handleApproved();
                                break;
                            case RETRY:
                                break;
                            case DECLINED:
                                break;
                        }

                        Toast.makeText(Payment.this, "" + authResponse.getResptext(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    appUtils.showToast(Payment.this, getResources().getString(R.string.something_went_wrong));
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(Payment.this, getString(R.string.something_went_wrong));
                t.printStackTrace();
            }
        });
    }


    private void handleApproved() {
        Intent intentMain = new Intent(this, VideoHomeActivity.class);
        intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentMain);
        finish();

    }


    /*
     * Shows the MemberShipExpireDialog
     * */
    private void showMassageCompletionDialog(String message) {
        TextView submitTV;
        ImageView crossIV;
        final Dialog verifyDialog;
        verifyDialog = new Dialog(Payment.this);
        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifyDialog.setContentView(R.layout.dialog_login_screen);
        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifyDialog.show();
        verifyDialog.setCancelable(false);

        ((TextView) verifyDialog.findViewById(R.id.tv_dialog_message)).setText(message);
        submitTV = (TextView) verifyDialog.findViewById(R.id.tv_dialog_action_text);
        submitTV.setText(R.string.ok);
        submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentMain = new Intent(Payment.this, VideoHomeActivity.class);
                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentMain);
                finish();
            }
        });


        //if we close the dialog close the app
        crossIV = (ImageView) verifyDialog.findViewById(R.id.iv_dialog_cross);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyDialog.dismiss();
                Intent intentMain = new Intent(Payment.this, VideoHomeActivity.class);
                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentMain);
                finish();
            }
        });
    }
}
