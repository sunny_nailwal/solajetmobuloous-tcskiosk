package com.solajet.tcskiosk.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.solajet.tcskiosk.tcskiosk.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import customviews.TouchScreenTextView;

public class AccountActivity extends AppCompatActivity {
    @BindView(R.id.webview) WebView webView;
    @BindView(R.id.toolbar_title) TouchScreenTextView toolbarTitle;
    @BindView(R.id.iv_toolbar_left_icon) ImageView toolbarBack;
    @BindView(R.id.iv_cross) ImageView toolbarClose;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview_payment);
        ButterKnife.bind(this);

        toolbarTitle.setText(R.string.my_account);
        toolbarClose.setVisibility(View.GONE);

        toolbarBack.setImageResource(R.drawable.ic_home);
        toolbarBack.setColorFilter(ContextCompat.getColor(this, R.color.white));
        toolbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient());

        final String url = String.format("https://massagemember.com/my-account/subscriptions/");

        CookieManager.getInstance().removeAllCookie();
        webView.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        webView.destroy();
        super.onDestroy();
    }
}
