package com.solajet.tcskiosk.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.TouchScreenTextView;
import interfaces.ApiInterface;
import models.AcceptTermsAndConditionModel.AcceptTnC;
import models.SubscribedPackageModel.SubscribedPackageModel;
import models.TermsAndCondition.TermsAndConditionModel;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;
import utils.NetworkConnectionIndicator;
import utils.SignatureView;

public class TermsAndCondition extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tv_terms_and_condition)
    TouchScreenTextView tvTermsAndCondition;
    @BindView(R.id.rl_toolbar)
    RelativeLayout rlToolbar;
    @BindView(R.id.check_box_terms)
    CheckBox checkBoxTerms;
    @BindView(R.id.ll_agrre_disagree)
    LinearLayout llAgrreDisagree;
    @BindView(R.id.toolbar_title)
    TouchScreenTextView toolbarTitle;
    @BindView(R.id.iv_toolbar_left_icon)
    ImageView ivToolbarLeftIcon;
    @BindView(R.id.iv_cross)
    ImageView ivCross;
    @BindView(R.id.ll_agreement)
    LinearLayout llAgreement;
    @BindView(R.id.divider)
    View divider;
    @BindView(R.id.tv_agree)
    TouchScreenTextView tvAgree;
    @BindView(R.id.tv_disagree)
    TouchScreenTextView tvDisagree;
    @BindView(R.id.ll_root_view)
    RelativeLayout llRootView;
    private Resources res;
    private AppUtils appUtils;
    private NetworkConnectionIndicator networkConnectionIndicator;
    private View sign_view;
    private SubscribedPackageModel mSubscribedPackageModel;
    private String packageid;
    private String packageSessionTime;
    private String packageAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);
        ButterKnife.bind(this);
        initViews();
        //hitTermsAndConditionApi();
        LinearLayout layout = (LinearLayout) findViewById(R.id.sign_layout);
        sign_view = LayoutInflater.from(this).inflate(R.layout.sign_layout, null);
        layout.addView(sign_view);
        if (getIntent().getStringExtra(Constant.PACKAGE_ID) != null && !getIntent().getStringExtra(Constant.PACKAGE_ID).equalsIgnoreCase("")) {
            packageid = getIntent().getStringExtra(Constant.PACKAGE_ID);
        }
        if (getIntent().getStringExtra(Constant.PACKAGE_SESSION_TIME) != null && !getIntent().getStringExtra(Constant.PACKAGE_ID).equalsIgnoreCase("")) {
            packageSessionTime = getIntent().getStringExtra(Constant.PACKAGE_SESSION_TIME);
        }
        if (getIntent().getStringExtra(Constant.PACKAGE_AMOUNT) != null && !getIntent().getStringExtra(Constant.PACKAGE_ID).equalsIgnoreCase("")) {
            packageAmount = getIntent().getStringExtra(Constant.PACKAGE_AMOUNT);
        }
    }


    private void initViews() {
        ivCross.setVisibility(View.GONE);
        res = getResources();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(TermsAndCondition.this);
        toolbarTitle.setText(res.getString(R.string.club_owner));
        appUtils = AppUtils.getInstance();
    }


    private void hitTermsAndConditionApi() {

        appUtils.showProgressDialog(TermsAndCondition.this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.PACKAGE_ID, AppSharedPreference.getString(TermsAndCondition.this, Constant.PACKAGE_ID));

        Call<ResponseBody> call = service.termsAndCondition(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str;
                try {
                    str = response.body().string();
                    ObjectMapper mapper = new ObjectMapper();
                    TermsAndConditionModel termsAndConditionModel = mapper.readValue(str, TermsAndConditionModel.class);

                    if (termsAndConditionModel.getCODE().toString().equals("200")) {

                        tvTermsAndCondition.setText(termsAndConditionModel.getVALUE().getTerm());
                    }

                } catch (IOException e1) {
                    e1.printStackTrace();
                    appUtils.showToast(TermsAndCondition.this, getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                final Snackbar snackbar = Snackbar.make(llRootView, getResources().getString(R.string.something_went_wrong), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                hitTermsAndConditionApi();
                            }
                        });
                snackbar.show();
            }
        });
    }


    @OnClick({R.id.iv_toolbar_left_icon, R.id.tv_agree, R.id.tv_disagree})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_toolbar_left_icon:
                appUtils.hideNativeKeyboard(this);
                finish();
                break;

            case R.id.tv_agree:

                if (checkBoxTerms.isChecked()) {
                    if (((SignatureView) sign_view).isSigned()) {
                        if (networkConnectionIndicator.isNetworkAvailable(true))
                            hitSubscriptionApi();
                        else
                            appUtils.showToast(this, getResources().getString(R.string.pls_check_internet));
                    } else {
                        appUtils.showToast(TermsAndCondition.this, res.getString(R.string.please_sign));
                    }
                } else {
                    appUtils.showToast(TermsAndCondition.this, res.getString(R.string.pls_accept_terms_privacy));
                }
                break;

            case R.id.tv_disagree:
                appUtils.showDialog(TermsAndCondition.this, res.getString(R.string.you_must_agree), res.getString(R.string.ok), 4);

        }
    }


//    private Map setMapData(String token){
//        Map<String,RequestBody> fields=new HashMap<>();
//
//
//        RequestBody term = RequestBody.create(MediaType.parse("text/plain"), "term");
//        RequestBody subscribe_id = RequestBody.create(MediaType.parse("text/plain"), AppSharedPreference.getString(TermsAndCondition.this, Constant.SUBSCRIPTION_ID));
//        RequestBody user_i = RequestBody.create(MediaType.parse("text/plain"), "MyProperty");
//        RequestBody beds = RequestBody.create(MediaType.parse("text/plain"), "3");
//
//
//        fields.put(Constant.ACTION,term);
//        fields.put(Constant.SUBSCRIPTION_ID,property_title);
//        fields.put("property_desc",property_desc);
//        fields.put("beds",beds);
//
//
//        return fields;
//
//    }


    private void hitAcceptTermsAndServiceService() {
        Bitmap bMap = ((SignatureView) sign_view).getSignatureBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bMap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        //bMap is the bitmap object
        byte[] b = baos.toByteArray();
        int i = b.length;
        Log.e("the output is", "" + i);
        String IMAGE = Base64.encodeToString(b, Base64.DEFAULT);

        appUtils.showProgressDialog(TermsAndCondition.this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.ACTION, "term");
        params.put(Constant.SUBSCRIPTION_ID, AppSharedPreference.getString(TermsAndCondition.this, Constant.SUBSCRIPTION_ID));
        params.put(Constant.USER_ID, AppSharedPreference.getString(TermsAndCondition.this, Constant.USER_ID));
        params.put(Constant.SIGNATURE, IMAGE);

        Call<ResponseBody> call = service.acceptTermsAndCondition(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str;
                try {
                    if (response != null) {
                        str = response.body().string();
                        ObjectMapper mapper = new ObjectMapper();
                        AcceptTnC acceptTnC = mapper.readValue(str, AcceptTnC.class);

                        if (acceptTnC.getCODE().toString().equals("200")) {
                            appUtils.showToast(TermsAndCondition.this, acceptTnC.getMESSAGE());
                            startActivity(new Intent(TermsAndCondition.this, Payment.class));
                        }

                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                    appUtils.showToast(TermsAndCondition.this, res.getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(TermsAndCondition.this, res.getString(R.string.something_went_wrong));

            }
        });
    }


    private void hitSubscriptionApi() {

        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.ACTION, "subscribepackage");
        params.put(Constant.PACKAGE_ID, packageid != null ? packageid : AppSharedPreference.getString(this, Constant.PACKAGE_ID));
        params.put(Constant.USER_ID, AppSharedPreference.getString(this, Constant.USER_ID));

        Call<ResponseBody> call = service.subscribe(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {
                        str = response.body().string();

                        ObjectMapper mapper = new ObjectMapper();
                        mSubscribedPackageModel = mapper.readValue(str, SubscribedPackageModel.class);

                        if (mSubscribedPackageModel.getCODE().toString().equals("200")) {
                            AppSharedPreference.putBoolean(TermsAndCondition.this, Constant.IS_USER, true);
                            AppSharedPreference.putBoolean(TermsAndCondition.this, Constant.IS_GUEST, false);
                            AppSharedPreference.putBoolean(TermsAndCondition.this, Constant.IS_MANAGER, false);
                            AppSharedPreference.putString(TermsAndCondition.this, Constant.PACKAGE_ID, packageid != null ? packageid : AppSharedPreference.getString(TermsAndCondition.this, Constant.PACKAGE_ID));
                            AppSharedPreference.putString(TermsAndCondition.this, Constant.PACKAGE_SESSION_TIME, packageSessionTime != null ? packageSessionTime : AppSharedPreference.getString(TermsAndCondition.this, Constant.PACKAGE_SESSION_TIME));
                            AppSharedPreference.putString(TermsAndCondition.this, Constant.PACKAGE_AMOUNT, packageAmount != null ? packageAmount : AppSharedPreference.getString(TermsAndCondition.this, Constant.PACKAGE_AMOUNT));
                            AppSharedPreference.putString(TermsAndCondition.this, Constant.SUBSCRIPTION_ID, String.valueOf(mSubscribedPackageModel.getVALUE().getSubscribeId()));

                            appUtils.showToast(TermsAndCondition.this, mSubscribedPackageModel.getMESSAGE());
//                            startActivity(new Intent(SignUpActivity.this, TermsAndCondition.class));

//                            Intent intent = new Intent(TermsAndCondition.this, TermsAndCondition.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
                            hitAcceptTermsAndServiceService();
                        }


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(TermsAndCondition.this, getResources().getString(R.string.something_went_wrong));
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(TermsAndCondition.this, getResources().getString(R.string.something_went_wrong));

            }
        });
    }

}

