package com.solajet.tcskiosk.activities;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.TouchScreenTextView;
import interfaces.ApiInterface;
import models.SinglePackageModel.SinglePackageModel;
import models.SinglePackageModel.VALUE;
import models.SubscribedPackageModel.SubscribedPackageModel;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;
import utils.NetworkConnectionIndicator;


/**
 * Created by admin1 on 27/10/16.
 */

public class PackageDetailsActivity extends AppCompatActivity {

    @BindView(R.id.iv_massage_image)
    ImageView ivMassageImage;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_massage_name)
    TouchScreenTextView tvMassageName;
    @BindView(R.id.tv_massage_duration)
    TouchScreenTextView tvMassageDuration;
    @BindView(R.id.tv_massage_description)
    TouchScreenTextView tvMassageDescription;
    @BindView(R.id.tv_massage_count)
    TouchScreenTextView tvMassageCount;
    @BindView(R.id.tv_massage_expiry_date)
    TouchScreenTextView tvMassageExpiryDate;
    @BindView(R.id.btn_subcribe)
    TouchScreenTextView btnSubcribe;
    @BindView(R.id.ll_rootView)
    LinearLayout llRootView;
    @BindView(R.id.tv_massage_bullet_one)
    TouchScreenTextView tvMassageBulletOne;
    @BindView(R.id.tv_massage_bullet_second)
    TouchScreenTextView tvMassageBulletSecond;
    @BindView(R.id.tv_massage_bullet_three)
    TouchScreenTextView tvMassageBulletThree;


    private List<VALUE> subscribePackageModels;
    private LinearLayoutManager linearLayoutManager;
    private String packageid, packageSessionTime, packageAmount;
    private AppUtils appUtils;
    private NetworkConnectionIndicator networkConnectionIndicator;
    private SinglePackageModel singlePackageModel;
    private SubscribedPackageModel mSubscribedPackageModel;
    private String massageType, massageDescription;
    private boolean subscribe;
    private Handler windowCloseHandler = new Handler();
    private Runnable windowCloserRunnable = new Runnable() {
        @Override
        public void run() {
            ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

            if (cn != null && cn.getClassName().equals("com.android.systemui.recent.RecentsActivity")) {
                toggleRecents();
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_details);
        ButterKnife.bind(this);
        initView();
        if (getIntent().getStringExtra("subscribe") != null && getIntent().getStringExtra("subscribe").equalsIgnoreCase("subscribe")) {
            subscribe = true;
        }
        if (getIntent().getStringExtra("change_package") != null && getIntent().getStringExtra("change_package").equalsIgnoreCase("change_package")) {
            subscribe = true;
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (!hasFocus) {
            windowCloseHandler.postDelayed(windowCloserRunnable, 250);
        }
    }

    private void toggleRecents() {
        Intent closeRecents = new Intent("com.android.systemui.recent.action.TOGGLE_RECENTS");
        closeRecents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        ComponentName recents = new ComponentName("com.android.systemui", "com.android.systemui.recent.RecentsActivity");
        closeRecents.setComponent(recents);
        this.startActivity(closeRecents);
    }

    private void initView() {

        Intent intent = getIntent();

        if (intent.getExtras().getString(Constant.PACKAGE_ID) != null) {
            packageid = getIntent().getExtras().getString(Constant.PACKAGE_ID);
        }

        appUtils = AppUtils.getInstance();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);

        getPackageDetails();
    }


    //This method is used to hit the api for fetching details subscribe package
    public void getPackageDetails() {

        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.ACTION, "packagedetail");
        params.put(Constant.PACKAGE_ID, packageid);

        Call<ResponseBody> call = service.getPackageDetail(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String str = "";
                try {
                    appUtils.hideProgressDialog();
                    str = response.body().string();

                    ObjectMapper mapper = new ObjectMapper();
                    singlePackageModel = mapper.readValue(str, SinglePackageModel.class);
                    if (singlePackageModel.getCODE().toString().equals("200")) {

                        getMassageType(singlePackageModel.getVALUE().getPackageType());
                        packageSessionTime = singlePackageModel.getVALUE().getDuration();
                        packageAmount = singlePackageModel.getVALUE().getPrice();

                        String massageDuration;
                        massageDuration = "( " + singlePackageModel.getVALUE().getDuration() + " min ) - " +
                                massageType;

                        tvMassageDuration.setText(massageDuration);
                        Glide.with(PackageDetailsActivity.this)
                                .load(singlePackageModel.getVALUE().getImage())
                                .into(ivMassageImage);


                        tvMassageDescription.setText(singlePackageModel.getVALUE().getPacDesc());
                        tvMassageExpiryDate.setText(massageDescription);
                        tvMassageCount.setText(String.valueOf(singlePackageModel.getVALUE().getNuMassage()) + "/Month");
                        tvMassageName.setText(singlePackageModel.getVALUE().getPackageName());
                        tvMassageBulletOne.setText(singlePackageModel.getVALUE().getB1());
                        tvMassageBulletSecond.setText(singlePackageModel.getVALUE().getB2());
//                        tvMassageBulletThree.setText(singlePackageModel.getVALUE().getB3());


                    } else {
                        appUtils.hideProgressDialog();
                        appUtils.showToast(PackageDetailsActivity.this, singlePackageModel.getMESSAGE());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();

                final Snackbar snackbar = Snackbar.make(llRootView, getResources().getString(R.string.something_went_wrong), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                getPackageDetails();
                            }
                        });
                snackbar.show();
            }
        });

    }

    //this method massageType = s the type of massage 1=day,2,=week,3=byweek,4=month,5=bymonth
    private void getMassageType(Integer packageType) {

        switch (packageType) {

            case 1:
                massageType = "Daily";
                massageDescription = getString(R.string.text_daily);
                break;

            case 2:
                massageType = "Weekly";
                massageDescription = getString(R.string.text_weekly);
                break;

            case 3:
                massageType = "Bi-Weekly";
                massageDescription = getString(R.string.text_bi_weekly);
                break;


            case 4:
                massageType = "Monthly";
                massageDescription = getString(R.string.text_monthly);
                break;


            case 5:
                massageType = "Bi-monthly";
                massageDescription = getString(R.string.text_bi_monthly);
                break;


            default:
                massageType = "Other";
                massageDescription = getString(R.string.other);
                break;

        }
    }


    @OnClick({R.id.btn_subcribe, R.id.iv_back})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_subcribe:
                appUtils.hideNativeKeyboard(this);
                if (networkConnectionIndicator.isNetworkAvailable(true))
                    if (subscribe) {
                        hitSubscriptionApi();
                    } else {
                        startActivity(new Intent(PackageDetailsActivity.this, SignUpActivity.class).putExtra(Constant.PACKAGE_ID, packageid).putExtra(Constant.PACKAGE_SESSION_TIME, packageSessionTime).putExtra(Constant.PACKAGE_AMOUNT, packageAmount));
                    }
//                    hitSubscriptionApi();
                else
                    appUtils.showToast(PackageDetailsActivity.this, getResources().getString(R.string.pls_check_internet));
                break;

            case R.id.iv_back:
                finish();
        }
    }


    /*
    * This method is sued to hit the package Subcription Api
    */
    private void hitSubscriptionApi() {

        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.ACTION, "subscribepackage");
        params.put(Constant.PACKAGE_ID, packageid);
        params.put(Constant.USER_ID, AppSharedPreference.getString(this, Constant.USER_ID));

        Call<ResponseBody> call = service.subscribe(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {
                        str = response.body().string();

                        ObjectMapper mapper = new ObjectMapper();
                        mSubscribedPackageModel = mapper.readValue(str, SubscribedPackageModel.class);

                        if (mSubscribedPackageModel.getCODE().toString().equals("200")) {

                            if (getIntent().getStringExtra("change_package").equalsIgnoreCase("change_package")) {
                                appUtils.showToast(PackageDetailsActivity.this, mSubscribedPackageModel.getMESSAGE());
                                Intent intentMain = new Intent(PackageDetailsActivity.this, HomeActivity.class);
                                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intentMain);
                                finish();
                            } else {
                                AppSharedPreference.putBoolean(PackageDetailsActivity.this, Constant.IS_USER, true);
                                AppSharedPreference.putBoolean(PackageDetailsActivity.this, Constant.IS_GUEST, false);
                                AppSharedPreference.putBoolean(PackageDetailsActivity.this, Constant.IS_MANAGER, false);
                                AppSharedPreference.putString(PackageDetailsActivity.this, Constant.PACKAGE_ID, packageid);
                                AppSharedPreference.putString(PackageDetailsActivity.this, Constant.PACKAGE_SESSION_TIME, packageSessionTime);
                                AppSharedPreference.putString(PackageDetailsActivity.this, Constant.PACKAGE_AMOUNT, packageAmount);
                                AppSharedPreference.putString(PackageDetailsActivity.this, Constant.SUBSCRIPTION_ID, String.valueOf(mSubscribedPackageModel.getVALUE().getSubscribeId()));
                                appUtils.showToast(PackageDetailsActivity.this, mSubscribedPackageModel.getMESSAGE());
                                startActivity(new Intent(PackageDetailsActivity.this, TermsAndCondition.class).putExtra(Constant.PACKAGE_ID, packageid));
                            }

                        }


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(PackageDetailsActivity.this, getResources().getString(R.string.something_went_wrong));
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(PackageDetailsActivity.this, getResources().getString(R.string.something_went_wrong));

            }
        });
    }

}
