package com.solajet.tcskiosk.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.TouchScreenTextView;
import database.DbHelper;
import interfaces.ApiInterface;
import models.LoginModel.LoginModel;
import models.MemberLogin.MemberLoginBean;
import models.StartMassage.StartMassageBean;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;
import utils.NetworkConnectionIndicator;


/**
 * Created by Prateek on 25/10/16.
 */

public class LoginActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    @BindView(R.id.iv_member_first_name)
    ImageView ivMember;
    @BindView(R.id.et_memberid)
    AppCompatEditText etMemberid;
    @BindView(R.id.iv_passcode)
    ImageView ivPasscode;
    @BindView(R.id.et_passcode)
    AppCompatEditText etPasscode;
    @BindView(R.id.tv_forgotpasscode)
    TextView tvForgotpasscode;
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.rootView)
    RelativeLayout rootView;
    @BindView(R.id.iv_toolbar_left_icon)
    ImageView ivToolbarLeftIcon;
    @BindView(R.id.iv_cross)
    ImageView ivCross;
    @BindView(R.id.toolbar_title)
    TouchScreenTextView toolbarTitle;


    private AppUtils appUtils;
    private NetworkConnectionIndicator networkConnectionIndicator;
    private Dialog verifyDialog;
    private DbHelper dbHelper;
    private ArrayList<MemberLoginBean> memberLoginList;
    private StartMassageBean startMassagemodel;
    private Resources res;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initView();
//        createMemberLoginTable();
    }


    private void initView() {

        dbHelper = DbHelper.getInstance(this);
        res = getResources();
        memberLoginList = new ArrayList();
        toolbarTitle.setText(res.getString(R.string.login));
        ivCross.setVisibility(View.GONE);
        etPasscode.setOnFocusChangeListener(this);
        etMemberid.setOnFocusChangeListener(this);
        appUtils = AppUtils.getInstance();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);
        etPasscode.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    appUtils.hideNativeKeyboard(LoginActivity.this);
                    if (networkConnectionIndicator.isNetworkAvailable(true)) {
                        if (validateUser()) {

                            logInService();
//                            verifyMemberFromDb(etMemberid.getText().toString().trim(), etPasscode.getText().toString().trim());
                        }
                    } else {
                        appUtils.showToast(LoginActivity.this, res.getString(R.string.pls_check_internet));
                    }
                }


                return false;
            }
        });
    }


    /*
    * hitting login api
    * */
    public void logInService() {

        appUtils.showProgressDialog(LoginActivity.this, res.getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.OWNER_ID, AppSharedPreference.getString(LoginActivity.this, Constant.OWNER_ID));
        params.put(Constant.MEMBER_ID, etMemberid.getText().toString().trim());
        params.put(Constant.MEMBER_PASSCODE, etPasscode.getText().toString().trim());
        params.put(Constant.ACTION, "normal");

        Call<ResponseBody> call = service.login(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {

                        str = response.body().string();
                        ObjectMapper mapper = new ObjectMapper();
                        LoginModel loginModel = mapper.readValue(str, LoginModel.class);

                        if (loginModel.getCODE().toString().equals("200")) {
//
//                            AppSharedPreference.putBoolean(LoginActivity.this, Constant.IS_USER, true);
//                            AppSharedPreference.putBoolean(LoginActivity.this, Constant.IS_GUEST, false);
//                            AppSharedPreference.putBoolean(LoginActivity.this, Constant.IS_MANAGER, false);

                            AppSharedPreference.putString(LoginActivity.this, Constant.USER_ID, loginModel.getVALUE().getUserdetail().getUserId());
                            AppSharedPreference.putString(LoginActivity.this, Constant.MEMBER_ID, loginModel.getVALUE().getUserdetail().getMemberId());
                            AppSharedPreference.putString(LoginActivity.this, Constant.USER_NAME, loginModel.getVALUE().getUserdetail().getFullName());
                            AppSharedPreference.putString(LoginActivity.this, Constant.LAST_NAME, loginModel.getVALUE().getUserdetail().getLastName());
                            if (getIntent().getStringExtra("change_package") != null && getIntent().getStringExtra("change_package").equalsIgnoreCase("change_package")) {
                                Intent intent = new Intent(LoginActivity.this, AllPackageActivity.class);
                                intent.putExtra("change_package", "change_package");
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else if (loginModel.getVALUE().getPacakageDetail().size() < 1) {
                                appUtils.showDialog(LoginActivity.this, res.getString(R.string.currently_you_are_not_long_text), res.getString(R.string.subscribe_now), 2);
                            } else if (loginModel.getVALUE().getPacakageDetail().get(0).getIsTerm().equalsIgnoreCase("0")) {
                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_ID, loginModel.getVALUE().getPacakageDetail().get(0).getPackageId());
                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_AMOUNT, loginModel.getVALUE().getPacakageDetail().get(0).getPrice());
                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_SESSION_TIME, loginModel.getVALUE().getPacakageDetail().get(0).getDuration());
                                AppSharedPreference.putString(LoginActivity.this, Constant.SUBSCRIPTION_ID, loginModel.getVALUE().getPacakageDetail().get(0).getSubscribeId());

                                appUtils.showDialog(LoginActivity.this, res.getString(R.string.your_tc_long_text), res.getString(R.string.accept), 3);

                            } else if (loginModel.getVALUE().getPacakageDetail().get(0).getIsManager() != null && loginModel.getVALUE().getPacakageDetail().get(0).getIsManager().equalsIgnoreCase("yes")) {

                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_ID, loginModel.getVALUE().getPacakageDetail().get(0).getPackageId());
                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_AMOUNT, loginModel.getVALUE().getPacakageDetail().get(0).getPrice());
                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_SESSION_TIME, loginModel.getVALUE().getPacakageDetail().get(0).getDuration());
//                                AppSharedPreference.putString(LoginActivity.this, Constant.EMPLOEE_MASSAGE_DURATION, loginModel.getVALUE().getPacakageDetail().get(0).getDuration());
                                AppSharedPreference.putBoolean(LoginActivity.this, Constant.IS_USER, true);
                                AppSharedPreference.putBoolean(LoginActivity.this, Constant.IS_GUEST, false);
                                AppSharedPreference.putBoolean(LoginActivity.this, Constant.IS_MANAGER, false);
                                AppSharedPreference.putBoolean(LoginActivity.this, Constant.MANAGER_LOGIN, true);

//                                Intent intentMain = new Intent(LoginActivity.this, MassageSplashActivity.class);
//                                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intentMain);
//                                finish();

                            }
//                            else if (validateDate(loginModel.getVALUE().getPacakageDetail().get(0).getExpireDate())) {
//
//                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_ID, loginModel.getVALUE().getPacakageDetail().get(0).getPackageId());
//                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_AMOUNT, loginModel.getVALUE().getPacakageDetail().get(0).getPrice());
//                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_SESSION_TIME, loginModel.getVALUE().getPacakageDetail().get(0).getDuration());
//
//                                validateMemberApi();
//
//                            }
                            else if (loginModel.getVALUE().getPacakageDetail().size() > 0) {

                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_ID, loginModel.getVALUE().getPacakageDetail().get(0).getPackageId());
                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_AMOUNT, loginModel.getVALUE().getPacakageDetail().get(0).getPrice());
                                AppSharedPreference.putString(LoginActivity.this, Constant.PACKAGE_SESSION_TIME, loginModel.getVALUE().getPacakageDetail().get(0).getDuration());

                                validateMemberApi();

                            } else {
                                appUtils.showDialog(LoginActivity.this, res.getString(R.string.your_massage_package_expired), res.getString(R.string.subscribe_now), 2);
                            }

                        } else {
                            appUtils.showToast(LoginActivity.this, loginModel.getMESSAGE());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(LoginActivity.this, res.getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(LoginActivity.this, res.getString(R.string.something_went_wrong));
            }
        });

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {

            case R.id.et_memberid:
                if (hasFocus) {
                    ivMember.setImageResource(R.drawable.ic_tablet_signup_member_id_select);

                } else {
                    ivMember.setImageResource(R.drawable.ic_tablet_signup_member_id_deselect);
                }
                break;
            default:
                if (hasFocus) {
                    ivPasscode.setImageResource(R.drawable.ic_tablet_signup_lock_select);

                } else {
                    ivPasscode.setImageResource(R.drawable.ic_tablet_signup_lock_deselect);
                }
        }
    }


    @OnClick({R.id.iv_toolbar_left_icon, R.id.tv_forgotpasscode, R.id.tv_login, R.id.rootView})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_toolbar_left_icon:
                appUtils.hideNativeKeyboard(LoginActivity.this);
                finish();
                break;

            case R.id.tv_forgotpasscode:
                startActivity(new Intent(LoginActivity.this, ForgotPasscodeActivity.class));
                break;

            case R.id.tv_login:
                appUtils.hideNativeKeyboard(this);
                if (networkConnectionIndicator.isNetworkAvailable(true)) {
                    if (validateUser()) {
                        logInService();
                    }
                } else {
                    appUtils.showToast(LoginActivity.this, res.getString(R.string.pls_check_internet));
                }
                break;

            case R.id.rootView:
                appUtils.hideNativeKeyboard(LoginActivity.this);
                break;

        }
    }


    /*
    * validate user input field
    * */
    private boolean validateUser() {

        if (!appUtils.validate(etMemberid.getText().toString().trim())) {
            appUtils.showToast(LoginActivity.this, res.getString(R.string.pls_enter_member_id));
            return false;
        } else if (etMemberid.getText().toString().trim().length() < 4) {
            appUtils.showToast(LoginActivity.this, res.getString(R.string.pls_member_id_cannot_be_smaller_than_4));
            return false;
        } else if (etPasscode.getText().toString().trim().length() < 4) {
            appUtils.showToast(LoginActivity.this, res.getString(R.string.passcode_min_character));
            return false;
        } else {
            return true;
        }
    }


//
//    /*
//   * Shows the CancelledSubscriptionDialog
//   * */
//    private void showSubscribePackageDialog(String message, String actionMessage) {
//        TextView submitTV;
//        ImageView crossIV;
//        verifyDialog = new Dialog(LoginActivity.this);
//        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        verifyDialog.setContentView(R.layout.dialog_login_screen);
//        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        verifyDialog.show();
//        verifyDialog.setCancelable(false);
//
//        ((TextView) verifyDialog.findViewById(R.id.tv_dialog_message)).setText(message);
//        submitTV = (TextView) verifyDialog.findViewById(R.id.tv_dialog_action_text);
//        submitTV.setText(actionMessage);
//
//
//        submitTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intentMain = new Intent(LoginActivity.this, AllPackageActivity.class);
//                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intentMain);
//                finish();
//            }
//        });
//
//
//        //if we close the dialog close the app
//        crossIV = (ImageView) verifyDialog.findViewById(R.id.iv_dialog_cross);
//        crossIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                verifyDialog.dismiss();
//                Intent intentMain = new Intent(LoginActivity.this, HomeActivity.class);
//                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intentMain);
//                finish();
//            }
//        });
//    }
//
//
//    /*
//   * Shows the assagesOverDialog
//   * */
//    private void showMassagesOverDialog() {
//        TextView submitTV;
//        ImageView crossIV;
//        verifyDialog = new Dialog(LoginActivity.this);
//        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        verifyDialog.setContentView(R.layout.dialog_login_screen);
//        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        verifyDialog.show();
//        verifyDialog.setCancelable(false);
//
//        ((TextView) verifyDialog.findViewById(R.id.tv_dialog_message)).setText(res.getString(R.string.your_period_session_long_text));
//        submitTV = (TextView) verifyDialog.findViewById(R.id.tv_dialog_action_text);
//        submitTV.setText(res.getString(R.string.renew_subscribe));
//        submitTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intentMain = new Intent(LoginActivity.this, AllPackageActivity.class);
//                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intentMain);
//                finish();
//            }
//        });
//
//
//        //if we close the dialog close the app
//        crossIV = (ImageView) verifyDialog.findViewById(R.id.iv_dialog_cross);
//        crossIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                verifyDialog.dismiss();
//                Intent intentMain = new Intent(LoginActivity.this, HomeActivity.class);
//                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intentMain);
//                finish();
//            }
//        });
//    }
//
//
//    /*
//       * Shows the assagesOverDialog
//       * */
//    private void showPaymentPendingDialog() {
//        TextView submitTV;
//        ImageView crossIV;
//        verifyDialog = new Dialog(LoginActivity.this);
//        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        verifyDialog.setContentView(R.layout.dialog_login_screen);
//        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        verifyDialog.show();
//        verifyDialog.setCancelable(false);
//
//        ((TextView) verifyDialog.findViewById(R.id.tv_dialog_message)).setText(res.getString(R.string.your_payment_pending_long_text));
//        submitTV = (TextView) verifyDialog.findViewById(R.id.tv_dialog_action_text);
//        submitTV.setText(res.getString(R.string.make_payment));
//        submitTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intentMain = new Intent(LoginActivity.this, Payment.class);
//                startActivity(intentMain);
//                finish();
//            }
//        });
//
//
//        //if we close the dialog close the app
//        crossIV = (ImageView) verifyDialog.findViewById(R.id.iv_dialog_cross);
//        crossIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                verifyDialog.dismiss();
//                Intent intentMain = new Intent(LoginActivity.this, HomeActivity.class);
//                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intentMain);
//                finish();
//            }
//        });
//    }


    /*
    * This method is used to create and insert data in the member table
    * */
    private void createMemberLoginTable() {

        ArrayList<MemberLoginBean> memberList = new ArrayList<>();

        MemberLoginBean m1 = new MemberLoginBean();
        m1.setUserId("1");
        m1.setMemberId("1234");
        m1.setMemberPasscode("1234");
        m1.setMemberName("Prateek");
        m1.setPackageName("Package1");
        m1.setPackageType("daily");
        m1.setPackageAmount("100");
        m1.setPackageExpiryDate("14/01/2017");
        m1.setPackageSubscriptionStatus("1");
        m1.setPackageAmountStatus("1");
        m1.setPackageMassageCount("2");
        memberList.add(m1);

        MemberLoginBean m2 = new MemberLoginBean();
        m2.setUserId("2");
        m2.setMemberId("activity_package_details");
        m2.setMemberPasscode("1234");
        m2.setMemberName("Dev Sharma");
        m2.setPackageName("Package2");
        m2.setPackageType("bi-weekly");
        m2.setPackageAmount("100");
        m2.setPackageExpiryDate("14/01/2017");
        m2.setPackageSubscriptionStatus("0");
        m2.setPackageAmountStatus("0");
        m2.setPackageMassageCount("1");
        memberList.add(m2);

        MemberLoginBean m3 = new MemberLoginBean();
        m3.setUserId("3");
        m3.setMemberId("1111");
        m3.setMemberPasscode("1234");
        m3.setMemberName("Akshay");
        m3.setPackageName("Package2");
        m3.setPackageType("weekly");
        m3.setPackageAmount("100");
        m3.setPackageExpiryDate("14/01/2017");
        m3.setPackageSubscriptionStatus("1");
        m3.setPackageAmountStatus("0");
        m3.setPackageMassageCount("2");
        memberList.add(m3);

        MemberLoginBean m4 = new MemberLoginBean();
        m4.setUserId("4");
        m4.setMemberId("2222");
        m4.setMemberPasscode("1234");
        m4.setMemberName("Naveen");
        m4.setPackageName("Package4");
        m4.setPackageType("monthly");
        m4.setPackageAmount("100");
        m4.setPackageExpiryDate("14/12/2016");
        m4.setPackageSubscriptionStatus("1");
        m4.setPackageAmountStatus("1");
        m4.setPackageMassageCount("1");
        memberList.add(m4);

        dbHelper.insertDataIntoMemberLoginTable(memberList);
    }


    /*
    * This method is used to compare the dates, if curretn date is after expiry date it returns true
    * */
    public boolean validateDate(String date) { //12/15/2016

        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
        try {
            Date d = sdf.parse(date);
            return !new Date().after(d);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


    /*
    * This method is used to find validity of member
    * */
    public void validateMemberApi() {

        final AppUtils appUtils = AppUtils.getInstance();
        appUtils.showProgressDialog(this, res.getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.PACKAGE_ID, AppSharedPreference.getString(this, Constant.PACKAGE_ID));
        params.put(Constant.USER_ID, AppSharedPreference.getString(this, Constant.USER_ID));
        params.put(Constant.ACTION, "packagestatus");

        Call<ResponseBody> call = service.checkPackageStatus(params);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                int count = 0;
                String str = "";

                try {
                    str = response.body().string();

                    ObjectMapper objectMapper = new ObjectMapper();
                    startMassagemodel = objectMapper.readValue(str, StartMassageBean.class);

                    if (startMassagemodel.getCODE().toString().equalsIgnoreCase("200")) {

                        if (startMassagemodel.getVALUE().getIsUse() == 1) {
                            appUtils.showDialog(LoginActivity.this, res.getString(R.string.your_massage_has_been_taken), res.getString(R.string.ok), 1);
                        } else if (startMassagemodel.getVALUE().getIsUse() == 0) {

                            AppSharedPreference.putBoolean(LoginActivity.this, Constant.IS_USER, true);
                            AppSharedPreference.putBoolean(LoginActivity.this, Constant.IS_GUEST, false);
                            AppSharedPreference.putBoolean(LoginActivity.this, Constant.IS_MANAGER, false);

//                            Intent intentMain = new Intent(LoginActivity.this, MassageSplashActivity.class);
//                            intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(intentMain);
//                            finish();

                        }
                    }
                } catch (Exception e1) {
                    appUtils.hideProgressDialog();
                    appUtils.showToast(LoginActivity.this, res.getString(R.string.something_went_wrong));
                    e1.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(LoginActivity.this, res.getString(R.string.something_went_wrong));
            }
        });

    }


//
//    /*
//    * Shows the MemberShipExpireDialog
//    * */
//    private void showMassageCompletionDialog(String message) {
//
//        TextView submitTV;
//        ImageView crossIV;
//        verifyDialog = new Dialog(LoginActivity.this);
//        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        verifyDialog.setContentView(R.layout.dialog_login_screen);
//        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        verifyDialog.show();
//        verifyDialog.setCancelable(false);
//
//        ((TextView) verifyDialog.findViewById(R.id.tv_dialog_message)).setText(message);
//        submitTV = (TextView) verifyDialog.findViewById(R.id.tv_dialog_action_text);
//        submitTV.setText(R.string.ok);
//        submitTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intentMain = new Intent(LoginActivity.this, HomeActivity.class);
//                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intentMain);
//                finish();
//            }
//        });
//
//
//
//        //if we close the dialog close the app
//        crossIV = (ImageView) verifyDialog.findViewById(R.id.iv_dialog_cross);
//        crossIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                verifyDialog.dismiss();
//                Intent intentMain = new Intent(LoginActivity.this, HomeActivity.class);
//                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intentMain);
//                finish();
//            }
//        });
//    }
}

