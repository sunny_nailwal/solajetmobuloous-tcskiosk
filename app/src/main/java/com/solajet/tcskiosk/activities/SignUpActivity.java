package com.solajet.tcskiosk.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.TouchScreenEditText;
import customviews.TouchScreenTextView;
import interfaces.ApiInterface;
import models.SignUpModel.SignUpModel;
import models.SubscribedPackageModel.SubscribedPackageModel;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;
import utils.NetworkConnectionIndicator;

/**
 * Created by admin1 on 26/10/16.
 */

public class SignUpActivity extends AppCompatActivity implements View.OnFocusChangeListener {


    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    @BindView(R.id.iv_first_name)
    ImageView ivFirstName;
    @BindView(R.id.et_first_name)
    EditText etFirstname;
    @BindView(R.id.iv_member_first_name)
    ImageView ivMember;
    @BindView(R.id.et_memberid)
    EditText etMemberid;
    @BindView(R.id.iv_passcode)
    ImageView ivPasscode;
    @BindView(R.id.et_passcode)
    EditText etPasscode;
    @BindView(R.id.iv_repasscode)
    ImageView ivRepasscode;
    @BindView(R.id.et_repasscode)
    EditText etRepasscode;
    @BindView(R.id.iv_mobile)
    ImageView ivMobile;
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @BindView(R.id.tv_signup)
    TextView tvSignup;
    @BindView(R.id.ll_rootView)
    LinearLayout llRootView;
    @BindView(R.id.iv_manager_id)
    ImageView ivEmail;
    @BindView(R.id.et_manager_id)
    EditText etEmail;
    @BindView(R.id.toolbar_title)
    TouchScreenTextView toolbarTitle;
    @BindView(R.id.iv_toolbar_left_icon)
    ImageView ivToolbarLeftIcon;
    @BindView(R.id.iv_cross)
    ImageView ivCross;
    @BindView(R.id.iv_member_last_name)
    ImageView ivMemberLastName;
    @BindView(R.id.et_last_name)
    TouchScreenEditText etLastName;
    private AppUtils appUtils;
    private NetworkConnectionIndicator networkConnectionIndicator;
    private SubscribedPackageModel mSubscribedPackageModel;
    private String packageid;
    private String packageSessionTime;
    private String packageAmount;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        initView();
    }


    private void initView() {

        toolbarTitle.setText(R.string.signup_text);
        ivCross.setVisibility(View.GONE);
        etFirstname.setOnFocusChangeListener(this);
        etMemberid.setOnFocusChangeListener(this);
        etPasscode.setOnFocusChangeListener(this);
        etRepasscode.setOnFocusChangeListener(this);
        etMobile.setOnFocusChangeListener(this);
        etEmail.setOnFocusChangeListener(this);
        appUtils = AppUtils.getInstance();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);
        Intent intent = getIntent();
        if (intent.getExtras().getString(Constant.PACKAGE_ID) != null) {
            packageid = getIntent().getExtras().getString(Constant.PACKAGE_ID);
        }
        if (intent.getExtras().getString(Constant.PACKAGE_SESSION_TIME) != null) {
            packageSessionTime = getIntent().getExtras().getString(Constant.PACKAGE_SESSION_TIME);
        }
        if (intent.getExtras().getString(Constant.PACKAGE_AMOUNT) != null) {
            packageAmount = getIntent().getExtras().getString(Constant.PACKAGE_AMOUNT);
        }
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_DONE) {
                    appUtils.hideNativeKeyboard(SignUpActivity.this);
                    if (networkConnectionIndicator.isNetworkAvailable(true)) {
                        if (validate()) {
                            signUpApi();
                        }
                    } else {
                        appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.pls_check_internet));
                    }
                }
                return false;
            }
        });
    }


    /*
    * This method is used for hitting signup api
    *
    * */
    public void signUpApi() {

        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));
        final ApiInterface service = RestApi.createService(ApiInterface.class);
//        Bitmap bMap = ((SignatureView) sign_view).getSignatureBitmap();
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bMap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
//        //bMap is the bitmap object
//        byte[] b = baos.toByteArray();
//        int i = b.length;
//        Log.e("the output is", "" + i);
//        String IMAGE = Base64.encodeToString(b, Base64.DEFAULT);

        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.ACTION, "normal");
        params.put(Constant.USER_NAME, etFirstname.getText().toString().trim());
        params.put(Constant.LAST_NAME, etLastName.getText().toString().trim());
        params.put(Constant.MEMBER_ID, etMemberid.getText().toString().trim());
        params.put(Constant.MEMBER_PASSCODE, etPasscode.getText().toString().trim());
        params.put(Constant.MEMBER_PHONE_NUMBER, etMobile.getText().toString().trim());
        params.put(Constant.OWNER_ID, AppSharedPreference.getString(SignUpActivity.this, Constant.OWNER_ID));
        params.put(Constant.USER_EMAIL_ID, etEmail.getText().toString().trim());
//        params.put(Constant.SIGNATURE, IMAGE);

        Call<ResponseBody> call = service.signUp(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {

                        str = response.body().string();

                        ObjectMapper mapper = new ObjectMapper();
                        SignUpModel signUpModel = mapper.readValue(str, SignUpModel.class);

                        if (signUpModel.getCODE().toString().equals("200")) {

                            AppSharedPreference.putBoolean(SignUpActivity.this, Constant.IS_USER, true);
                            AppSharedPreference.putBoolean(SignUpActivity.this, Constant.IS_GUEST, false);
                            AppSharedPreference.putBoolean(SignUpActivity.this, Constant.IS_MANAGER, false);

                            AppSharedPreference.putString(SignUpActivity.this, Constant.USER_NAME, signUpModel.getVALUE().getUserdetail().getFullName());
                            AppSharedPreference.putString(SignUpActivity.this, Constant.LAST_NAME, signUpModel.getVALUE().getUserdetail().getLastName());
                            AppSharedPreference.putString(SignUpActivity.this, Constant.USER_ID, signUpModel.getVALUE().getUserdetail().getUserId().toString());
                            AppSharedPreference.putString(SignUpActivity.this, Constant.MEMBER_ID, signUpModel.getVALUE().getUserdetail().getMemberId());

//                            Intent intent = new Intent(SignUpActivity.this, TermsAndCondition.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                            hitSubscriptionApi();

                            Intent intent = new Intent(SignUpActivity.this, TermsAndCondition.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(Constant.PACKAGE_ID, packageid);
                            intent.putExtra(Constant.PACKAGE_SESSION_TIME, packageSessionTime);
                            intent.putExtra(Constant.PACKAGE_AMOUNT, packageAmount);
                            startActivity(intent);

                        } else {
                            appUtils.showToast(SignUpActivity.this, signUpModel.getMESSAGE());
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();

                final Snackbar snackbar = Snackbar.make(llRootView, getResources().getString(R.string.something_went_wrong), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (networkConnectionIndicator.isNetworkAvailable(true)) {
                                    if (validate()) {
                                        signUpApi();
                                    }
                                } else {
                                    appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.pls_check_internet));
                                }
                            }
                        });
                snackbar.show();
            }
        });
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.et_first_name:
                if (hasFocus) {
                    ivFirstName.setImageResource(R.drawable.ic_tablet_signup_user_select);

                } else {
                    ivFirstName.setImageResource(R.drawable.ic_tablet_signup_user_deselect);
                }
                break;

            case R.id.et_last_name:
                if (hasFocus) {
                    ivMemberLastName.setImageResource(R.drawable.ic_tablet_signup_user_select);

                } else {
                    ivMemberLastName.setImageResource(R.drawable.ic_tablet_signup_user_deselect);
                }
                break;


            case R.id.et_memberid:
                if (hasFocus) {
                    ivMember.setImageResource(R.drawable.ic_tablet_signup_member_id_select);

                } else {
                    ivMember.setImageResource(R.drawable.ic_tablet_signup_member_id_deselect);
                }
                break;
            case R.id.et_passcode:
                if (hasFocus) {
                    ivPasscode.setImageResource(R.drawable.ic_tablet_signup_lock_select);

                } else {
                    ivPasscode.setImageResource(R.drawable.ic_tablet_signup_lock_deselect);
                }
                break;
            case R.id.et_repasscode:
                if (hasFocus) {
                    ivRepasscode.setImageResource(R.drawable.ic_tablet_signup_lock_select);

                } else {
                    ivRepasscode.setImageResource(R.drawable.ic_tablet_signup_lock_deselect);
                }
                break;

            case R.id.et_manager_id:
                if (hasFocus) {
                    ivEmail.setImageResource(R.drawable.ic_tablet_signup_email_select);
                } else {
                    ivEmail.setImageResource(R.drawable.ic_tablet_signup_email_deselect);
                }
                break;

            default:
                if (hasFocus) {
                    ivMobile.setImageResource(R.drawable.ic_tablet_signup_mobile_select);

                } else {
                    ivMobile.setImageResource(R.drawable.ic_tablet_signup_mobile_deselect);
                }
                break;
        }
    }




    /*
    * validate user input fields
    *
    * */

    private boolean validate() {

        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (!appUtils.validate(etFirstname.getText().toString().trim())) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.pls_enter_full_name));
            return false;
        } else if (etFirstname.getText().toString().trim().length() < 2) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.full_name_cannot_be));
            return false;
        } else if (!appUtils.validate(etMemberid.getText().toString().trim())) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.pls_enter_member_id));
            return false;
        } else if (etMemberid.getText().toString().length() < 4) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.pls_member_id_cannot_be_smaller_than_4));
            return false;
        } else if (etPasscode.getText().toString().trim().length() == 0) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.passcode_cannot_be_empty));
            return false;
        } else if (!etPasscode.getText().toString().trim().equals(etRepasscode.getText().toString().trim())) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.pass_repass_does_not_match));
            return false;
        } else if (!appUtils.isValidNumber(etMobile.getText().toString().trim())) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.pls_enter_valid_number));
            return false;
        } else if (etRepasscode.getText().toString().trim().length() < 4 || etPasscode.getText().toString().trim().length() < 4) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.passcode_min_character));
            return false;
        } else if (etEmail.getText().toString().trim().length() == 0) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.email_cannot_be_empty));
            return false;
        } else if (etEmail.getText().toString().trim().length() < 3 || etEmail.getText().toString().trim().length() > 265) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.enter_valid_email));
            return false;
        } else if (!etEmail.getText().toString().matches(EMAIL_PATTERN)) {
            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.enter_valid_email));
            return false;
        }
//        else if (!((SignatureView) sign_view).isSigned()) {
//            appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.please_sign));
//            return false;
//        }
        else {
            return true;
        }
    }

    @OnClick({R.id.iv_toolbar_left_icon, R.id.tv_signup, R.id.ll_rootView})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_toolbar_left_icon:
                appUtils.hideNativeKeyboard(SignUpActivity.this);
                finish();
                break;
            case R.id.tv_signup:
                appUtils.hideNativeKeyboard(this);
                if (networkConnectionIndicator.isNetworkAvailable(true)) {
                    if (validate()) {

                        signUpApi();
                    }
                } else {
                    appUtils.showToast(this, getResources().getString(R.string.pls_check_internet));
                }
                break;
            case R.id.ll_rootView:
                appUtils.hideNativeKeyboard(SignUpActivity.this);
                break;
        }
    }


    /*
    * This method is sued to hit the package Subcription Api
    */
    private void hitSubscriptionApi() {

        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.ACTION, "subscribepackage");
        params.put(Constant.PACKAGE_ID, packageid);
        params.put(Constant.USER_ID, AppSharedPreference.getString(this, Constant.USER_ID));

        Call<ResponseBody> call = service.subscribe(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {
                        str = response.body().string();

                        ObjectMapper mapper = new ObjectMapper();
                        mSubscribedPackageModel = mapper.readValue(str, SubscribedPackageModel.class);

                        if (mSubscribedPackageModel.getCODE().toString().equals("200")) {
                            AppSharedPreference.putBoolean(SignUpActivity.this, Constant.IS_USER, true);
                            AppSharedPreference.putBoolean(SignUpActivity.this, Constant.IS_GUEST, false);
                            AppSharedPreference.putBoolean(SignUpActivity.this, Constant.IS_MANAGER, false);
                            AppSharedPreference.putString(SignUpActivity.this, Constant.PACKAGE_ID, packageid);
                            AppSharedPreference.putString(SignUpActivity.this, Constant.PACKAGE_SESSION_TIME, packageSessionTime);
                            AppSharedPreference.putString(SignUpActivity.this, Constant.PACKAGE_AMOUNT, packageAmount);
                            AppSharedPreference.putString(SignUpActivity.this, Constant.SUBSCRIPTION_ID, String.valueOf(mSubscribedPackageModel.getVALUE().getSubscribeId()));

                            appUtils.showToast(SignUpActivity.this, mSubscribedPackageModel.getMESSAGE());
//                            startActivity(new Intent(SignUpActivity.this, TermsAndCondition.class));

                            Intent intent = new Intent(SignUpActivity.this, TermsAndCondition.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(Constant.PACKAGE_ID, packageid);
                            intent.putExtra(Constant.PACKAGE_SESSION_TIME, packageSessionTime);
                            intent.putExtra(Constant.PACKAGE_AMOUNT, packageAmount);
                            startActivity(intent);
                        }


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.something_went_wrong));
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(SignUpActivity.this, getResources().getString(R.string.something_went_wrong));

            }
        });
    }
}
