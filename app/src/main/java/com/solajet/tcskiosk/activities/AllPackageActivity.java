package com.solajet.tcskiosk.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import adapter.AllPackageAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.RecyclerViewItemDecorator;
import customviews.TouchScreenTextView;
import interfaces.ApiInterface;
import models.AllPackagesModel.AllPackageModel;
import models.AllPackagesModel.VALUE;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;
import utils.NetworkConnectionIndicator;

import static android.view.View.GONE;

/**
 * Created by admin1 on 27/10/16.
 */

public class AllPackageActivity extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    @BindView(R.id.tv_header)
    TouchScreenTextView tvHeader;
    @BindView(R.id.recycler)
    RecyclerView rvPackages;
    @BindView(R.id.ll_rootView)
    LinearLayout llRootView;
    @BindView(R.id.tv_no_packages_available)
    TouchScreenTextView tvNoPackagesAvailable;
    private List<VALUE> packageModelsList;
    private AllPackageModel allPackageModel;
    private AllPackageAdapter allAllPackageAdapter;
    private AppUtils appUtils;
    private NetworkConnectionIndicator networkConnectionIndicator;
    private boolean subscribe;
    private boolean change;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectpackage);

        ButterKnife.bind(this);
        packageModelsList = new ArrayList<>();
        if (getIntent().getStringExtra("subscribe") != null && getIntent().getStringExtra("subscribe").equalsIgnoreCase("subscribe")) {
            subscribe = true;
        } else if (getIntent().getStringExtra("change_package") != null && getIntent().getStringExtra("change_package").equalsIgnoreCase("change_package")) {
            change = true;
        }
//        this.startLockTask();
        initView();
    }


    private void initView() {
        tvHeader.setText(getResources().getString(R.string.welcome_to) + " " + AppSharedPreference.getString(this, Constant.CLUB_NAME));

        appUtils = AppUtils.getInstance();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);
        getPackages();
    }

    public boolean getSubscription() {
        return subscribe;
    }

    public boolean getChange() {
        return change;
    }


    /*
    * fetch all packages from  the server
    *
    * */
    public void getPackages() {
        appUtils.showProgressDialog(AllPackageActivity.this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.ACTION, "packages");
        params.put(Constant.OWNER_ID, AppSharedPreference.getString(AllPackageActivity.this, Constant.OWNER_ID));


        Call<ResponseBody> call = service.getPackages(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {
                        str = response.body().string();
                        ObjectMapper mapper = new ObjectMapper();
                        allPackageModel = mapper.readValue(str, AllPackageModel.class);

                        if (allPackageModel.getCODE().toString().equals("200")) {

                            packageModelsList = allPackageModel.getVALUE();

                            if (packageModelsList.size() < 1) {
                                rvPackages.setVisibility(GONE);
                                tvNoPackagesAvailable.setVisibility(View.VISIBLE);
                            } else {
                                tvNoPackagesAvailable.setVisibility(View.GONE);
                                setDataToRecycler();
                            }

                        } else {
                            appUtils.showToast(AllPackageActivity.this, allPackageModel.getMESSAGE());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(AllPackageActivity.this, getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();

                final Snackbar snackbar = Snackbar.make(llRootView, getResources().getString(R.string.something_went_wrong), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                getPackages();
                            }
                        });
                snackbar.show();
            }
        });
    }


    /*
    * set fetched data from server to rvPackages view
    */
    private void setDataToRecycler() {

        allAllPackageAdapter = new AllPackageAdapter(this, packageModelsList);
        rvPackages.setAdapter(allAllPackageAdapter);
        rvPackages.setLayoutManager(new GridLayoutManager(this, 3));
        rvPackages.setNestedScrollingEnabled(false);
        rvPackages.setHasFixedSize(true);
        rvPackages.addItemDecoration(new RecyclerViewItemDecorator(AllPackageActivity.this));

//        rvPackages.addOnItemTouchListener(
//
//                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        Intent intent = new Intent(AllPackageActivity.this, PackageDetailsActivity.class);
//                        intent.putExtra(Constant.PACKAGE_ID, packageModelsList.get(position).getPackageId());
//                        startActivity(intent);
//                    }
//                })
//        );
    }

    @OnClick(R.id.iv_back)
    public void onClick() {
        Intent intentMain = new Intent(AllPackageActivity.this, VideoHomeActivity.class);
        intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentMain);
        finish();
    }
}
