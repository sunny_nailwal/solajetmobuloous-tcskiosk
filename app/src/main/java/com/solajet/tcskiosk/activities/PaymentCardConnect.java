package com.solajet.tcskiosk.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.solajet.tcskiosk.tcskiosk.R;
import com.stripe.android.model.Card;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.TouchScreenTextView;
import dialogs.CardSwipeDialog;
import interfaces.CardConnectApi;
import models.CardConnectModel.AuthRequest;
import models.CardConnectModel.AuthResponse;
import models.CardConnectModel.CardSwipeData;
import network.RestApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;

public class PaymentCardConnect extends AppCompatActivity {
    public static final String DIALOG_SWIPE_CARD_TAG = "DIALOG_SWIPE_CARD";
    private final String TAG = PaymentCardConnect.class.getSimpleName();


    @BindView(R.id.toolbar_title) TouchScreenTextView toolbarTitle;
    @BindView(R.id.iv_toolbar_left_icon) ImageView ivToolbarLeftIcon;
    @BindView(R.id.iv_cross) ImageView ivcross;
    @BindView(R.id.year_edt) Spinner year_edt;
    @BindView(R.id.month_edt) Spinner month_edt;
    @BindView(R.id.card_no) EditText card_no;
    @BindView(R.id.cvc_edt) EditText cvc_edt;
    @BindView(R.id.card_holder_nme) EditText card_holder_nme;
    @BindView(R.id.btn_pay) Button btn_pay;
    @BindView(R.id.package_name) TextView package_name;
    @BindView(R.id.total_amaount) TextView total_amaount;

    private String track1Data = null;
    private String track2Data = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_card_connect);
        ButterKnife.bind(this);
        initView();
    }


    public void initView() {
        ivToolbarLeftIcon.setVisibility(View.VISIBLE);
        toolbarTitle.setText("Payment");

        package_name.setText(AppSharedPreference.getString(this, Constant.PACKAGE_NAME));
        total_amaount.setText("$" + AppSharedPreference.getString(this, Constant.PACKAGE_AMOUNT));

        //YEARS
        setUpYearSpinner();

        //MONTHS
        setUpMonthSpinner();
    }

    @OnClick(R.id.iv_toolbar_left_icon)
    void onToolbarLeftIconClicked() {
        onBackPressed();
    }

    @OnClick(R.id.iv_cross)
    void onCloseClicked() {
        finish();
    }

    @OnClick(R.id.btn_swipe)
    void onSwipeClicked() {
        resetForm();
        enableForm();

        final CardSwipeDialog cardSwipeDialog = CardSwipeDialog.newInstance();
        cardSwipeDialog.setCardSwipeListener(new CardSwipeDialog.CardSwipeListener() {
            @Override
            public void onCardSwipe(CardSwipeData cardSwipeData) {
                card_holder_nme.setText(cardSwipeData.getCardHolderName());
                card_no.setText(cardSwipeData.getCardNumber());
                final int monthPosition = ((ArrayAdapter) month_edt.getAdapter()).getPosition(cardSwipeData.getExpirationMonth());
                month_edt.setSelection(monthPosition);
                final int yearPosition = ((ArrayAdapter) year_edt.getAdapter()).getPosition("20" + cardSwipeData.getExpirationYear());
                year_edt.setSelection(yearPosition);
                track1Data = cardSwipeData.getTrack1();
                track2Data = cardSwipeData.getTrack2();

                disableForm();
            }
        });

        if (getSupportFragmentManager().findFragmentByTag(DIALOG_SWIPE_CARD_TAG) == null) {
            cardSwipeDialog.show(getSupportFragmentManager(), DIALOG_SWIPE_CARD_TAG);
        }
    }

    private void disableForm() {
        card_holder_nme.setEnabled(false);
        card_no.setEnabled(false);
        month_edt.setEnabled(false);
        year_edt.setEnabled(false);
    }

    private void enableForm() {
        card_holder_nme.setEnabled(true);
        card_no.setEnabled(true);
        month_edt.setEnabled(true);
        year_edt.setEnabled(true);
    }

    private void resetForm() {
        track1Data = null;
        track2Data = null;
        card_holder_nme.setText("");
        card_no.setText("");
        cvc_edt.setText("");
        month_edt.setSelection(0);
        year_edt.setSelection(0);
    }

    @OnClick(R.id.btn_pay)
    void onPayClicked() {
        if (validation()) {

            final String ccNumber = card_no.getText().toString().trim();
            final int expMonth = Integer.parseInt(month_edt.getSelectedItem().toString().trim());
            final int expYear = Integer.parseInt(year_edt.getSelectedItem().toString());
            final String cvc = cvc_edt.getText().toString().trim();
            final String cardHolderName = card_holder_nme.getText().toString().trim();

            //Use stripe card class for validation
            final Card card = new Card(ccNumber, expMonth, expYear, cvc);

            if (!card.validateCard()) {
                Toast.makeText(PaymentCardConnect.this, "Invalid card", Toast.LENGTH_SHORT).show();
                return;
            }

            //Send auth request to CardConnect
            sendAuth(ccNumber, expMonth, expYear, cvc, cardHolderName);
        }
    }

    void sendAuth(String cardNumber, int month, int year, String cvc, String cardHolderName) {
        final AppUtils appUtils = AppUtils.getInstance();
        final CardConnectApi cardConnectApi = RestApi.createCardConnectService();
        final String amount = AppSharedPreference.getString(this, Constant.PACKAGE_AMOUNT);

        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));

        //!!! Assumption: Amount is always a dollar amount with no cents portion, add 00 to make it a value in cents
        final AuthRequest authRequest = new AuthRequest.Builder()
                .setAccount(cardNumber)
                .setExpiry(String.format(Locale.getDefault(), "%02d%02d", month, year - 2000))
                .setAmount(amount + "00")
                .setCvv2(cvc)
                .setName(cardHolderName)
                .setTrack1(track1Data)
                .setTrack2(track2Data)
                .build();

        final Call<AuthResponse> authorizeCall = cardConnectApi.authorize(Constant.CARD_CONNECT_AUTH_HEADER, authRequest);

        authorizeCall.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                appUtils.hideProgressDialog();

                try {
                    if (response.isSuccessful()) {
                        final AuthResponse authResponse = response.body();

                        switch (authResponse.getResponseStatus()) {
                            case APPROVED:
                                AppSharedPreference.putBoolean(PaymentCardConnect.this, Constant.IS_PAYMENT, true);
                                handleApproved();
                                break;
                            case RETRY:
                                break;
                            case DECLINED:
                                break;
                        }

                        Toast.makeText(PaymentCardConnect.this, "" + authResponse.getResptext(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    appUtils.showToast(PaymentCardConnect.this, getResources().getString(R.string.something_went_wrong));
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(PaymentCardConnect.this, getString(R.string.something_went_wrong));
                t.printStackTrace();
            }
        });
    }

    private void handleApproved() {
        Intent intentMain = new Intent(this, VideoHomeActivity.class);
        intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentMain);
        finish();
    }




    private boolean validation() {
        boolean flag = true;


        if (card_no.getText().toString().trim().equals("")) {
            Toast.makeText(PaymentCardConnect.this, "Please enter card number", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (month_edt.getSelectedItem().toString().trim().equalsIgnoreCase("MM")) {
            Toast.makeText(PaymentCardConnect.this, "Please enter Month", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (year_edt.getSelectedItem().toString().trim().equalsIgnoreCase("YY")) {
            Toast.makeText(PaymentCardConnect.this, "Please enter year", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (cvc_edt.getText().toString().trim().equals("")) {
            Toast.makeText(PaymentCardConnect.this, "Please enter cvv", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (card_holder_nme.getText().toString().trim().equals("")) {
            Toast.makeText(PaymentCardConnect.this, "Please enter Card holder name", Toast.LENGTH_LONG).show();
            flag = false;
        }
        return flag;
    }


    private void setUpYearSpinner() {
        String[] spinnerItems = new String[]{
                "YY",
                "2018",
                "2019",
                "2020",
                "2021",
                "2022",
                "2023",
                "2024",
                "2025",
                "2026",
                "2028",
                "2029",
                "2030",
                "2031",
                "2032",
                "2033",
                "2034",
                "2035",
                "2036",
                "2037",
                "2038",
                "2039",
                "2040",
                "2041",
                "2042",
                "2043",
                "2044",
                "2045",
                "2046",
                "2047",
                "2048",
                "2049",
                "2050"};


        List<String> spinnerList = new ArrayList<>(Arrays.asList(spinnerItems));

        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(PaymentCardConnect.this, R.layout.spinner_drop_singlerow, spinnerList) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            hideKeyboard(PaymentCardConnect.this);
                            //Disable the first item of spinner.
                            Log.i(TAG, "Position[0]: Spinner first item is disabled");
                            return false;
                        } else {
                            hideKeyboard(PaymentCardConnect.this);
                            String itemSelected = year_edt.getItemAtPosition(position).toString();
                            Log.i(TAG, "Selected Item[" + position + "]: " + itemSelected);
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                        View spinnerView = super.getDropDownView(position, convertView, parent);

                        TextView spinnerTextV = (TextView) spinnerView;
                        if (position == 0) {
                            //Set the disable spinner item color fade .
                            spinnerTextV.setTextColor(Color.parseColor("#a7a7a6"));
                        } else {
                            //#404040
                            spinnerTextV.setTextColor(Color.parseColor("#b2000000"));
                        }
                        return spinnerView;
                    }
                };

        arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_singlerow);

        year_edt.setAdapter(arrayAdapter);

    }

    private void setUpMonthSpinner() {
        String[] spinnerItems = new String[]{
                "MM",
                "01",
                "02",
                "03",
                "04",
                "05",
                "06",
                "07",
                "08",
                "09",
                "10",
                "11",
                "12"};


        List<String> spinnerList = new ArrayList<>(Arrays.asList(spinnerItems));

        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(PaymentCardConnect.this, R.layout.spinner_drop_singlerow, spinnerList) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            hideKeyboard(PaymentCardConnect.this);
                            //Disable the first item of spinner.
                            Log.i(TAG, "Position[0]: Spinner first item is disabled");
                            return false;
                        } else {
                            hideKeyboard(PaymentCardConnect.this);
                            String itemSelected = month_edt.getItemAtPosition(position).toString();
                            Log.i(TAG, "Selected Item[" + position + "]: " + itemSelected);
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                        View spinnerView = super.getDropDownView(position, convertView, parent);

                        TextView spinnerTextV = (TextView) spinnerView;
                        if (position == 0) {
                            //Set the disable spinner item color fade .
                            spinnerTextV.setTextColor(Color.parseColor("#a7a7a6"));
                        } else {
                            //#404040
                            spinnerTextV.setTextColor(Color.parseColor("#b2000000"));
                        }
                        return spinnerView;
                    }
                };

        arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_singlerow);

        month_edt.setAdapter(arrayAdapter);
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
