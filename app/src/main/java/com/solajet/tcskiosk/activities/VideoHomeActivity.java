package com.solajet.tcskiosk.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import adapter.PackageVideoAdapter;
import adapter.QuestionAdapter;
import adapter.TextPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.RecyclerViewItemDecorator;
import interfaces.ApiInterface;
import models.AllPackagesModel.AllPackageModel;
import models.AllPackagesModel.VALUE;
import models.LoginModel.LoginModel;
import models.QuestionsModel;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.AutoScrollViewPager;
import utils.Constant;
import utils.NetworkConnectionIndicator;

import static android.view.View.GONE;

public class VideoHomeActivity extends AppCompatActivity {
    private Dialog thankDialog;
    private Button btnonce, btnstop;
    private VideoView vv;
    private MediaController mediacontroller;
    private Uri uri;
    private boolean isContinuously = false;
    private ProgressBar progressBar;
    private String packageid, packageSessionTime, packageAmount;
    private AppUtils appUtils;
    private NetworkConnectionIndicator networkConnectionIndicator;
    private QuestionAdapter questionAdapter;

    private AutoScrollViewPager viewPager;
    private TextView indexText;
    private AllPackageModel allPackageModel;
    private PackageVideoAdapter allAllPackageAdapter;
    private List<VALUE> packageModelsList;
    private Dialog UnlockDialog;
    private boolean isTouch = false;
    private CountDownTimer countDownTime;


    private TextView btn_explore;

    private List<String> imageIdList;
    private TabLayout tabs;
    @BindView(R.id.tv_more_info)
    TextView tv_more_info;

    @BindView(R.id.tv_signup)
    TextView tv_signup;

    @BindView(R.id.tv_free_session)
    TextView tv_free_session;

    @BindView(R.id.tv_how_work)
    TextView tv_how_work;

    @BindView(R.id.rl_free_session)
    RelativeLayout rl_free_session;

    @BindView(R.id.rl_signup)
    RelativeLayout rl_signup;

    @BindView(R.id.ll_rootView)
    RelativeLayout llRootView;

    @BindView(R.id.rv_expandble)
    RecyclerView rvCycle;

    @BindView(R.id.recycler)
    RecyclerView rvPackages;

    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.edt_phn)
    EditText edt_phn;

    @BindView(R.id.tv_save)
    TextView tv_save;

    private boolean isShowVideo = false;

    private CopyOnWriteArrayList<QuestionsModel> questionsModelList;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_home);
        ButterKnife.bind(this);

        isShowVideo = true;
        vv = (VideoView) findViewById(R.id.vv);
        rvCycle.setVisibility(View.GONE);
        imageIdList = new ArrayList<String>();
        questionsModelList = new CopyOnWriteArrayList<>();
        questionAdapter = new QuestionAdapter(questionsModelList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvCycle.setLayoutManager(layoutManager);
        rvCycle.setAdapter(questionAdapter);

        ((Button) findViewById(R.id.cancel)).setTypeface(Typeface.createFromAsset(getAssets(), "ProximaNova-Light.otf"));

        imageIdList.add("What Every Body Wants");
        imageIdList.add("FEEL BETTER FAST");
        imageIdList.add("Making the World Feel Better");
        imageIdList.add("LIVE INSPIRED!");
        imageIdList.add("The World’s #1 Massage System");
        imageIdList.add("Feel Better Perform Better");
        imageIdList.add("The Pain Eraser!");
        imageIdList.add("Simply the Best Massage!");
        imageIdList.add("The Perfect PRE or POST workout Partner!");


        llRootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isTouch = true;

                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }

                return false;
            }
        });


        edtName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                isTouch = true;

                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }
                return false;
            }
        });

        edt_phn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                isTouch = true;

                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }
                return false;
            }
        });


        VideoView view = vv;
        String path = "android.resource://" + getPackageName() + "/" + R.raw.solajet;
        uri = Uri.parse(path);
        view.setVideoURI(uri);
        view.start();


        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

//                Toast.makeText(VideoHomeActivity.this,"Done", Toast.LENGTH_SHORT).show();
//                  rewardVideoService();
                if (isContinuously) {
                    vv.start();
                }
            }
        });


        initView(imageIdList);
        prepareData();



        if (AppSharedPreference.getBoolean(VideoHomeActivity.this,Constant.IS_PAYMENT)){
            showThanksDialog();
        }


    }


    public void showThanksDialog() {
        thankDialog = new Dialog(VideoHomeActivity.this);
        thankDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        thankDialog.setContentView(R.layout.payment_sucess_dialog_layout);
        thankDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        thankDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        thankDialog.show();
        thankDialog.setCancelable(false);

        TextView show_msg = (TextView) thankDialog.findViewById(R.id.touchScreenTextView);
        TextView no_btn = (TextView) thankDialog.findViewById(R.id.tv_okclick);

        AppSharedPreference.putBoolean(VideoHomeActivity.this, Constant.IS_PAYMENT, false);

        show_msg.setText("Payment Successful");

        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thankDialog.dismiss();
            }
        });
    }



    private void prepareData() {
        questionsModelList.add(new QuestionsModel("What is the cost?", "Packages are designed to be affordable for most any budget. See pricing tab for details."));
        questionsModelList.add(new QuestionsModel("How long to sessions take?", "Sessions generally range from 10-20 minutes depending on packages selected. Many describe 10 minutes on SolaJet as giving them the feeling and effect of a 30-minute massage."));
        questionsModelList.add(new QuestionsModel("What are the benefits?", "Massage has numerous benefits which help normalize muscles and soft tissue. Some benefits include... improved circulation, lymphatic stimulation, pain reduction, improved range of motion, endorphin release, and muscle conditioning and recovery to name just a few. SolaJet is designed to help you feel better fast!"));
        questionsModelList.add(new QuestionsModel("Are there any restrictions on who can use?", "Any persons who have had surgery within the last 10 days, unstable spinal conditions and individuals who are pregnant should consult their physician prior to use."));

    }


    private void setupTabLayout() {
        for (int i = 0; i < imageIdList.size(); i++) {
            tabs.addTab(tabs.newTab());
        }

        for (int i = 0; i < tabs.getTabCount(); i++) {
            View tab = ((ViewGroup) tabs.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 50, 50, 0);
            tab.setLayoutParams(p);
            tab.requestLayout();
        }
    }

    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
//            indexText.setText("2");
            if (tabs.getTabAt(position) != null) {
                tabs.getTabAt(position).select();
            }

        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // stop auto scroll when onPause
        viewPager.stopAutoScroll();
        vv.pause();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
//            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
//        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (isShowVideo == true) {
            isContinuously = true;
            progressBar.setVisibility(View.GONE);
            //vv.setMediaController(mediacontroller);
            vv.setVideoURI(uri);
            vv.requestFocus();
            vv.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isShowVideo == true) {
            // vv.setMediaController(mediacontroller);
            vv.setVideoURI(uri);
            vv.requestFocus();
            vv.resume();
        }

        viewPager.startAutoScroll();

    }

//    @Override
//    protected void onPause() {
//        vv.pause();
//        super.onPause();
//    }


    private void initView(List<String> imageIdList) {


//        imageIdList.add("Don’t you deserve comfort?");
//        imageIdList.add("Free 1st session");
//        imageIdList.add("Pre & Post work out benefits");
//        imageIdList.add("You work hard on your, why not on it’s recovery");
//        imageIdList.add(R.drawable.pic_five);
        tabs = (TabLayout) findViewById(R.id.tabs);
        setupTabLayout();
        viewPager = (AutoScrollViewPager) findViewById(R.id.view_pager);
        btn_explore = (TextView) findViewById(R.id.btn_explore);
        viewPager.setAdapter(new TextPagerAdapter(this, imageIdList).setInfiniteLoop(false));
        viewPager.addOnPageChangeListener(new MyOnPageChangeListener());
        tabs.setupWithViewPager(viewPager);

        appUtils = AppUtils.getInstance();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);
        viewPager.setInterval(5000);
        viewPager.startAutoScroll(5000);


        progressBar = (ProgressBar) findViewById(R.id.progrss);
        btnonce = (Button) findViewById(R.id.btnonce);
        btnstop = (Button) findViewById(R.id.btnstop);
        vv = (VideoView) findViewById(R.id.vv);


        //Indicator count////
        Intent intent = getIntent();

//        if (intent.getExtras().getString(Constant.PACKAGE_ID) != null) {
//            packageid = getIntent().getExtras().getString(Constant.PACKAGE_ID);
//        }

        appUtils = AppUtils.getInstance();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);


//        findViewById(R.id.tv_signup_txt).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                startActivity(new Intent(VideoHomeActivity.this, SignUpActivity.class).putExtra(Constant.PACKAGE_ID, packageid).putExtra(Constant.PACKAGE_SESSION_TIME, packageSessionTime).putExtra(Constant.PACKAGE_AMOUNT, packageAmount));
//            }
//        });

        // videoSetup();
    }

    @OnClick({R.id.tv_how_work, R.id.tv_free_session, R.id.tv_more_info, R.id.tv_signup, R.id.tv_save, R.id.cancel})
    void onClick(View view) {

        switch (view.getId()) {

            case R.id.tv_how_work:
                isShowVideo = true;
                initialState();
                howWorkClick();
                isTouch = true;
                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }

                tv_how_work.setBackgroundColor(ContextCompat.getColor(VideoHomeActivity.this, R.color.color_selected_rv_bg));

                break;

            case R.id.tv_more_info:
                isShowVideo = false;
                initialState();
                moreInfoClick();

                isTouch = true;
                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }

                tv_more_info.setBackgroundColor(ContextCompat.getColor(VideoHomeActivity.this, R.color.color_selected_rv_bg));

                break;

            case R.id.tv_free_session:
                isShowVideo = false;
                initialState();
                freeSessionClick();

                isTouch = true;
                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }

                tv_free_session.setBackgroundColor(ContextCompat.getColor(VideoHomeActivity.this, R.color.color_selected_rv_bg));

                break;

            case R.id.tv_signup:


                isTouch = true;
                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }

                startActivity(new Intent(VideoHomeActivity.this, HomeActivity.class).putExtra(Constant.PACKAGE_ID, packageid).putExtra(Constant.PACKAGE_SESSION_TIME, packageSessionTime).putExtra(Constant.PACKAGE_AMOUNT, packageAmount));
                //startActivity(new Intent(VideoHomeActivity.this,HomeActivity.class));

//                isShowVideo=false;
//                getPackages();
//                initialState();
//                signUpClick();
//                tv_signup.setBackgroundColor(ContextCompat.getColor(VideoHomeActivity.this, R.color.color_selected_rv_bg));

                break;

            case R.id.tv_save:
                isTouch = true;
                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }


                validation();


                edt_phn.getText().clear();
                edtName.getText().clear();

                break;

            case R.id.cancel:

                isTouch = true;
                if (countDownTime != null) {
                    countDownTime.cancel();
                    startTimer();
                } else {
                    startTimer();
                }

                startActivity(new Intent(this, AccountActivity.class));
                break;

        }
    }

    private void initialState() {
        tv_how_work.setBackgroundColor(ContextCompat.getColor(VideoHomeActivity.this, R.color.color_light_grey));
        tv_more_info.setBackgroundColor(ContextCompat.getColor(VideoHomeActivity.this, R.color.color_light_grey));
        tv_free_session.setBackgroundColor(ContextCompat.getColor(VideoHomeActivity.this, R.color.color_light_grey));
        tv_signup.setBackgroundColor(ContextCompat.getColor(VideoHomeActivity.this, R.color.color_light_grey));
    }


    private void videoSetup() {


        // mediacontroller = new MediaController(this);
        // mediacontroller.setAnchorView(vv);
        //String uriPath1 = "https://mobulous.in/vuewa/video/5248887180706020538.mp4"; //update package name


        VideoView view = vv;
        String path = "https://mobulous.in/vuewa/video/5248887180706020538.mp4"; //update package name
        uri = Uri.parse(path);
        view.setVideoURI(uri);
        view.start();

//        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                mp.setLooping(true);
//            }
//        });

        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {


//                VideoView view = vv;
//                String path = "android.resource://" + getPackageName() + "/" + R.raw.solarjetv;
//                uri = Uri.parse(path);
//                view.setVideoURI(uri);
//                view.start();
//
//                vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mp) {
//
//                        VideoView view = vv;
//                        String path = "android.resource://" + getPackageName() + "/" + R.raw.solarjetv;
//                        uri = Uri.parse(path);
//                        view.setVideoURI(uri);
//                        view.start();
//                    }
//                });

//                Toast.makeText(VideoHomeActivity.this,"Done", Toast.LENGTH_SHORT).show();
//                  rewardVideoService();
                if (isContinuously) {
                    vv.start();
                }
            }
        });

        btnstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        btnonce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isContinuously = false;
                progressBar.setVisibility(View.VISIBLE);
                // vv.setMediaController(mediacontroller);
                vv.setVideoURI(uri);
                vv.requestFocus();
                //vv.start();

            }
        });


        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);


            }
        });
    }


    private void howWorkClick() {
        rl_free_session.setVisibility(View.GONE);
        rvCycle.setVisibility(View.GONE);
        rl_signup.setVisibility(View.GONE);
        vv.setVisibility(View.VISIBLE);

        isContinuously = true;
        progressBar.setVisibility(View.GONE);
        //vv.setMediaController(mediacontroller);

        // String uriPath = "https://mobulous.in/vuewa/video/1272639180724114343.mp4"; //update package name
        // uri = Uri.parse(uriPath);

        VideoView view = vv;
        String path = "android.resource://" + getPackageName() + "/" + R.raw.solajetv;
        uri = Uri.parse(path);
        view.setVideoURI(uri);
        vv.requestFocus();
        view.start();


        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                VideoView view = vv;
                String path = "android.resource://" + getPackageName() + "/" + R.raw.solajet;
                uri = Uri.parse(path);
                view.setVideoURI(uri);
                view.start();
            }
        });


        imageIdList.clear();


        imageIdList.add(getResources().getString(R.string.how_work_txt2));


        initView(imageIdList);

    }

    private void moreInfoClick() {
        vv.setVisibility(View.GONE);
        vv.stopPlayback();
        rvCycle.setVisibility(View.VISIBLE);
        rl_free_session.setVisibility(View.GONE);
        rl_signup.setVisibility(View.GONE);

        imageIdList.clear();

        imageIdList.add(getResources().getString(R.string.more_info_txt));

        initView(imageIdList);

    }

    private void freeSessionClick() {

        vv.setVisibility(View.GONE);
        vv.stopPlayback();
        rvCycle.setVisibility(View.GONE);
        rl_free_session.setVisibility(View.VISIBLE);
        rl_signup.setVisibility(View.GONE);

        imageIdList.clear();

        imageIdList.add(getResources().getString(R.string.free_session_txt));

        initView(imageIdList);

    }

    private void signUpClick() {
        vv.setVisibility(View.GONE);
        vv.stopPlayback();
        rvCycle.setVisibility(View.GONE);
        rl_free_session.setVisibility(View.GONE);
        rl_signup.setVisibility(View.VISIBLE);

        imageIdList.clear();

        imageIdList.add(getResources().getString(R.string.signUp_now));

        initView(imageIdList);

    }


    public void getPackages() {
        appUtils.showProgressDialog(VideoHomeActivity.this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.ACTION, "packages");
        params.put(Constant.OWNER_ID, AppSharedPreference.getString(VideoHomeActivity.this, Constant.OWNER_ID));


        Call<ResponseBody> call = service.getPackages(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {
                        str = response.body().string();
                        ObjectMapper mapper = new ObjectMapper();
                        allPackageModel = mapper.readValue(str, AllPackageModel.class);

                        if (allPackageModel.getCODE().toString().equals("200")) {

                            packageModelsList = allPackageModel.getVALUE();

                            if (packageModelsList.size() < 1) {
                                rvPackages.setVisibility(GONE);

                                //tvNoPackagesAvailable.setVisibility(View.VISIBLE);
                            } else {
                                //tvNoPackagesAvailable.setVisibility(View.GONE);
                                setDataToRecycler();
                            }

                        } else {
                            appUtils.showToast(VideoHomeActivity.this, allPackageModel.getMESSAGE());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(VideoHomeActivity.this, getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();

                final Snackbar snackbar = Snackbar.make(llRootView, getResources().getString(R.string.something_went_wrong), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                getPackages();
                            }
                        });
                snackbar.show();
            }
        });
    }


    /*
     * set fetched data from server to rvPackages view
     */
    private void setDataToRecycler() {

        allAllPackageAdapter = new PackageVideoAdapter(this, packageModelsList, 6);
        rvPackages.setAdapter(allAllPackageAdapter);
        rvPackages.setLayoutManager(new LinearLayoutManager(this));
        rvPackages.setNestedScrollingEnabled(false);
        rvPackages.setHasFixedSize(true);
        rvPackages.addItemDecoration(new RecyclerViewItemDecorator(VideoHomeActivity.this));

//        rvPackages.addOnItemTouchListener(
//
//                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        Intent intent = new Intent(AllPackageActivity.this, PackageDetailsActivity.class);
//                        intent.putExtra(Constant.PACKAGE_ID, packageModelsList.get(position).getPackageId());
//                        startActivity(intent);
//                    }
//                })
//        );
    }


    private boolean validation() {

        String MobilePattern = "[0-9]{10}";
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


        if (edt_phn.length() == 0 || edtName.length() == 0) {

            Toast.makeText(getApplicationContext(), "pls fill the empty fields", Toast.LENGTH_SHORT).show();
            return false;


        } else if (!edt_phn.getText().toString().matches(emailPattern) && !edt_phn.getText().toString().matches(MobilePattern)) {

            Toast.makeText(getApplicationContext(), "Please Enter Valid Phone No", Toast.LENGTH_SHORT).show();
            return false;

        } else {

//            Toast.makeText(getApplicationContext(), "Update sucessfully", Toast.LENGTH_SHORT).show();
            //showDialog();
            //showMsgDialog();


            freeMassageService();
        }

        return false;
    }





    /*
     * hitting free Message  User api
     * */
    public void freeMassageService() {

        appUtils.showProgressDialog(VideoHomeActivity.this, getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.USER_NAME, edtName.getText().toString());
        params.put(Constant.MEMBER_PHONE_NUMBER, edt_phn.getText().toString());
        params.put(Constant.ACTION, "normal_free");

        Call<ResponseBody> call = service.signUp(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {

                        str = response.body().string();
                        ObjectMapper mapper = new ObjectMapper();
                        LoginModel loginModel = mapper.readValue(str, LoginModel.class);

                        if (loginModel.getCODE().toString().equals("200")) {

                            showMsgDialog();
                            edt_phn.getText().clear();
                            edtName.getText().clear();

                        } else {
                            showAlreadyMassageDialog();
                            appUtils.showToast(VideoHomeActivity.this, loginModel.getMESSAGE());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(VideoHomeActivity.this, getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(VideoHomeActivity.this, getString(R.string.something_went_wrong));
            }
        });

    }


    private void showAlreadyMassageDialog() {
        final Dialog dialog1 = new Dialog(this, R.style.ThemeDialogCustom);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog1.setContentView(R.layout.dialog_confirm);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog1.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        TextView show_msg = (TextView) dialog1.findViewById(R.id.touchScreenTextView);
        TextView ok_btn = (TextView) dialog1.findViewById(R.id.tv_ok);
        TextView no_btn = (TextView) dialog1.findViewById(R.id.tv_cancel);


        show_msg.setText("By this number has been already taken free massage now You want to enjoying the massage,Please subscribe the massage pakages");


        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(VideoHomeActivity.this, AllPackageActivity.class).putExtra(Constant.PACKAGE_ID, packageid).putExtra(Constant.PACKAGE_SESSION_TIME, packageSessionTime).putExtra(Constant.PACKAGE_AMOUNT, packageAmount));

                dialog1.dismiss();
            }
        });

        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });

        dialog1.show();

    }


    private void showDialog() {
        final Dialog dialog1 = new Dialog(this, R.style.ThemeDialogCustom);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog1.setContentView(R.layout.dialog_confirm);

        TextView show_msg = (TextView) dialog1.findViewById(R.id.touchScreenTextView);
        TextView no_btn = (TextView) dialog1.findViewById(R.id.tv_okclick);


        show_msg.setText("Thank you and enjoy First 6 digits of their phone number is the login and the last 4 are the password ");


        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });

        dialog1.show();

    }


    public void showMsgDialog() {
        UnlockDialog = new Dialog(VideoHomeActivity.this);
        UnlockDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        UnlockDialog.setContentView(R.layout.free_session_dialog);
        UnlockDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        UnlockDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        UnlockDialog.show();
        UnlockDialog.setCancelable(false);

        TextView show_msg = (TextView) UnlockDialog.findViewById(R.id.touchScreenTextView);
        TextView no_btn = (TextView) UnlockDialog.findViewById(R.id.tv_okclick);


        show_msg.setText("Thank you. Please take go to the massage bed and enter your phone number as member id and last 4 digits of phone number as password for a single massage.");

        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnlockDialog.dismiss();
            }
        });
    }


    private void startTimer() {

        countDownTime = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {


            }

            public void onFinish() {

                hideKeyboard(VideoHomeActivity.this);
                initialState();
                isTouch = false;

                rl_free_session.setVisibility(View.GONE);
                rvCycle.setVisibility(View.GONE);
                rl_signup.setVisibility(View.GONE);
                vv.setVisibility(View.VISIBLE);

                VideoView view = vv;
                String path = "android.resource://" + getPackageName() + "/" + R.raw.solajet;
                uri = Uri.parse(path);
                view.setVideoURI(uri);
                progressBar.setVisibility(View.GONE);
                view.start();

                imageIdList.clear();

                imageIdList.add("What Every Body Wants");
                imageIdList.add("FEEL BETTER FAST");
                imageIdList.add("Making the World Feel Better");
                imageIdList.add("LIVE INSPIRED!");
                imageIdList.add("The World’s #1 Massage System");
                imageIdList.add("Feel Better Perform Better");
                imageIdList.add("The Pain Eraser!");
                imageIdList.add("Simply the Best Massage!");
                imageIdList.add("The Perfect PRE or POST workout Partner!");

                initView(imageIdList);

            }

        }.start();

    }




    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
