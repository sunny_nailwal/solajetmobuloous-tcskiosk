package com.solajet.tcskiosk.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import adapter.ImagePagerAdapter;
import interfaces.ApiInterface;
import models.VerifyOwneridModel.VerifyOwnerIdModel;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.AutoScrollViewPager;
import utils.Constant;
import utils.NetworkConnectionIndicator;

/**
 * ScrollingSplashActivity
 * 
 * @author <a href="http://www.trinea.cn" target="_blank">Trinea</a> 2014-2-22
 */
public class ScrollingSplashActivity extends AppCompatActivity {

    private AutoScrollViewPager viewPager;
    private TextView indexText;

    private TextView btn_explore;

    private List<Integer> imageIdList;
    private TabLayout tabs;
    private Dialog verifyDialog;
    private EditText tvOwnerId;
    private NetworkConnectionIndicator networkConnectionIndicator;
    private AppUtils appUtils;
    private TextView tvSubmit;
    private ImageView ivCross;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auto_scroll_view_pager_demo);
        imageIdList = new ArrayList<Integer>();
//        imageIdList.add(R.drawable.custom_logo);
        imageIdList.add(R.drawable.pic_one);
        imageIdList.add(R.drawable.pic_teo);
        imageIdList.add(R.drawable.pic_three);
        imageIdList.add(R.drawable.pic_four);
//        imageIdList.add(R.drawable.pic_five);
        tabs = (TabLayout) findViewById(R.id.tabs);
        setupTabLayout();
        viewPager = (AutoScrollViewPager)findViewById(R.id.view_pager);
        btn_explore = (TextView)findViewById(R.id.btn_explore);
        viewPager.setAdapter(new ImagePagerAdapter(this, imageIdList).setInfiniteLoop(false));
        viewPager.addOnPageChangeListener(new MyOnPageChangeListener());
        tabs.setupWithViewPager(viewPager);

        appUtils = AppUtils.getInstance();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);
        viewPager.setInterval(10000);
        viewPager.startAutoScroll(10000);
//        viewPager.setCurrentItem(Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % imageIdList.size());
        btn_explore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScrollingSplashActivity.this,HomeActivity.class));
//                finish();
            }
        });
        if (AppSharedPreference.getString(ScrollingSplashActivity.this, Constant.OWNER_ID) == null || AppSharedPreference.getString(ScrollingSplashActivity.this, Constant.OWNER_ID).equalsIgnoreCase("")) {
            showConfirmOwnerIdentityDialog();
        }
//

        // the more properties whose you can set
        // // set whether stop auto scroll when touching, default is true
        // viewPager.setStopScrollWhenTouch(false);
        // // set whether automatic cycle when auto scroll reaching the last or first item
        // // default is true
        // viewPager.setCycle(false);
        // /** set auto scroll direction, default is AutoScrollViewPager#RIGHT **/
        // viewPager.setDirection(AutoScrollViewPager.LEFT);
        // // set how to process when sliding at the last or first item
        // // default is AutoScrollViewPager#SLIDE_BORDER_NONE
        // viewPager.setBorderProcessWhenSlide(AutoScrollViewPager.SLIDE_BORDER_CYCLE);
        // viewPager.setScrollDurationFactor(3);
        // viewPager.setBorderAnimation(false);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    /*
       This method is used to verify the owner ID for the club member every time on using any session
       * */
    private void showConfirmOwnerIdentityDialog() {
        verifyDialog = new Dialog(ScrollingSplashActivity.this);
        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifyDialog.setContentView(R.layout.dialog_takeownerid);
        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifyDialog.show();
        verifyDialog.setCancelable(false);

        tvOwnerId = (EditText) verifyDialog.findViewById(R.id.et_ownerid);

        tvOwnerId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_DONE) {

                    if (!tvOwnerId.getText().toString().trim().equals("")) {
                        if (networkConnectionIndicator.isNetworkAvailable(true)) {
                            verifyOwnerIdApi();
                        } else {
                            appUtils.showToast(ScrollingSplashActivity.this, getResources().getString(R.string.pls_check_internet));
                        }

                    } else {
                        appUtils.showToast(ScrollingSplashActivity.this, getResources().getString(R.string.pls_enter_owner_id));
                    }
                }
                return false;
            }
        });

        tvSubmit = (TextView) verifyDialog.findViewById(R.id.tv_submitownerid);

        verifyDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
//                ScrollingSplashActivity.this.finish();
            }
        });

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvOwnerId.getText().toString().trim().equals("")) {
                    if (networkConnectionIndicator.isNetworkAvailable(true)) {
                        verifyOwnerIdApi();
                    } else {
                        appUtils.showToast(ScrollingSplashActivity.this, getResources().getString(R.string.pls_check_internet));
                    }

                } else {
                    appUtils.showToast(ScrollingSplashActivity.this, getResources().getString(R.string.pls_enter_owner_id));
                }
            }
        });


        ivCross = (ImageView) verifyDialog.findViewById(R.id.iv_cross);
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyDialog.dismiss();
//                finish();
            }
        });
    }

    private void setupTabLayout() {
        for (int i=0;i<imageIdList.size();i++){
            tabs.addTab(tabs.newTab());
        }

        for(int i=0; i < tabs.getTabCount(); i++) {
            View tab = ((ViewGroup) tabs.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 50, 50, 0);
            tab.setLayoutParams(p);
            tab.requestLayout();
        }
    }

    public class MyOnPageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
//            indexText.setText("2");
            if( tabs.getTabAt(position)!=null) {
                tabs.getTabAt(position).select();
            }

        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

        @Override
        public void onPageScrollStateChanged(int arg0) {}
    }

    @Override
    protected void onPause() {
        super.onPause();
        // stop auto scroll when onPause
        viewPager.stopAutoScroll();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // start auto scroll when onResume
        viewPager.startAutoScroll();
    }


    //This method is used hit the Owner Verify API
    public void verifyOwnerIdApi() {

        appUtils.showProgressDialog(ScrollingSplashActivity.this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.OWNER_ID, tvOwnerId.getText().toString().trim());

        Call<ResponseBody> call = service.checkOwner(params);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                tvOwnerId.setText("");
                appUtils.hideProgressDialog();
                String str = "";
                try {
                    if (response != null) {
                        str = response.body().string();
                        ObjectMapper mapper = new ObjectMapper();
                        VerifyOwnerIdModel verifyOwnerIdModel = mapper.readValue(str, VerifyOwnerIdModel.class);


                        if (verifyOwnerIdModel.getCODE().toString().equals("200")) {

                            AppSharedPreference.putString(ScrollingSplashActivity.this, Constant.OWNER_ID, verifyOwnerIdModel.getVALUE().getOwnerId());
                            AppSharedPreference.putString(ScrollingSplashActivity.this, Constant.CLUB_NAME, verifyOwnerIdModel.getVALUE().getCName());

                            String club_name = verifyOwnerIdModel.getVALUE().getCName()
                                    + " " + getResources().getString(R.string.massage_centre);
//
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//                                tvWelcomeText.setText(Html.fromHtml(getResources().getString(R.string.welcome_to) + " <br> " + club_name, Html.FROM_HTML_MODE_LEGACY));
//                            else
//                                tvWelcomeText.setText(Html.fromHtml(getResources().getString(R.string.welcome_to) + " <br> " + club_name));
                            verifyDialog.dismiss();

                        } else {
                            Toast toatsMessage = Toast.makeText(ScrollingSplashActivity.this, verifyOwnerIdModel.
                                    getMESSAGE(), Toast.LENGTH_LONG);
                            toatsMessage.setGravity(Gravity.CENTER, 0, 0);
                            toatsMessage.show();

                            ViewGroup viewGroup = (ViewGroup) toatsMessage.getView();
                            TextView toastText = (TextView) viewGroup.getChildAt(0);
                            toastText.setTextSize(20);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(ScrollingSplashActivity.this, getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(ScrollingSplashActivity.this, getResources().getString(R.string.something_went_wrong));
            }
        });
    }
}