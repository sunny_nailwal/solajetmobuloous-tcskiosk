package com.solajet.tcskiosk.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;

import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import customviews.TouchScreenEditText;
import customviews.TouchScreenTextView;
import interfaces.ApiInterface;
import models.ForgotPasscodeModel.ForgotPasscodeModel;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;
import utils.NetworkConnectionIndicator;

/**
 * Created by admin1 on 25/10/16.
 */

public class ForgotPasscodeActivity extends AppCompatActivity implements View.OnFocusChangeListener {


    @BindView(R.id.toolbar_title)
    TouchScreenTextView toolbarTitle;
    @BindView(R.id.iv_toolbar_left_icon)
    ImageView ivToolbarLeftIcon;
    @BindView(R.id.iv_cross)
    ImageView ivCross;
    @BindView(R.id.iv_mobile)
    ImageView ivMobile;
    @BindView(R.id.et_mobile)
    TouchScreenEditText etMobile;
    @BindView(R.id.tv_submit)
    TouchScreenTextView tvSubmit;
    @BindView(R.id.ll_rootView)
    LinearLayout llRootView;

    private ImageView crossIV;
    private TextView dialogokTV, dialogeditTV, mobileTV;
    private Dialog verifyDialog, passcodeGenerateDialog;
    private AppUtils appUtils;
    private NetworkConnectionIndicator networkConnectionIndicator;

    private ForgotPasscodeModel forgotPasscodeModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpasscode);
        ButterKnife.bind(this);
        initView();
    }


    private void initView() {
        ivToolbarLeftIcon.setVisibility(View.GONE);
        toolbarTitle.setText(getResources().getString(R.string.forgot_passcode));
        etMobile.setOnFocusChangeListener(this);
        appUtils = AppUtils.getInstance();
        networkConnectionIndicator = NetworkConnectionIndicator.getInstance(this);
        etMobile.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (networkConnectionIndicator.isNetworkAvailable(true)) {
                        appUtils.hideNativeKeyboard(ForgotPasscodeActivity.this);
                        String mobileno = etMobile.getText().toString();
                        if (validate()) {
                            showMobileConfirmDialog(mobileno);
                        }

                    } else {
                        appUtils.showToast(ForgotPasscodeActivity.this, getResources().getString(R.string.pls_check_internet));
                    }
                }
                return false;
            }
        });

    }


    /*
    *  forgot passcode api hit and get passcode as response
   */

    public void forgotPasscodeApi() {

        appUtils.showProgressDialog(ForgotPasscodeActivity.this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.MEMBER_PHONE_NUMBER, etMobile.getText().toString().trim());
        params.put(Constant.OWNER_ID, AppSharedPreference.getString(ForgotPasscodeActivity.this, Constant.OWNER_ID));


        Call<ResponseBody> call = service.forgot_passcode(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                String str = "";
                try {

                    if (response != null) {
                        str = response.body().string();

                        ObjectMapper mapper = new ObjectMapper();
                        forgotPasscodeModel = mapper.readValue(str, ForgotPasscodeModel.class);
                        if (forgotPasscodeModel.getCODE().toString().equals("200")) {
                            showPasscodeGeneratedDialog();
                        } else {
                            appUtils.showToast(ForgotPasscodeActivity.this, forgotPasscodeModel.getMESSAGE());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appUtils.showToast(ForgotPasscodeActivity.this, getResources().getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(ForgotPasscodeActivity.this, getResources().getString(R.string.something_went_wrong));
            }
        });

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {

            case R.id.et_mobile:
                if (hasFocus) {
                    ivMobile.setImageResource(R.drawable.ic_tablet_signup_mobile_select);
                    changeEdittextBackground(etMobile, getResources().getColor(R.color.blue));
                } else {
                    ivMobile.setImageResource(R.drawable.ic_tablet_signup_mobile_deselect);
                    changeEdittextBackground(etMobile, getResources().getColor(R.color.black));
                }
                break;
        }
    }


    /*
        * confirm dialog for mobile
        * */
    private void showMobileConfirmDialog(String mobileno) {

        verifyDialog = new Dialog(ForgotPasscodeActivity.this);
        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifyDialog.setContentView(R.layout.dialog_forgotpasscodeverfitymobile);
        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifyDialog.show();

        crossIV = (ImageView) verifyDialog.findViewById(R.id.iv_dialogcross);
        dialogeditTV = (TextView) verifyDialog.findViewById(R.id.tv_dialogedit);
        dialogokTV = (TextView) verifyDialog.findViewById(R.id.tv_dialogok);
        mobileTV = (TextView) verifyDialog.findViewById(R.id.tv_mobile);
        mobileTV.setText(mobileno);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyDialog.dismiss();
            }
        });
        dialogeditTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyDialog.dismiss();
            }
        });
        dialogokTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyDialog.dismiss();

                if (networkConnectionIndicator.isNetworkAvailable(true)) {
                    forgotPasscodeApi();

                } else {
                    appUtils.showToast(ForgotPasscodeActivity.this, getResources().getString(R.string.pls_check_internet));
                }
            }
        });
    }

    /*
    * generated passcode from server and show on dialog
    *
    * */
    private void showPasscodeGeneratedDialog() {
        passcodeGenerateDialog = new Dialog(ForgotPasscodeActivity.this);
        passcodeGenerateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        passcodeGenerateDialog.setContentView(R.layout.dialog_resetpasscode);
        passcodeGenerateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        passcodeGenerateDialog.show();
        passcodeGenerateDialog.setCancelable(false);

        crossIV = (ImageView) passcodeGenerateDialog.findViewById(R.id.iv_dialogcross);

//        showtextIntoTextViews();
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passcodeGenerateDialog.dismiss();
                ForgotPasscodeActivity.this.finish();
            }
        });
    }

//
//    /*
//    *  show single passcode into dialog textviews
//    *
//    * */
//    private void showtextIntoTextViews() {
//        String passcode = forgotPasscodeModel.getVALUE().getPassword();
//
//        passcodeoneTV.setText(passcode.substring(0, 1));
//        passcodetwoTV.setText(passcode.substring(1, 2));
//        passcodethreeTV.setText(passcode.substring(2, 3));
//        passcodefourTV.setText(passcode.substring(3, 4));
//    }



    /*
    * for changing edit text background color
    * */

    private void changeEdittextBackground(EditText mEdittext, int color) {
        Drawable drawable = mEdittext.getBackground(); // get current EditText drawable
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP); // change the drawable color

        if (Build.VERSION.SDK_INT > 16) {
            mEdittext.setBackground(drawable); // set the new drawable to EditText
        } else {
            mEdittext.setBackgroundDrawable(drawable); // use setBackgroundDrawable because setBackground required API 16
        }
    }


    /*
    * check validation of mobile number
    * */
    private boolean validate() {
        if (etMobile.getText().toString().trim().length() < 10) {
            appUtils.showToast(this, getResources().getString(R.string.pls_enter_valid_number));
            return false;
        } else {
            return true;
        }
    }


    @OnClick({R.id.iv_cross, R.id.tv_submit, R.id.ll_rootView})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_cross:
                appUtils.hideNativeKeyboard(ForgotPasscodeActivity.this);
                finish();
                break;

            case R.id.tv_submit:
                appUtils.hideNativeKeyboard(this);
                String mobileno = etMobile.getText().toString();
                if (validate()) {
                    showMobileConfirmDialog(mobileno);
                }
                break;

            case R.id.ll_rootView:
                appUtils.hideNativeKeyboard(ForgotPasscodeActivity.this);
                break;

        }
    }

}
