package com.solajet.tcskiosk.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solajet.tcskiosk.tcskiosk.R;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import customviews.TouchScreenTextView;
import interfaces.ApiInterface;
import models.StartMassage.StartMassageBean;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import utils.AppSharedPreference;
import utils.AppUtils;
import utils.Constant;

public class PaymentStripe extends AppCompatActivity {
    @BindView(R.id.toolbar_title)
    TouchScreenTextView toolbarTitle;

    @BindView(R.id.iv_toolbar_left_icon)
    ImageView ivToolbarLeftIcon;

    @BindView(R.id.iv_cross)
    ImageView ivcross;

    private StartMassageBean startMassagemodel;

    private final String TAG = PaymentStripe.class.getSimpleName();
    private Spinner year_edt, month_edt;
    private EditText card_no, cvc_edt, card_holder_nme;
    private Button btn_pay;
    private String my_token;
    private Stripe stripe;
    private String yearSelected;
    private String monthSelected;
    private int monthPos = 0;
    private int yearPos;
    private TextView package_name, total_amaount;

    String testSecretKey = "sk_test_e26cqRA6RydOxsJizQCRtOIa";
    String testPublishablekey = "pk_test_uA3oqVmtlhwUKG9BgpfYWVT0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_stripe);
        ButterKnife.bind(this);
        initView();
    }


    public void initView() {

        ivToolbarLeftIcon.setVisibility(View.VISIBLE);
        toolbarTitle.setText("Payment");
        package_name = (TextView) findViewById(R.id.package_name);
        total_amaount = (TextView) findViewById(R.id.total_amaount);

        package_name.setText(AppSharedPreference.getString(this, Constant.PACKAGE_NAME));
        total_amaount.setText("$" + AppSharedPreference.getString(this, Constant.PACKAGE_AMOUNT));


        //YEARS
        year_edt = (Spinner) findViewById(R.id.year_edt);
        setUpYearSpinner();
        year_edt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yearSelected = parent.getItemAtPosition(position).toString();
                yearPos = position;
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //MONTHS
        month_edt = (Spinner) findViewById(R.id.month_edt);
        setUpMonthSpinner();
        month_edt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthSelected = parent.getItemAtPosition(position).toString();
                monthPos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        card_no = (EditText) findViewById(R.id.card_no);
        cvc_edt = (EditText) findViewById(R.id.cvc_edt);
        card_holder_nme = (EditText) findViewById(R.id.card_holder_nme);
        btn_pay = (Button) findViewById(R.id.btn_pay);


        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {

                    Card card = new Card(card_no.getText().toString().trim(), Integer.parseInt(month_edt.getSelectedItem().toString().trim()), Integer.parseInt(year_edt.getSelectedItem().toString()), cvc_edt.getText().toString().trim());

                    if (!card.validateCard()) {
                        Toast.makeText(PaymentStripe.this, "Invalid card", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    final Stripe stripe = new Stripe(PaymentStripe.this, "pk_test_ojjMH1M5JUEkUNEJDIkUYpfG");
                    // MyDialog.getInstance(PaymentGateway.this).showDialog(PaymentGateway.this);
                    stripe.createToken(card, new TokenCallback() {
                                public void onSuccess(Token token) {
                                    my_token = token.getId();
                                    Log.i("Token ", " token is " + my_token);

                                   // Toast.makeText(PaymentStripe.this, "Done", Toast.LENGTH_SHORT).show();


                                    // Send token to your server
                                    PaymentApi();
                                }

                                public void onError(Exception error) {
                                    // Show localized error message
                                    Toast.makeText(PaymentStripe.this, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();

                                }
                            }
                    );
                }

            }
        });


        ivToolbarLeftIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        ivcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }





    public void PaymentApi() {

        final AppUtils appUtils = AppUtils.getInstance();
        appUtils.showProgressDialog(this, getResources().getString(R.string.please_wait));
        ApiInterface service = RestApi.createService(ApiInterface.class);
        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.PACKAGE_ID, AppSharedPreference.getString(this, Constant.PACKAGE_ID));
        params.put(Constant.USER_ID, AppSharedPreference.getString(this, Constant.USER_ID));
        params.put(Constant.PACKAGE_AMOUNT, AppSharedPreference.getString(this, Constant.PACKAGE_AMOUNT));
        params.put(Constant.STRIPE_TOKEN, my_token);

        params.put(Constant.ACTION, "package_payment");

        Call<ResponseBody> call = service.checkPackageStatus(params);
        call.enqueue(new Callback<ResponseBody>() {


            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                appUtils.hideProgressDialog();
                int count = 0;
                String str = "";

                try {
                    if (response != null) {
                        str = response.body().string();
                        ObjectMapper objectMapper = new ObjectMapper();
                        startMassagemodel = objectMapper.readValue(str, StartMassageBean.class);

                        if (startMassagemodel.getCODE().toString().equalsIgnoreCase("200")) {


                            Intent intentMain = new Intent(PaymentStripe.this, VideoHomeActivity.class);
                            intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intentMain);
                            finish();


                            Toast.makeText(PaymentStripe.this, "" + startMassagemodel.getMESSAGE(), Toast.LENGTH_SHORT).show();

//
//                            }
                        }

                        Toast.makeText(PaymentStripe.this, "" + startMassagemodel.getMESSAGE(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e1) {
                    appUtils.hideProgressDialog();
                    appUtils.showToast(PaymentStripe.this, getResources().getString(R.string.something_went_wrong));
                    e1.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                appUtils.hideProgressDialog();
                appUtils.showToast(PaymentStripe.this, startMassagemodel.getMESSAGE()!=null?startMassagemodel.getMESSAGE():getResources().getString(R.string.something_went_wrong));
            }
        });

    }








    private boolean validation() {
        boolean flag = true;


        if (card_no.getText().toString().trim().equals("")) {
            Toast.makeText(PaymentStripe.this, "Please enter card number", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (month_edt.getSelectedItem().toString().trim().equalsIgnoreCase("MM")) {
            Toast.makeText(PaymentStripe.this, "Please enter Month", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (year_edt.getSelectedItem().toString().trim().equalsIgnoreCase("YY")) {
            Toast.makeText(PaymentStripe.this, "Please enter year", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (cvc_edt.getText().toString().trim().equals("")) {
            Toast.makeText(PaymentStripe.this, "Please enter cvv", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (card_holder_nme.getText().toString().trim().equals("")) {
            Toast.makeText(PaymentStripe.this, "Please enter Card holder name", Toast.LENGTH_LONG).show();
            flag = false;
        }
        return flag;
    }


    private void setUpYearSpinner() {
        String[] spinnerItems = new String[]{
                "YY",
                "2018",
                "2019",
                "2020",
                "2021",
                "2022",
                "2023",
                "2024",
                "2025",
                "2026",
                "2028",
                "2029",
                "2030",
                "2031",
                "2032",
                "2033",
                "2034",
                "2035",
                "2036",
                "2037",
                "2038",
                "2039",
                "2040",
                "2041",
                "2042",
                "2043",
                "2044",
                "2045",
                "2046",
                "2047",
                "2048",
                "2049",
                "2050"};


        List<String> spinnerList = new ArrayList<>(Arrays.asList(spinnerItems));

        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(PaymentStripe.this, R.layout.spinner_drop_singlerow, spinnerList) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            //Disable the first item of spinner.
                            Log.i(TAG, "Position[0]: Spinner first item is disabled");
                            return false;
                        } else {
                            String itemSelected = year_edt.getItemAtPosition(position).toString();
                            Log.i(TAG, "Selected Item[" + position + "]: " + itemSelected);
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                        View spinnerView = super.getDropDownView(position, convertView, parent);

                        TextView spinnerTextV = (TextView) spinnerView;
                        if (position == 0) {
                            //Set the disable spinner item color fade .
                            spinnerTextV.setTextColor(Color.parseColor("#a7a7a6"));
                        } else {
                            //#404040
                            spinnerTextV.setTextColor(Color.parseColor("#b2000000"));
                        }
                        return spinnerView;
                    }
                };

        arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_singlerow);

        year_edt.setAdapter(arrayAdapter);

    }

    private void setUpMonthSpinner() {
        String[] spinnerItems = new String[]{
                "MM",
                "01",
                "02",
                "03",
                "04",
                "05",
                "06",
                "07",
                "08",
                "09",
                "10",
                "11",
                "12"};


        List<String> spinnerList = new ArrayList<>(Arrays.asList(spinnerItems));

        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(PaymentStripe.this, R.layout.spinner_drop_singlerow, spinnerList) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            //Disable the first item of spinner.
                            Log.i(TAG, "Position[0]: Spinner first item is disabled");
                            return false;
                        } else {
                            String itemSelected = month_edt.getItemAtPosition(position).toString();
                            Log.i(TAG, "Selected Item[" + position + "]: " + itemSelected);
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                        View spinnerView = super.getDropDownView(position, convertView, parent);

                        TextView spinnerTextV = (TextView) spinnerView;
                        if (position == 0) {
                            //Set the disable spinner item color fade .
                            spinnerTextV.setTextColor(Color.parseColor("#a7a7a6"));
                        } else {
                            //#404040
                            spinnerTextV.setTextColor(Color.parseColor("#b2000000"));
                        }
                        return spinnerView;
                    }
                };

        arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_singlerow);

        month_edt.setAdapter(arrayAdapter);


    }

}
