package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import java.util.ArrayList;

import models.EmployeeLogin.EmployeeLoginBean;
import models.EmployeeLogin.EmployeeMassageCountBean;
import models.MemberLogin.MemberLoginBean;

/**
 * Created by Prateek on 12/12/16.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "health_club_details";
    //Table names
    private static final String TABLE_EMPLOYEE = "employee_table";
    private static final String TABLE_EMPLOYEE_MASSAGE = "employee_massage_count";
    private static final String TABLE_MEMBER_LOGIN = "member_login_table";
    private static final String TABLE_MEMBER_MASSAGE_DATA = "member_massage_data";
    //TABLE_EMPLOYEE details
    private static final String EMPLOYEE_ID = "employee_id";
    private static final String EMPLOYEE_PASSCODE = "employee_passcode";
    private static final String EMPLOYEE_MASSAGE_DURATION = "massage_duration";
    private static final String CREATE_EMPLOYEE_TABLE = "CREATE TABLE " + TABLE_EMPLOYEE + "( " + EMPLOYEE_ID + " TEXT PRIMARY KEY, "
            + EMPLOYEE_PASSCODE + " TEXT NOT NULL, " + EMPLOYEE_MASSAGE_DURATION + " TEXT NOT NULL )";
    //TABLE_EMPLOYEE massages details
    private static final String EMP_ID = "employee_id";
    private static final String EMP_MASSAGE_COMPLETION = "massage_completion";
    private static final String CREATE_EMPLOYEE_MASSAGE_TABLE = "CREATE TABLE " + TABLE_EMPLOYEE_MASSAGE + "( " + EMP_ID + " TEXT PRIMARY KEY, "
            + EMP_MASSAGE_COMPLETION + " INTEGER NOT NULL )";
    // TABLE_MEMBER_LOGIN fields
    private final static String USER_ID = "user_id";
    private final static String MEMBER_ID = "member_id";
    private final static String MEMBER_PASSCODE = "member_passcode";
    private final static String MEMBER_NAME = "member_name";
    private final static String PACKAGE_NAME = "package_name";
    private final static String PACKAGE_TYPE = "package_type";
    private final static String PACKAGE_AMOUNT = "package_amount";
    private final static String PACKAGE_EXPIRY_DATE = "package_expiry_date";
    private final static String PACKAGE_SUBSCRIPTION_STATUS = "package_subscription_status";
    private final static String PACKAGE_PAYMENT_STATUS = "package_payment_status";
    private final static String PACKAGE_MASSAGE_COUNT = "package_massage_count";
    private static final String CREATE_MEMBER_TABLE = "CREATE TABLE " + TABLE_MEMBER_LOGIN + "( " + MEMBER_ID + " TEXT PRIMARY KEY, "
            + MEMBER_PASSCODE + " TEXT NOT NULL, " + USER_ID + " TEXT NOT NULL, " + MEMBER_NAME + " TEXT NOT NULL, " + PACKAGE_NAME + " TEXT, "
            + PACKAGE_TYPE + " TEXT, " + PACKAGE_AMOUNT + " TEXT, " + PACKAGE_EXPIRY_DATE + " TEXT, " + PACKAGE_SUBSCRIPTION_STATUS + " TEXT NOT NULL, "
            + PACKAGE_PAYMENT_STATUS + " TEXT NOT NULL, " + PACKAGE_MASSAGE_COUNT + " TEXT )";
    private static DbHelper mInstance = null;
    private static int DATABASE_VERSION = 1;


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /*
    * This method makes sure that there is only instanse of DBHelper class
    * */
    public static DbHelper getInstance(Context context) {

        if (mInstance == null) {
            return new DbHelper(context.getApplicationContext());
        } else {
            return mInstance;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_EMPLOYEE_TABLE);
        Log.d("EMPLOYEE_TABLE = ", CREATE_EMPLOYEE_TABLE);
        db.execSQL(CREATE_EMPLOYEE_MASSAGE_TABLE);
        Log.d("EMP_Massage_TABLE = ", CREATE_EMPLOYEE_MASSAGE_TABLE);
        db.execSQL(CREATE_MEMBER_TABLE);
        Log.d("MEMBER_TABLE = ", CREATE_MEMBER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        //on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMPLOYEE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMPLOYEE_MASSAGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMBER_LOGIN);

        Log.d("Database", "Database created");

        //create tables again
        onCreate(db);
    }

    /*
    * This method is used to insert values in the TABLE_EMPLOYEE
    * */
    public void insertDataIntoEmployeeTable(ArrayList<EmployeeLoginBean> employeeLoginList) {

        SQLiteDatabase sqlDb = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        deleteAlldataOfEmployeeTable();//delete all the data of table evrytime service is hit

        for (int i = 0; i < employeeLoginList.size(); i++) {

            cv.put(EMPLOYEE_ID, employeeLoginList.get(i).getEmployeeId());
            cv.put(EMPLOYEE_PASSCODE, employeeLoginList.get(i).getEmployeePascode());
            cv.put(EMPLOYEE_MASSAGE_DURATION, employeeLoginList.get(i).getEmployeeMassageDuration());
            long l = sqlDb.insert(TABLE_EMPLOYEE, null, cv);
            Log.d("Insertion row", String.valueOf(l));
        }
        sqlDb.close();
    }


    /*
         * This method is used to delete all values in the TABLE_EMPLOYEE
     */
    public void deleteAlldataOfEmployeeTable() {
        SQLiteDatabase helper = this.getWritableDatabase();
        int del_rows = helper.delete(TABLE_EMPLOYEE, null, null);//to delete all the data of the table
        Log.d("Delete rows", String.valueOf(del_rows));

    }



    /*
    * This method is used to fetch values in the TABLE_EMPLOYEE
    * */

    public ArrayList<EmployeeLoginBean> fetchAllEmployeeInformation() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<EmployeeLoginBean> employeeList = new ArrayList<>();
//        Cursor c = db.query(TABLE_EMPLOYEE, null,null,null,null,null,null,null);
        String SELECT_QUERY = "SELECT * FROM " + TABLE_EMPLOYEE;
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);

        if (cursor.moveToFirst()) {

            do {
                EmployeeLoginBean emp1 = new EmployeeLoginBean();
                emp1.setEmployeeId(cursor.getString(cursor.getColumnIndex(EMPLOYEE_ID)));
                emp1.setEmployeePascode(cursor.getString(cursor.getColumnIndex(EMPLOYEE_PASSCODE)));
                emp1.setEmployeeMassageDuration(cursor.getString(cursor.getColumnIndex(EMPLOYEE_MASSAGE_DURATION)));

                employeeList.add(emp1);

            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return employeeList;

    }


    /*
   * This method is used to insert values in the TABLE_EMPLOYEE_MASSAGE_COUNT
   * */
    public void insertDataIntoManagerMassageCountTable(EmployeeMassageCountBean beanObject) {

        SQLiteDatabase sqlDb = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(EMP_ID, beanObject.getEmpId());
        cv.put(EMP_MASSAGE_COMPLETION, beanObject.getEmpCompletion());

        long l = sqlDb.insert(TABLE_EMPLOYEE, null, cv);
        Log.d("MassageCount Row", String.valueOf(l));
        sqlDb.close();
    }

//    public boolean validateManagerFromDb(String id){}


    /*
   * This method is used to insert values in the TABLE_EMPLOYEE
   * */
    public void updateDataIntoEmployeeMassageCountTable(String emp_id) {

        SQLiteDatabase sqlDb = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(EMP_MASSAGE_COMPLETION, "1");
        int i = sqlDb.update(TABLE_EMPLOYEE, cv, EMP_ID + " =?", new String[]{emp_id});
        Log.d("Updation", i + " rows up-dated");
        sqlDb.close();
    }


    /*
    * This method is used to insert data into the MEMBER_LOGIN_TABLE
    * */
    public void insertDataIntoMemberLoginTable(ArrayList<MemberLoginBean> memberList) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        deleteAllDataofMemberLoginTable();

        for (int i = 0; i < memberList.size(); i++) {

            contentValues.put(USER_ID, memberList.get(i).getUserId());
            contentValues.put(MEMBER_ID, memberList.get(i).getMemberId());
            contentValues.put(MEMBER_PASSCODE, memberList.get(i).getMemberPasscode());
            contentValues.put(MEMBER_NAME, memberList.get(i).getMemberName());
            contentValues.put(PACKAGE_NAME, memberList.get(i).getPackageName());
            contentValues.put(PACKAGE_TYPE, memberList.get(i).getPackageType());
            contentValues.put(PACKAGE_AMOUNT, memberList.get(i).getPackageAmount());
            contentValues.put(PACKAGE_PAYMENT_STATUS, memberList.get(i).getPackageAmountStatus());
            contentValues.put(PACKAGE_SUBSCRIPTION_STATUS, memberList.get(i).getPackageSubscriptionStatus());
            contentValues.put(PACKAGE_MASSAGE_COUNT, memberList.get(i).getPackageMassageCount());
            contentValues.put(PACKAGE_EXPIRY_DATE, memberList.get(i).getPackageExpiryDate());

            long l = db.insert(TABLE_MEMBER_LOGIN, null, contentValues);
            Log.d("Member Login Row", String.valueOf(l));
        }
    }


    /*
    * Method to verify member from DB
    * */
    public boolean verifyMemberFromDb(String id, String passcode) {

        SQLiteDatabase sqlDb = this.getWritableDatabase();
        String passCode;

        Cursor cursor = sqlDb.query(TABLE_MEMBER_LOGIN, new String[]{EMPLOYEE_PASSCODE}, EMPLOYEE_ID + "=" + id, null, null, null, null, null);

        if (cursor != null) {
            return (cursor.getString(cursor.getColumnIndex(EMPLOYEE_PASSCODE))).equals(passcode);
        } else {
            return false;
        }

    }


    /*
    * This method is used to delete all the data of the MEMBER_LOGIN_TABLE
     */
    public void deleteAllDataofMemberLoginTable() {

        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete(TABLE_MEMBER_LOGIN, null, null);
        Log.d("MemberTabledelRows", String.valueOf(i));
    }


    /*
    * This method is used to fetch all the data from the Member Table
    * */
    public ArrayList<MemberLoginBean> fetchAllDataFromMemberTable() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<MemberLoginBean> memberList = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_MEMBER_LOGIN, null);

        if (cursor.moveToFirst()) {

            do {
                MemberLoginBean m = new MemberLoginBean();
                m.setUserId(cursor.getString(cursor.getColumnIndex(USER_ID)));
                m.setMemberId(cursor.getString(cursor.getColumnIndex(MEMBER_ID)));
                m.setMemberName(cursor.getString(cursor.getColumnIndex(MEMBER_NAME)));
                m.setMemberPasscode(cursor.getString(cursor.getColumnIndex(MEMBER_PASSCODE)));
                m.setPackageName(cursor.getString(cursor.getColumnIndex(PACKAGE_NAME)));
                m.setPackageName(cursor.getString(cursor.getColumnIndex(PACKAGE_TYPE)));
                m.setPackageName(cursor.getString(cursor.getColumnIndex(PACKAGE_AMOUNT)));
                m.setPackageName(cursor.getString(cursor.getColumnIndex(PACKAGE_EXPIRY_DATE)));
                m.setPackageName(cursor.getString(cursor.getColumnIndex(PACKAGE_MASSAGE_COUNT)));
                m.setPackageName(cursor.getString(cursor.getColumnIndex(PACKAGE_SUBSCRIPTION_STATUS)));
                m.setPackageName(cursor.getString(cursor.getColumnIndex(PACKAGE_PAYMENT_STATUS)));

                memberList.add(m);
            } while (cursor.moveToNext());
        }
        return memberList;
    }
}

