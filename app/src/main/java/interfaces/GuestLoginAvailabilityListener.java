package interfaces;

/**
 * Created by mobulous1 on 16/6/17.
 */

public interface GuestLoginAvailabilityListener {
    void onCheckAvailability(int availability);

    void onCheckAvailabilityOnFailure();
}
