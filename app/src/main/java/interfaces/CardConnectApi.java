package interfaces;

import models.CardConnectModel.AuthRequest;
import models.CardConnectModel.AuthResponse;
import retrofit2.http.PUT;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;

public interface CardConnectApi {

    @PUT("auth")
    Call<AuthResponse> authorize(@Header("Authorization") String authHeader, @Body AuthRequest authRequestModel);
}
