package interfaces;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;


public interface ApiInterface {

//    @Multipart
//    @POST("signup")
//    Call<ResponseBody> signUpWithFB(@Part MultipartBody.Part file, @PartMap Map<String, RequestBody> map);

    @FormUrlEncoded
    @POST("login")
    Call<ResponseBody> login(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("signup")
    Call<ResponseBody> signUp(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("forgot")
    Call<ResponseBody> forgot_passcode(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("packages")
    Call<ResponseBody> getPackages(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("packages")
    Call<ResponseBody> getPackageDetail(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("checkowner")
    Call<ResponseBody> checkOwner(@FieldMap HashMap<String, String> map);


    @FormUrlEncoded
    @POST("packages")
    Call<ResponseBody> subscribe(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("login")
    Call<ResponseBody> subAdminLogin(@FieldMap HashMap<String, String> map);


    @GET("terms")
    Call<ResponseBody> termsAndCondition(@QueryMap HashMap<String, String> map);

    @GET("/pad/getapkversion")
    Call<ResponseBody> updateApp();

    @FormUrlEncoded
    @POST("packages")
    Call<ResponseBody> checkPackageStatus(@FieldMap HashMap<String, String> map);


    @FormUrlEncoded
    @POST("packages")
    Call<ResponseBody> massageEnd(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("packages")
    Call<ResponseBody> acceptTermsAndCondition(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("codematch")
    Call<ResponseBody> singleSession(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("guestmassage")
    Call<ResponseBody> guestAvailability(@FieldMap HashMap<String, String> map);


}
