package task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;


import constant.MsgConstent;
import interfaces.ApiInterface;
import network.RestApi;
import retrofit2.Call;
import retrofit2.Callback;

//import android.util.Log;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;


/**
 * Created by Administrator on 2015/11/8.
 */
public class CheckVersionTask implements Runnable {

    private static final String LOG_TAG = "CheckVersionTask";

    private Handler mHandler;
    private Context mContext;

    public CheckVersionTask(Handler handler, Context context) {
        this.mHandler = handler;
        this.mContext = context;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        ApiInterface service = RestApi.createServiceForUpdate(ApiInterface.class);

        Call<okhttp3.ResponseBody> call = service.updateApp();
        call.enqueue(new Callback<okhttp3.ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, retrofit2.Response<okhttp3.ResponseBody> response) {

                System.out.println("response=" + response);
                Message msg = Message.obtain(); //it is an message that will be send to the handler
                msg.what = MsgConstent.MSG_GETVERSIONINFO;
                msg.obj = response;
                mHandler.sendMessage(msg);

            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {

                Message msg = Message.obtain();
                msg.what = MsgConstent.MSG_CONNECTSERVER_FAILED;
                mHandler.sendMessage(msg);
            }
        });


//
        //Here an api is being hit using volley we nned to do it using retrofit
//
//        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, NetConstant.GETVERSION_URL, null,
//
//                new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        System.out.println("response=" + response);
//                        Message msg = Message.obtain(); //it is an message that will be send to the handler
//                        msg.what = MsgConstent.MSG_GETVERSIONINFO;
//                        msg.obj = response;
//                        mHandler.sendMessage(msg);
//                    }
//                },
//                new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError arg0) {
//                Log.e(LOG_TAG, "sorry,Error  " + arg0.getMessage());
//                Message msg = Message.obtain();
//                msg.what = MsgConstent.MSG_CONNECTSERVER_FAILED;
//                mHandler.sendMessage(msg);
//            }
//        });
//        requestQueue.add(jsonObjectRequest);
//    }


        Thread thread = new Thread() {
            @Override
            public void run() {
                super.run();
            }
        };
    }
}

