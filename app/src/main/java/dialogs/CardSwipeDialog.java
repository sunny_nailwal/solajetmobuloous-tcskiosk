package dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Window;

import com.solajet.tcskiosk.tcskiosk.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import models.CardConnectModel.CardSwipeData;

public class CardSwipeDialog extends DialogFragment {

    public interface CardSwipeListener {
        void onCardSwipe(CardSwipeData cardSwipeData);
    }

    private static final String TAG = CardSwipeDialog.class.getSimpleName();
    private CardSwipeListener cardSwipeListener;

    public static CardSwipeDialog newInstance() {

        Bundle args = new Bundle();

        CardSwipeDialog fragment = new CardSwipeDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog1 = new Dialog(getContext(), R.style.ThemeDialogCustom);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.dialog_card_swipe);
        dialog1.setCanceledOnTouchOutside(false);
        ButterKnife.bind(this, dialog1);

        return dialog1;
    }

    @OnClick(R.id.tv_dialog_action_text)
    void onCancelClick() {
        dismiss();
    }

    @OnTextChanged(R.id.card_swipe_input)
    void onCardSwiped(CharSequence text) {
        final CardSwipeData cardSwipeData = CardSwipeData.parseSwipe(text.toString());

        if (cardSwipeData != null && cardSwipeListener != null) {
            cardSwipeListener.onCardSwipe(cardSwipeData);
            dismiss();
        }
    }

    public void setCardSwipeListener(CardSwipeListener cardSwipeListener) {
        this.cardSwipeListener = cardSwipeListener;
    }
}
