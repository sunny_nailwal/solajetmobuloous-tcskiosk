package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.solajet.tcskiosk.activities.AllPackageActivity;
import com.solajet.tcskiosk.activities.HomeActivity;
import com.solajet.tcskiosk.activities.LoginActivity;
import com.solajet.tcskiosk.activities.ManagerLoginActivity;
import com.solajet.tcskiosk.tcskiosk.R;

import java.util.ArrayList;

import models.UserCategoryModel;
import utils.AppSharedPreference;
import utils.Constant;

/**
 * Created by appinventiv on 1/12/16.
 */

public class UserCategoryAdapter extends RecyclerView.Adapter<UserCategoryAdapter.MyViewHolder> {
    private categorySelecttion listner;
    private ArrayList<UserCategoryModel> userCategoryList;
    private Context context;

    public UserCategoryAdapter(ArrayList<UserCategoryModel> userCategoryList, Context context) {
        this.userCategoryList = userCategoryList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.grid_view_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvCategory.setText(userCategoryList.get(position).getUserType());
        holder.rl.setClickable(true);
        holder.rl.setPressed(false);
    }


    @Override
    public int getItemCount() {
        return userCategoryList.size();
    }


    public void SetonClickListner(categorySelecttion listner) {

        this.listner = listner;

    }

    public void performAction(int position) {
        switch (position) {

            case 0:
                context.startActivity(new Intent(context, AllPackageActivity.class));
                break;


            case 1:


                if (listner != null)
                    listner.onCategoriesClick(position);

                context.startActivity(new Intent(context, LoginActivity.class).putExtra("change_package", "change_package"));
                break;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvCategory;
        RelativeLayout rl;

        public MyViewHolder(final View itemView) {
            super(itemView);
            tvCategory = (TextView) itemView.findViewById(R.id.tv_user_category);
            rl = (RelativeLayout) itemView.findViewById(R.id.rl_root);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getAdapterPosition()==0){

                        if(AppSharedPreference.getString(context, Constant.OWNER_ID) != null && !AppSharedPreference.getString(context, Constant.OWNER_ID).equalsIgnoreCase("")) {
                            performAction(getAdapterPosition());
                        }else {
                            ((HomeActivity)context).showUnlockDialog(getAdapterPosition());
                        }

                        if (listner != null)
                            listner.onCategoriesClick(0);
                    }else {
                    performAction(getAdapterPosition());
                    }
                }
            });
        }
    }



    public interface categorySelecttion {

        void onCategoriesClick(int position);

    }
}
