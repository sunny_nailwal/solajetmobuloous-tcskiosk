package adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.solajet.tcskiosk.tcskiosk.R;

import java.util.List;

public class DeviceListAdapter extends BaseAdapter {

    //private Context mContext = null;
    private List<BluetoothDevice> list_Device;
    private LayoutInflater inflater = null;
    private String mDefault;

    public DeviceListAdapter(Context context, String defaultDevid) {
        //this.mContext = context;
        inflater = LayoutInflater.from(context);
        mDefault = defaultDevid;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        int iCount = 0;
        if (null != list_Device) {
            iCount = list_Device.size();
        }

        return iCount;
    }

    public List<BluetoothDevice> getItems() {
        return this.list_Device;
    }

    public void setItems(List<BluetoothDevice> mlist) {
        this.list_Device = mlist;
    }

    @Override
    public Object getItem(int position) {
        BluetoothDevice dev = null;
        if (null != list_Device) {
            dev = list_Device.get(position);
        }
        return dev;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {

            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.item_device_info, null);
            holder.tv_devlist_name = (TextView) convertView
                    .findViewById(R.id.tv_devlist_name);
            holder.tv_devlist_uuid = (TextView) convertView
                    .findViewById(R.id.tv_devlist_uuid);

            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        BluetoothDevice item = list_Device.get(position);
        if (item != null) {
            //noinspection MissingPermission
            String devName = item.getName();
            String devUUID = item.getAddress();
            holder.tv_devlist_name.setText(devName);
            holder.tv_devlist_name.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(devUUID)) {

                if (!mDefault.equals(String.valueOf("default")) && mDefault.equals(devUUID)) {
                    holder.tv_devlist_uuid.setText(devUUID + "[default]");
                } else {
                    holder.tv_devlist_uuid.setText(devUUID);
                }

                holder.tv_devlist_uuid.setVisibility(View.VISIBLE);
            } else {
                holder.tv_devlist_uuid.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView tv_devlist_name;
        public TextView tv_devlist_uuid;
    }

}
