package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.solajet.tcskiosk.activities.AllPackageActivity;
import com.solajet.tcskiosk.activities.PackageDetailsActivity;
import com.solajet.tcskiosk.activities.VideoHomeActivity;
import com.solajet.tcskiosk.tcskiosk.R;

import java.util.List;

import models.AllPackagesModel.VALUE;
import utils.AppSharedPreference;
import utils.Constant;

public class PackageVideoAdapter extends RecyclerView.Adapter<PackageVideoAdapter.ViewHolder> {
    AQuery aQuery;
    private Context mContext;
    private List<VALUE> packageModelsList;
    int size;


    public PackageVideoAdapter(Context mContext, List<VALUE> packageModelsList,int size) {
        this.mContext = mContext;
        this.size = size;
        this.packageModelsList = packageModelsList;
        if (aQuery == null) {
            aQuery = new AQuery(mContext);
        }
    }


    @Override
    public PackageVideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.package_list_layout, parent, false);

        PackageVideoAdapter.ViewHolder vhItem = new PackageVideoAdapter.ViewHolder(v);
        return vhItem;

    }

    @Override
    public void onBindViewHolder(final PackageVideoAdapter.ViewHolder holder, int position) {

//        Glide.with(mContext).load(packageModelsList.get(position).getImage())
//                .error(R.drawable.massage_default)
//                .into(holder.backgroundIV);
        if (packageModelsList.get(position).getImage() == null || packageModelsList.get(position).getImage().equalsIgnoreCase("")) {
            holder.backgroundIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.massage_default));
        } else {
            aQuery.id(holder.backgroundIV).image(packageModelsList.get(position).getImage(), true, true);
        }


        holder.packageTV.setText(packageModelsList.get(position).getPackageName());
        holder.priceTV.setText("$" + packageModelsList.get(position).getPrice());
        holder.tvBulletOne.setText(packageModelsList.get(position).getB1());
        holder.tvBulletTwo.setText(packageModelsList.get(position).getB2());


    }


    @Override
    public int getItemCount() {
        return size;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView backgroundIV;
        TextView packageTV, priceTV, tvBulletOne, tvBulletTwo, tvDetails;

        public ViewHolder(View itemView) {
            super(itemView);

            backgroundIV = (ImageView) itemView.findViewById(R.id.iv_background);
            packageTV = (TextView) itemView.findViewById(R.id.tv_package);
            priceTV = (TextView) itemView.findViewById(R.id.tv_price);
            tvBulletOne = (TextView) itemView.findViewById(R.id.tv_session);
            tvBulletTwo = (TextView) itemView.findViewById(R.id.tv_frequency);


        }
    }

}
