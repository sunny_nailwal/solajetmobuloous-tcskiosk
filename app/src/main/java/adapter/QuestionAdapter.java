package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.solajet.tcskiosk.tcskiosk.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import models.QuestionsModel;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.MyViewHolder> {

    private List<QuestionsModel> userCategoryList;
    private Context context;

    public QuestionAdapter(List<QuestionsModel> userCategoryList, Context context) {
        this.userCategoryList = userCategoryList;
        this.context = context;
    }

    @Override
    public QuestionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(context).inflate(R.layout.question_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuestionAdapter.MyViewHolder holder, int position) {
        QuestionsModel questionsModel = userCategoryList.get(position);
        holder.tvQuation.setText(userCategoryList.get(position).getQuestion());
        holder.tvAnswer.setText(userCategoryList.get(position).getAnswer());

        if (questionsModel.isOnClick()) {
            holder.tvAnswer.setVisibility(View.VISIBLE);
            holder.tvQuation.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);


        } else {
            holder.tvAnswer.setVisibility(View.GONE);
            holder.tvQuation.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);


        }
        holder.bindContent(questionsModel);


    }


    @Override
    public int getItemCount() {
        return userCategoryList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private QuestionsModel questionsModel;
        @BindView(R.id.tv_ques)
        TextView tvQuation;
        @BindView(R.id.tv_ans)
        TextView tvAnswer;

        public MyViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick({R.id.tv_ques})
        void onClick() {
            if (questionsModel.isOnClick()) {
                clearList();
                questionsModel.setOnClick(false);
            } else {
                clearList();
                questionsModel.setOnClick(true);
            }
            notifyDataSetChanged();

        }

        private void clearList() {
            for (QuestionsModel question : userCategoryList) {
                question.setOnClick(false);

            }
        }

        public void bindContent(QuestionsModel question) {
            this.questionsModel = question;
        }
    }


}

