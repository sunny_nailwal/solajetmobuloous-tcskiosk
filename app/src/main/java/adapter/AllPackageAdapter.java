package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.solajet.tcskiosk.activities.AllPackageActivity;
import com.solajet.tcskiosk.activities.PackageDetailsActivity;
import com.solajet.tcskiosk.activities.VideoHomeActivity;
import com.solajet.tcskiosk.tcskiosk.R;

import java.util.List;

import models.AllPackagesModel.VALUE;
import utils.AppSharedPreference;
import utils.Constant;

/**
 * Created by admin1 on 24/10/16.
 */

public class AllPackageAdapter extends RecyclerView.Adapter<AllPackageAdapter.ViewHolder> {


    AQuery aQuery;
    private Context mContext;
    private List<VALUE> packageModelsList;


    public AllPackageAdapter(Context mContext, List<VALUE> packageModelsList) {
        this.mContext = mContext;
        this.packageModelsList = packageModelsList;
        if (aQuery == null) {
            aQuery = new AQuery(mContext);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item_new, parent, false);

        ViewHolder vhItem = new ViewHolder(v);
        return vhItem;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

//        Glide.with(mContext).load(packageModelsList.get(position).getImage())
//                .error(R.drawable.massage_default)
//                .into(holder.backgroundIV);
        if (packageModelsList.get(position).getImage() == null || packageModelsList.get(position).getImage().equalsIgnoreCase("")) {
            holder.backgroundIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.massage_default));
        } else {
            aQuery.id(holder.backgroundIV).image(packageModelsList.get(position).getImage(), true, true);
        }


        holder.packageTV.setText(packageModelsList.get(position).getPackageName());
        holder.priceTV.setText("$" + packageModelsList.get(position).getPrice());
        holder.tvBulletOne.setText(packageModelsList.get(position).getB1());
        holder.tvBulletTwo.setText(packageModelsList.get(position).getB2());

        holder.tvDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((AllPackageActivity) mContext).getSubscription()) {
                    Intent intent = new Intent(mContext, PackageDetailsActivity.class);
                    intent.putExtra(Constant.PACKAGE_ID, packageModelsList.get(holder.getAdapterPosition()).getPackageId());
                    intent.putExtra("subscribe", "subscribe");
                    mContext.startActivity(intent);
                }
                if (((AllPackageActivity) mContext).getChange()) {
                    Intent intent = new Intent(mContext, PackageDetailsActivity.class);
                    intent.putExtra(Constant.PACKAGE_ID, packageModelsList.get(holder.getAdapterPosition()).getPackageId());
                    intent.putExtra("change_package", "change_package");
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, PackageDetailsActivity.class);
                    intent.putExtra(Constant.PACKAGE_ID, packageModelsList.get(holder.getAdapterPosition()).getPackageId());
                    AppSharedPreference.putString(mContext,Constant.PACKAGE_NAME,packageModelsList.get(holder.getAdapterPosition()).getPackageName());
                    AppSharedPreference.putString(mContext,Constant.PACKAGE_ID,packageModelsList.get(holder.getAdapterPosition()).getPackageId());
                    mContext.startActivity(intent);
                }


            }
        });

    }


    @Override
    public int getItemCount() {
        return packageModelsList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView backgroundIV;
        TextView packageTV, priceTV, tvBulletOne, tvBulletTwo, tvDetails;

        public ViewHolder(View itemView) {
            super(itemView);

            backgroundIV = (ImageView) itemView.findViewById(R.id.iv_background);
            packageTV = (TextView) itemView.findViewById(R.id.tv_package);
            priceTV = (TextView) itemView.findViewById(R.id.tv_price);
            tvBulletOne = (TextView) itemView.findViewById(R.id.tv_massage_bullet_one);
            tvBulletTwo = (TextView) itemView.findViewById(R.id.tv_massage_bullet_second);
            tvDetails = (TextView) itemView.findViewById(R.id.tv_details);


        }
    }

}
