package adapter;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.solajet.tcskiosk.tcskiosk.R;

import java.util.ArrayList;

/**
 * Created by appinventiv on 15/12/16.
 */

public class TutorialAdapter extends PagerAdapter {

    private ArrayList<Integer> images;
    private Context context;
    private LayoutInflater inflater;


    public TutorialAdapter(ArrayList<Integer> images, Context context) {
        this.images = images;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.tuts_one, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);


        imageView.setImageResource(images.get(position));

        view.addView(imageLayout, 0);

        return imageLayout;
    }

}
