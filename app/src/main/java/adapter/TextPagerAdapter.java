package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.solajet.tcskiosk.tcskiosk.R;

import java.util.List;


/**
 * ImagePagerAdapter
 *
 * @author <a href="http://www.trinea.cn" target="_blank">Trinea</a> 2014-2-23
 */
public class TextPagerAdapter extends RecyclingPagerAdapter {

    private Context context;
    private List<String> imageIdList;

    private int           size;
    private boolean       isInfiniteLoop;
    private LayoutInflater inflater;

    public TextPagerAdapter(Context context, List<String> imageIdList) {
        this.context = context;
        this.imageIdList = imageIdList;
        this.size =imageIdList.size();
        isInfiniteLoop = false;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // Infinite loop
        return isInfiniteLoop ? Integer.MAX_VALUE :imageIdList.size();
    }

    /**
     * get really position
     *
     * @param position
     * @return
     */
    private int getPosition(int position) {
        return isInfiniteLoop ? position % size : position;
    }

    @Override
    public View getView(int position, View view, ViewGroup container) {
        ViewHolder holder;
//        if (view == null) {
//            holder = new ViewHolder();
//            view = holder.imageView = new ImageView(context);
//            ViewGroup.LayoutParams params= new ViewGroup.LayoutParams()
//            view.setTag(holder);
//        } else {
//            holder = (ViewHolder)view.getTag();
//        }
//        holder.imageView.setImageResource(imageIdList.get(getPosition(position)));
//        return view;

        if (view == null) {
            view = inflater.inflate(R.layout.textview_list_layout, null);
            holder = new ViewHolder();
            holder.imageView = (TextView) view.findViewById(R.id.tv_header);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }
        holder.imageView.setText(imageIdList.get(getPosition(position)));
        return view;
    }

    private static class ViewHolder {

        TextView imageView;
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public TextPagerAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }
}