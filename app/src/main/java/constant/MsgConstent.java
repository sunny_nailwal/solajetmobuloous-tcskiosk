package constant;

/**
 * Created by Administrator on 2015/11/8.
 */
public class MsgConstent {
    public static final int MSG_GETVERSIONINFO = 100;
    public static final int MSG_CONNECTSERVER_FAILED = 101;
    public static final int MSG_DOWNLOAD_SUCCESS = 102;
    public static final int MSG_DOWNLOAD_FAILED = 103;

}
