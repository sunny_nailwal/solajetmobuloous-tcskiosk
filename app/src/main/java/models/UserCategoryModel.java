package models;

/**
 * Created by appinventiv on 1/12/16.
 */

public class UserCategoryModel {

    String userType;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
