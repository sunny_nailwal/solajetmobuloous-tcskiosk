package models.LoginModel;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "packageId",
        "packageName",
        "duration",
        "packageType",
        "price",
        "expireDate",
        "isPayment",
        "isTerm",
        "subscribeId",
        "isManager"
})
public class PacakageDetail {

    @JsonProperty("packageId")
    private String packageId;
    @JsonProperty("packageName")
    private String packageName;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("packageType")
    private String packageType;
    @JsonProperty("price")
    private String price;
    @JsonProperty("expireDate")
    private String expireDate;
    @JsonProperty("isPayment")
    private Integer isPayment;
    @JsonProperty("isTerm")
    private String isTerm;
    @JsonProperty("subscribeId")
    private String subscribeId;
    @JsonProperty("isManager")
    private String isManager;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("isManager")
    public String getIsManager() {
        return isManager;
    }

    @JsonProperty("isManager")
    public void setIsManager(String isManager) {
        this.isManager = isManager;
    }

    @JsonProperty("packageId")
    public String getPackageId() {
        return packageId;
    }

    @JsonProperty("packageId")
    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    @JsonProperty("packageName")
    public String getPackageName() {
        return packageName;
    }

    @JsonProperty("packageName")
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("packageType")
    public String getPackageType() {
        return packageType;
    }

    @JsonProperty("packageType")
    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("expireDate")
    public String getExpireDate() {
        return expireDate;
    }

    @JsonProperty("expireDate")
    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    @JsonProperty("isPayment")
    public Integer getIsPayment() {
        return isPayment;
    }

    @JsonProperty("isPayment")
    public void setIsPayment(Integer isPayment) {
        this.isPayment = isPayment;
    }

    @JsonProperty("isTerm")
    public String getIsTerm() {
        return isTerm;
    }

    @JsonProperty("isTerm")
    public void setIsTerm(String isTerm) {
        this.isTerm = isTerm;
    }

    @JsonProperty("subscribeId")
    public String getSubscribeId() {
        return subscribeId;
    }

    @JsonProperty("subscribeId")
    public void setSubscribeId(String subscribeId) {
        this.subscribeId = subscribeId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
