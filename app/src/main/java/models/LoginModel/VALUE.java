package models.LoginModel;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "userdetail",
        "pacakageDetail",
        "duration",
        "status"
})
public class VALUE {

    @JsonProperty("userdetail")
    private Userdetail userdetail;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("status")
    private String status;
    @JsonProperty("pacakageDetail")
    private List<PacakageDetail> pacakageDetail = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("userdetail")
    public Userdetail getUserdetail() {
        return userdetail;
    }

    @JsonProperty("userdetail")
    public void setUserdetail(Userdetail userdetail) {
        this.userdetail = userdetail;
    }

    @JsonProperty("pacakageDetail")
    public List<PacakageDetail> getPacakageDetail() {
        return pacakageDetail;
    }

    @JsonProperty("pacakageDetail")
    public void setPacakageDetail(List<PacakageDetail> pacakageDetail) {
        this.pacakageDetail = pacakageDetail;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
