package models.CardConnectModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthResponse {
    private String respstat;
    private String retref;
    private String account;
    private String amount;
    private String merchid;
    private String respcode;
    private String resptext;
    private String respproc;
    //returned onSuccess
    private String avsresp;
    private String cvvresp;
    private String authcode;

    public enum ResponseStatus {
        APPROVED, RETRY, DECLINED, UNKNOWN
    }

    public ResponseStatus getResponseStatus() {
        if ("A".equals(respstat)) {
            return ResponseStatus.APPROVED;
        } else if ("B".equals(respstat)) {
            return ResponseStatus.RETRY;
        } else if ("C".equals(respstat)) {
            return ResponseStatus.DECLINED;
        }

        return ResponseStatus.UNKNOWN;
    }

    @JsonProperty("respstat")
    public String getRespstat() {
        return respstat;
    }

    @JsonProperty("respstat")
    public void setRespstat(String respstat) {
        this.respstat = respstat;
    }

    @JsonProperty("retref")
    public String getRetref() {
        return retref;
    }

    @JsonProperty("retref")
    public void setRetref(String retref) {
        this.retref = retref;
    }

    @JsonProperty("account")
    public String getAccount() {
        return account;
    }

    @JsonProperty("account")
    public void setAccount(String account) {
        this.account = account;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("merchid")
    public String getMerchid() {
        return merchid;
    }

    @JsonProperty("merchid")
    public void setMerchid(String merchid) {
        this.merchid = merchid;
    }

    @JsonProperty("respcode")
    public String getRespcode() {
        return respcode;
    }

    @JsonProperty("respcode")
    public void setRespcode(String respcode) {
        this.respcode = respcode;
    }

    @JsonProperty("resptext")
    public String getResptext() {
        return resptext;
    }

    @JsonProperty("resptext")
    public void setResptext(String resptext) {
        this.resptext = resptext;
    }

    @JsonProperty("respproc")
    public String getRespproc() {
        return respproc;
    }

    @JsonProperty("respproc")
    public void setRespproc(String respproc) {
        this.respproc = respproc;
    }

    @JsonProperty("avsresp")
    public String getAvsresp() {
        return avsresp;
    }

    @JsonProperty("avsresp")
    public void setAvsresp(String avsresp) {
        this.avsresp = avsresp;
    }

    @JsonProperty("cvvresp")
    public String getCvvresp() {
        return cvvresp;
    }

    @JsonProperty("cvvresp")
    public void setCvvresp(String cvvresp) {
        this.cvvresp = cvvresp;
    }

    @JsonProperty("authcode")
    public String getAuthcode() {
        return authcode;
    }

    @JsonProperty("authcode")
    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }
}
