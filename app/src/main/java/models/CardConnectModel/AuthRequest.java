package models.CardConnectModel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.solajet.tcskiosk.tcskiosk.BuildConfig;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthRequest {
    private final String merchid = BuildConfig.CARD_CONNECT_MERCHANT_ID;
    private final String amount;
    private final String currency = "USD";
    private final String expiry;
    private final String account;
    private final String track;
    private final String profile;
    private final String acctid;
    private final String capture = "Y";
    private final String cvv2;
    private final String name;
    private final String address;
    private final String city;
    private final String postal;
    private final String region;
    private final String country = "US";
    private final String phone;
    private final String email;
    private final String orderid;
    private final String termid;
    private final String accttype;
    private final String ecomind = "E";

    private AuthRequest(Builder builder) {
        amount = builder.amount;
        expiry = builder.expiry;
        account = builder.account;
        profile = builder.profile;
        acctid = builder.acctid;
        cvv2 = builder.cvv2;
        name = builder.name;
        address = builder.address;
        city = builder.city;
        postal = builder.postal;
        region = builder.region;
        phone = builder.phone;
        email = builder.email;
        orderid = builder.orderid;
        termid = builder.termid;
        accttype = builder.accttype;

        StringBuilder sb = new StringBuilder();

        if (builder.track1 != null) {
            sb.append(builder.track1);
        }
        if (builder.track2 != null) {
            sb.append(builder.track2);
        }

        if (sb.length() > 0) {
            track = sb.toString();
        }else {
            track = null;
        }
    }

    @JsonProperty("merchid")
    public String getMerchid() {
        return merchid;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("expiry")
    public String getExpiry() {
        return expiry;
    }

    @JsonProperty("account")
    public String getAccount() {
        return account;
    }

    @JsonProperty("track")
    public String getTrack() {
        return track;
    }

    @JsonProperty("profile")
    public String getProfile() {
        return profile;
    }

    @JsonProperty("acctid")
    public String getAcctId() {
        return acctid;
    }

    @JsonProperty("capture")
    public String getCapture() {
        return capture;
    }

    @JsonProperty("cvv2")
    public String getCvv2() {
        return cvv2;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("postal")
    public String getPostal() {
        return postal;
    }

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("orderid")
    public String getOrderid() {
        return orderid;
    }

    @JsonProperty("termid")
    public String getTermid() {
        return termid;
    }

    @JsonProperty("accttype")
    public String getAccttype() {
        return accttype;
    }

    public static class Builder {
        private String amount;
        private String expiry;
        private String account;
        private String profile;
        private String acctid;
        private String cvv2;
        private String name;
        private String address;
        private String city;
        private String postal;
        private String region;
        private String phone;
        private String email;
        private String orderid;
        private String termid;
        private String accttype;
        private String track1;
        private String track2;

        public Builder() {
        }

        public Builder setAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public Builder setExpiry(String expiry) {
            this.expiry = expiry;
            return this;
        }

        public Builder setAccount(String account) {
            this.account = account;
            return this;
        }

        public Builder setProfile(String profile) {
            this.profile = profile;
            return this;
        }

        public Builder setAcctid(String acctid) {
            this.acctid = acctid;
            return this;
        }

        public Builder setCvv2(String cvv2) {
            this.cvv2 = cvv2;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setCity(String city) {
            this.city = city;
            return this;
        }

        public Builder setPostal(String postal) {
            this.postal = postal;
            return this;
        }

        public Builder setRegion(String region) {
            this.region = region;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setOrderid(String orderid) {
            this.orderid = orderid;
            return this;
        }

        public Builder setTermid(String termid) {
            this.termid = termid;
            return this;
        }

        public Builder setAccttype(String accttype) {
            this.accttype = accttype;
            return this;
        }

        public Builder setTrack1(String track1) {
            this.track1 = track1;
            return this;
        }

        public Builder setTrack2(String track2) {
            this.track2 = track2;
            return this;
        }

        public AuthRequest build() {
            return new AuthRequest(this);
        }
    }

}
