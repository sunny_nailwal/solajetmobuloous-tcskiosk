package models.CardConnectModel;

public class CardSwipeData {
    String cardNumber;
    String expirationMonth;
    String expirationYear;
    String cardHolderName;
    String track1;
    String track2;

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public String getTrack1() {
        return track1;
    }

    public String getTrack2() {
        return track2;
    }

    public static CardSwipeData parseSwipe(String swipeData) {
        //Sample Data
        //%B4111111111111111^Card Holder/DI^210800000000000000000000000?*;4111111111111111=21080000000000000000?*

        final String track1 = extractData("%", "?", swipeData, true); //extractTrack1(swipeData);
        final String track2 = extractData(";", "?", swipeData, true);

        if (track1 == null && track2 == null) {
            return null;
        }

        CardSwipeData result = new CardSwipeData();

        if (track1 != null) {
            result.cardHolderName = extractData("^", "^", track1);
        }

        String expDate = null;

        if (track1 != null) {
            final int cardNumberEndIdx = track1.indexOf("^");
            result.cardNumber = track1.substring(2, cardNumberEndIdx);
            final int expirationStartIdx = track1.lastIndexOf("^");
            expDate = track1.substring(expirationStartIdx + 1, expirationStartIdx + 5);
        } else if (track2 != null) {
            final int cardNumberEndIdx = track2.indexOf("=");
            result.cardNumber = track2.substring(1, cardNumberEndIdx);
            expDate = track2.substring(cardNumberEndIdx + 1, cardNumberEndIdx + 5);
        }

        result.expirationMonth = expDate.substring(2);
        result.expirationYear = expDate.substring(0, 2);
        result.track1 = track1;
        result.track2 = track2;

        return result;
    }

    private static String extractData(String startChar, String endChar, String input) {
        return extractData(startChar, endChar, input, false);
    }

    private static String extractData(String startChar, String endChar, String input, boolean retainTerminators) {
        int adjustIdx = 0;
        if (retainTerminators) {
            adjustIdx = 1;
        }

        int startIdx = input.indexOf(startChar);
        int endIdx = startIdx >= 0 ? input.indexOf(endChar, startIdx + 1) : -1;

        if (startIdx >= 0 && endIdx >= 0) {
            return input.substring(startIdx + 1 - adjustIdx, endIdx + adjustIdx); //don't include boundary chars
        }

        return null;
    }
}
