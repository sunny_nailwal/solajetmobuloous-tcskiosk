package models.MassageEnd;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "CODE",
        "APICODERESULT",
        "MESSAGE"
})
public class MassageEndBean {

    @JsonProperty("CODE")
    private Integer cODE;
    @JsonProperty("APICODERESULT")
    private String aPICODERESULT;
    @JsonProperty("MESSAGE")
    private String mESSAGE;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The cODE
     */
    @JsonProperty("CODE")
    public Integer getCODE() {
        return cODE;
    }

    /**
     * @param cODE The CODE
     */
    @JsonProperty("CODE")
    public void setCODE(Integer cODE) {
        this.cODE = cODE;
    }

    /**
     * @return The aPICODERESULT
     */
    @JsonProperty("APICODERESULT")
    public String getAPICODERESULT() {
        return aPICODERESULT;
    }

    /**
     * @param aPICODERESULT The APICODERESULT
     */
    @JsonProperty("APICODERESULT")
    public void setAPICODERESULT(String aPICODERESULT) {
        this.aPICODERESULT = aPICODERESULT;
    }

    /**
     * @return The mESSAGE
     */
    @JsonProperty("MESSAGE")
    public String getMESSAGE() {
        return mESSAGE;
    }

    /**
     * @param mESSAGE The MESSAGE
     */
    @JsonProperty("MESSAGE")
    public void setMESSAGE(String mESSAGE) {
        this.mESSAGE = mESSAGE;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
