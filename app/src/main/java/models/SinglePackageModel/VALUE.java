package models.SinglePackageModel;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "packageId",
        "packageName",
        "b1",
        "b2",
        "b3",
        "pacDesc",
        "image",
        "nuMassage",
        "price",
        "duration",
        "packageType",
        "expireDate"
})
public class VALUE {

    @JsonProperty("packageId")
    private String packageId;
    @JsonProperty("packageName")
    private String packageName;
    @JsonProperty("b1")
    private String b1;
    @JsonProperty("b2")
    private String b2;
    @JsonProperty("b3")
    private String b3;
    @JsonProperty("pacDesc")
    private String pacDesc;
    @JsonProperty("image")
    private String image;
    @JsonProperty("nuMassage")
    private Integer nuMassage;
    @JsonProperty("price")
    private String price;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("packageType")
    private Integer packageType;
    @JsonProperty("expireDate")
    private String expireDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("packageId")
    public String getPackageId() {
        return packageId;
    }

    @JsonProperty("packageId")
    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    @JsonProperty("packageName")
    public String getPackageName() {
        return packageName;
    }

    @JsonProperty("packageName")
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @JsonProperty("b1")
    public String getB1() {
        return b1;
    }

    @JsonProperty("b1")
    public void setB1(String b1) {
        this.b1 = b1;
    }

    @JsonProperty("b2")
    public String getB2() {
        return b2;
    }

    @JsonProperty("b2")
    public void setB2(String b2) {
        this.b2 = b2;
    }

    @JsonProperty("b3")
    public String getB3() {
        return b3;
    }

    @JsonProperty("b3")
    public void setB3(String b3) {
        this.b3 = b3;
    }

    @JsonProperty("pacDesc")
    public String getPacDesc() {
        return pacDesc;
    }

    @JsonProperty("pacDesc")
    public void setPacDesc(String pacDesc) {
        this.pacDesc = pacDesc;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("nuMassage")
    public Integer getNuMassage() {
        return nuMassage;
    }

    @JsonProperty("nuMassage")
    public void setNuMassage(Integer nuMassage) {
        this.nuMassage = nuMassage;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("packageType")
    public Integer getPackageType() {
        return packageType;
    }

    @JsonProperty("packageType")
    public void setPackageType(Integer packageType) {
        this.packageType = packageType;
    }

    @JsonProperty("expireDate")
    public String getExpireDate() {
        return expireDate;
    }

    @JsonProperty("expireDate")
    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
