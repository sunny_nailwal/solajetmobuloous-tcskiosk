package models;

/**
 * Created by admin1 on 27/10/16.
 */

public class SubscribePackageModel {

    int topimg;
    String title, time, generaltext, msgleftcount, expiredtime;

    public SubscribePackageModel(int topimg, String title, String time, String generaltext, String msgleftcount, String expiredtime) {
        this.topimg = topimg;
        this.time = time;
        this.generaltext = generaltext;
        this.msgleftcount = msgleftcount;
        this.expiredtime = expiredtime;
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public int getTopimg() {
        return topimg;
    }

    public String getGeneraltext() {
        return generaltext;
    }

    public String getExpiredtime() {
        return expiredtime;
    }

    public String getMsgleftcount() {
        return msgleftcount;
    }

    public String getTitle() {
        return title;
    }
}
