package models.EmployeeLogin;

/**
 * Created by appinventiv on 12/12/16.
 */

public class EmployeeLoginBean {

    String employeeId, employeePascode, employeeMassageDuration;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeePascode() {
        return employeePascode;
    }

    public void setEmployeePascode(String employeePascode) {
        this.employeePascode = employeePascode;
    }

    public String getEmployeeMassageDuration() {
        return employeeMassageDuration;
    }

    public void setEmployeeMassageDuration(String employeeMassageDuration) {
        this.employeeMassageDuration = employeeMassageDuration;
    }
}
