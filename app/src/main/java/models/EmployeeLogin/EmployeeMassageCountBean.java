package models.EmployeeLogin;

/**
 * Created by appinventiv on 12/12/16.
 */

public class EmployeeMassageCountBean {

    private String empId, empDuration;
    private int empCompletion;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpDuration() {
        return empDuration;
    }

    public void setEmpDuration(String empDuration) {
        this.empDuration = empDuration;
    }

    public int getEmpCompletion() {
        return empCompletion;
    }

    public void setEmpCompletion(int empCompletion) {
        this.empCompletion = empCompletion;
    }
}
