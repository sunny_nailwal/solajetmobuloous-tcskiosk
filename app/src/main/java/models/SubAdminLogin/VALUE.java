package models.SubAdminLogin;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "subAdminId",
        "email",
        "session"
})
public class VALUE {

    @JsonProperty("subAdminId")
    private String subAdminId;
    @JsonProperty("email")
    private String email;
    @JsonProperty("session")
    private String session;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The subAdminId
     */
    @JsonProperty("subAdminId")
    public String getSubAdminId() {
        return subAdminId;
    }

    /**
     * @param subAdminId The subAdminId
     */
    @JsonProperty("subAdminId")
    public void setSubAdminId(String subAdminId) {
        this.subAdminId = subAdminId;
    }

    /**
     * @return The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The session
     */
    @JsonProperty("session")
    public String getSession() {
        return session;
    }

    /**
     * @param session The session
     */
    @JsonProperty("session")
    public void setSession(String session) {
        this.session = session;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
