package models.MemberLogin;

/**
 * Created by appinventiv on 13/12/16.
 */

public class MemberLoginBean {

    String userId;
    String memberId;
    String memberName;
    String memberPasscode;
    String packageName;
    String packageType;
    String packageAmount;
    String packageExpiryDate;
    String packageSubscriptionStatus;
    String packageAmountStatus;
    String packageMassageCount;

    public String getMemberPasscode() {
        return memberPasscode;
    }

    public void setMemberPasscode(String memberPasscode) {
        this.memberPasscode = memberPasscode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getPackageAmount() {
        return packageAmount;
    }

    public void setPackageAmount(String packageAmount) {
        this.packageAmount = packageAmount;
    }

    public String getPackageExpiryDate() {
        return packageExpiryDate;
    }

    public void setPackageExpiryDate(String packageExpiryDate) {
        this.packageExpiryDate = packageExpiryDate;
    }

    public String getPackageSubscriptionStatus() {
        return packageSubscriptionStatus;
    }

    public void setPackageSubscriptionStatus(String packageSubscriptionStatus) {
        this.packageSubscriptionStatus = packageSubscriptionStatus;
    }

    public String getPackageAmountStatus() {
        return packageAmountStatus;
    }

    public void setPackageAmountStatus(String packageAmountStatus) {
        this.packageAmountStatus = packageAmountStatus;
    }

    public String getPackageMassageCount() {
        return packageMassageCount;
    }

    public void setPackageMassageCount(String packageMassageCount) {
        this.packageMassageCount = packageMassageCount;
    }
}
