package models.SignUpModel;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "userdetail",
        "pacakageDetail"
})
public class VALUE {

    @JsonProperty("userdetail")
    private Userdetail userdetail;
    @JsonProperty("pacakageDetail")
    private List<Object> pacakageDetail = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("userdetail")
    public Userdetail getUserdetail() {
        return userdetail;
    }

    @JsonProperty("userdetail")
    public void setUserdetail(Userdetail userdetail) {
        this.userdetail = userdetail;
    }

    @JsonProperty("pacakageDetail")
    public List<Object> getPacakageDetail() {
        return pacakageDetail;
    }

    @JsonProperty("pacakageDetail")
    public void setPacakageDetail(List<Object> pacakageDetail) {
        this.pacakageDetail = pacakageDetail;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
