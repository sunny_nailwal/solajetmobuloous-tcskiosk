package customviews;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class NoKeyboardEditText extends AppCompatEditText {

    public NoKeyboardEditText(Context context) {
        super(context);
    }

    public NoKeyboardEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoKeyboardEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onCheckIsTextEditor() {
        return false;
    }
}
