package customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.solajet.tcskiosk.tcskiosk.R;


/**
 * Created by appinventiv on 17/3/16.
 */
public class TouchScreenEditText extends AppCompatEditText {
    public TouchScreenEditText(Context context) {
        super(context);
    }

    public TouchScreenEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public TouchScreenEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        Typeface myTypeface;
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TouchScreenEditText);
            String fontName = a.getString(R.styleable.TouchScreenEditText_fontName);
            if (fontName.equalsIgnoreCase("black")) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "ProximaNova-Black.otf");
                setTypeface(myTypeface);
            } else if (fontName.equalsIgnoreCase("bold")) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "ProximaNova-Bold.otf");
                setTypeface(myTypeface);
            } else if (fontName.equalsIgnoreCase("light")) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "ProximaNova-Light.otf");
                setTypeface(myTypeface);
            } else if (fontName.equalsIgnoreCase("light_it")) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "ProximaNova-LightItalic.otf");
                setTypeface(myTypeface);
            } else if (fontName.equalsIgnoreCase("reg_it")) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "ProximaNova-RegItalic.otf");
                setTypeface(myTypeface);
            } else if (fontName.equalsIgnoreCase("regular")) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "ProximaNova-Regular.otf");
                setTypeface(myTypeface);
            } else if (fontName.equalsIgnoreCase("regular_it")) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "ProximaNova-RegularItalic.otf");
                setTypeface(myTypeface);
            } else if (fontName.equalsIgnoreCase("semibold")) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "ProximaNova-Semibold.otf");
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }
}
