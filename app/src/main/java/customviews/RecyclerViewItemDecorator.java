package customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.solajet.tcskiosk.tcskiosk.R;


/**
 * Created by appinventiv on 14/12/16.
 */

public class RecyclerViewItemDecorator extends RecyclerView.ItemDecoration {

    Drawable divider;

    public RecyclerViewItemDecorator(Context context) {
        this.divider = ContextCompat.getDrawable(context, R.drawable.packages_item_bg);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {

        int childCount = parent.getChildCount();

        for (int i = 0; i < childCount; i++) {

            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            divider.draw(c);
        }
    }
}
