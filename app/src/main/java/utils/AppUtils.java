package utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.solajet.tcskiosk.activities.AllPackageActivity;
import com.solajet.tcskiosk.activities.HomeActivity;
import com.solajet.tcskiosk.activities.ScrollingSplashActivity;
import com.solajet.tcskiosk.activities.TermsAndCondition;
import com.solajet.tcskiosk.activities.VideoHomeActivity;
import com.solajet.tcskiosk.tcskiosk.R;


/**
 * Created by Dev  Sharma on 06-09-2016.
 */
public class AppUtils {
    private static AppUtils appUtils;
    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    //    private  String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    boolean res = false;
    private ProgressDialog mDialog;

    public static AppUtils getInstance() {
        if (appUtils == null) {
            appUtils = new AppUtils();
        }
        return appUtils;
    }

    /**
     * Validate blank strings
     *
     * @param text
     * @return
     */
    public boolean validate(String text) {
        return text != null && text.length() > 0;
    }

    /**
     * check Email
     *
     * @param target
     * @return
     */
    public boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

//    /**
//     * show snackbar
//     *
//     * @param message
//     * @param view
//     */
//    public void showSnackBar(final Context context, String message, View view) {
//        Snackbar snackbar;
//        snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
//        View snackBarView = snackbar.getView();
//        snackBarView.setBackgroundColor(Color.parseColor("#000000"));
//        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.parseColor("#db0054"));
//        textView.setTextSize(18f);
//        snackbar.setAction("Retry", new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkInstance(context);
//            }
//        });
//
//        snackbar.show();
//    }


//    private void checkInstance(Context context)
//    {
//        if(context instanceof SignUpActivity)
//        {
//            ((SignUpActivity) context).signUpApi();
//        }
//        else if(context instanceof LoginActivity)
//        {
//            ((LoginActivity) context).GuestLogInApi();
//        }
//        else if(context instanceof GuestLoginActivity)
//        {
//            ((GuestLoginActivity) context).GuestLogInApi();
//        }
//        else if(context instanceof PackageOptionActivity)
//        {
//            ((PackageOptionActivity) context).getPackages();
//        }
//        else if(context instanceof ForgotPasscodeActivity)
//        {
//            ((ForgotPasscodeActivity) context).forgotPasscodeApi();
//        }
//    }

    /**
     * check Mobile Number
     *
     * @param target
     * @return
     */
    public boolean isValidNumber(CharSequence target) {
        return !(target == null || target.length() < 10);
    }

    public void changeFocusToNext(final EditText text1, final EditText text2) {
        text1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (text1.getText().toString().length() == 1) {
                    text2.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable s) {
            }

        });
    }

    public void changeFocusToPrevious(final EditText text1, final EditText text2) {
        text1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (text1.getText().toString().length() == 0) {
                    text2.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable s) {
            }

        });
    }

    public void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /**
     * show progress dialog
     */
    public void showProgressDialog(Context context, String msg) {
        mDialog = new ProgressDialog(context, R.style.Custom_Dialog_theme);
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setIndeterminateDrawable(ContextCompat.getDrawable(context, R.drawable.custom_progress_dialog));
        mDialog.show();


    }


    /**
     * hide progress dialog
     */
    public void hideProgressDialog() {
        if (mDialog != null)
            mDialog.dismiss();
    }

    public void hideNativeKeyboard(Activity pActivity) {
        if (pActivity != null) {
            if (pActivity.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) pActivity.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(pActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    public boolean isValifEmailId(String email) {
        return email.matches(EMAIL_PATTERN);
    }


    /*
    * Shows the MemberShipExpireDialog
   */
    public void showDialog(Activity activity, String message, String button_text, final int act) {

        final Activity context = activity;
        final int action = act;
        TextView submitTV;
        ImageView crossIV;
        final Dialog verifyDialog = new Dialog(context);
        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifyDialog.setContentView(R.layout.dialog_login_screen);
        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifyDialog.setCancelable(false);

        if (act == 4) {
            verifyDialog.findViewById(R.id.iv_dialog_cross).setVisibility(View.GONE);
        }

        ((TextView) verifyDialog.findViewById(R.id.tv_dialog_message)).setText(message);
        submitTV = (TextView) verifyDialog.findViewById(R.id.tv_dialog_action_text);
        submitTV.setText(button_text);
        submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentMain;
                switch (action) {

                    case 1:
                        intentMain = new Intent(context, HomeActivity.class);
                        intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intentMain);
                        context.finish();
                        break;

                    case 2:
                        intentMain = new Intent(context, AllPackageActivity.class);
                        intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intentMain.putExtra("subscribe", "subscribe");
                        context.startActivity(intentMain);
                        context.finish();
                        break;

                    case 3:
                        intentMain = new Intent(context, TermsAndCondition.class);
                        intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intentMain);
                        context.finish();
                        break;

                    case 4:
                        verifyDialog.dismiss();
                        break;

                    default:
                        intentMain = new Intent(context, HomeActivity.class);
                        intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intentMain);
                        context.finish();

                }


            }
        });


        //if we close the dialog close the app
        crossIV = (ImageView) verifyDialog.findViewById(R.id.iv_dialog_cross);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyDialog.dismiss();
                Intent intentMain = new Intent(context, HomeActivity.class);
                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intentMain);
                context.finish();
            }
        });

        verifyDialog.show();

    }

    public void showNewEnrollDialog(Activity activity) {

        final Activity context = activity;
        final Dialog verifyDialog = new Dialog(context);
        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifyDialog.setContentView(R.layout.new_enrollee_dialog);
        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifyDialog.setCancelable(false);


        verifyDialog.findViewById(R.id.txt_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentMain;

                intentMain = new Intent(context, VideoHomeActivity.class);
                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intentMain);
                context.finish();


            }
        });

        verifyDialog.show();

    }
}
