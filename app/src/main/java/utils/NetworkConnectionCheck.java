package utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by mobulous8 on 3/6/16.
 */
public class NetworkConnectionCheck {
    private static Context context;
    private static NetworkConnectionCheck networkConnectionCheck;

    public static NetworkConnectionCheck instanceOf(Context ctx) {
        if (networkConnectionCheck != null) {
            context = ctx;
            return networkConnectionCheck;
        } else {
            networkConnectionCheck = new NetworkConnectionCheck();
            context = ctx;
            return networkConnectionCheck;
        }
    }


    public boolean isConnect() {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }

        return false;
    }
}