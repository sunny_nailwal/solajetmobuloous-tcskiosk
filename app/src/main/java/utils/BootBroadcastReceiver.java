package utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.solajet.tcskiosk.activities.SplashActivity;


/**
 * Created by Dennis on 2015/12/8.
 */
public class BootBroadcastReceiver extends BroadcastReceiver {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub

        System.out.println("BootBroadcastReceiver");

        Intent in = new Intent();
        in.setClass(context.getApplicationContext(), SplashActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(in);
    }

}
