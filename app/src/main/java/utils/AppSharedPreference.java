package utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class AppSharedPreference {


    public static int getInt(Context context, String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        int value = sharedPref.getInt(key, 0);
        return value;
    }

    public static void putInt(Context context, String key, int value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void putLong(Context context, String key, long value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static long getLong(Context context, String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        long value = sharedPref.getLong(key, 0);
        return value;
    }


    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.commit();
        editor.apply();
    }

    public static boolean getBoolean(Context context, String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(key, false);
    }

    public static void putString(Context context, String key, String value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
        editor.apply();
    }

    public static String getString(Context context, String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(key, null);
    }

//    public static void clearAllPrefs(Context context) {
//        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.clear();
//        editor.commit();
//    }


    public static void clearUserRelatedPref(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = sharedPreferences.edit();

        edit.remove(Constant.USER_ID);
        edit.remove(Constant.MEMBER_ID);
        edit.remove(Constant.USER_NAME);


        edit.remove(Constant.IS_USER);
        edit.remove(Constant.IS_GUEST);
        edit.remove(Constant.IS_MANAGER);
        edit.remove(Constant.MANAGER_LOGIN);

        edit.remove(Constant.PACKAGE_SESSION_TIME);
        edit.remove(Constant.PACKAGE_ID);
        edit.remove(Constant.PACKAGE_AMOUNT);

        edit.remove(Constant.MANAGER_ID);
        edit.remove(Constant.EMPLOYEE_ID);
        edit.remove(Constant.EMPLOEE_MASSAGE_DURATION);

        edit.apply();
        edit.commit();

    }
}
