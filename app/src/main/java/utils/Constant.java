package utils;

import android.util.Base64;

import com.solajet.tcskiosk.tcskiosk.BuildConfig;

/**
 * Created by admin on 23-09-2016.
 */
public class Constant {

    public static String PACKAGE_NAME = "package_name";
    public static String DEVICE_MAC_ADDRESS = "device_mac_address";
    //    public static String BASE_URL = "http://touchscreen.applaurels.com/api/";
    //public static String BASE_URL = "http://mobulous.co.in/touchscreen/api/";
    public static String BASE_URL = "https://166.62.124.148/touchscreen/api/";
    public static String BASE_URL_UPDATE = "http://121.43.101.2:3000";
    public static String AccessToken = "Accesstoken";
    public static String AccessToken_Value = "45jdsklajasg64fdajkl";
    public static String USER_NAME = "fullName";
    public static String LAST_NAME = "lastName";
    public static String USER_ID = "userId";
    public static String USER_EMAIL_ID = "emailId";
    public static String SIGNATURE = "signature";
    public static String IS_USER = "isUser";
    public static String IS_PAYMENT = "isPayment";
    public static String SINGLE_SESSION = "single_session";
    public static String MANAGER_ID = "email";
    public static String EMPLOYEE_ID = "subAdminId";
    public static String EMPLOEE_MASSAGE_DURATION = "admin_massage_duration";
    public static String IS_MANAGER = "isSubAdmin";
    public static String GUEST_MASSAGE_DURATION = "guest_massage_duration";
    public static String IS_GUEST = "isGuest";
    public static String MEMBER_PASSCODE = "password";
    public static String MEMBER_ID = "memberId";
    public static String MEMBER_PHONE_NUMBER = "phone";
    public static String OWNER_ID = "ownerId";
    public static String PACKAGE_ID = "packageId";
    public static String PACKAGE_SESSION_TIME = "packageMassageTime";
    public static String PACKAGE_AMOUNT = "packageAmount";
    public static String SUBSCRIPTION_ID = "subscribeId";
    public static String CLUB_NAME = "cName";
    public static String ACTION = "action";
    public static String MANAGER_LOGIN = "manager_login";
    public static String CODE = "code";
    public static String GUEST_LOCK_CODE = "guest_lock_code";
    public static String TEMP_MAC_ADDRESS = "temp_mac_address";

    public static String STRIPE_TOKEN = "stripe_token";

    //CardConnect values
    public static final String CARD_CONNECT_BASE_URL = "https://" + BuildConfig.CARD_CONNECT_HOST_NAME + "/cardconnect/rest/";

    public static final String CARD_CONNECT_AUTH_HEADER;
    public static final String CARD_CONNECT_CREDENTIALS = BuildConfig.CARD_CONNECT_USERNAME + ":" + BuildConfig.CARD_CONNECT_PASSWORD;

    static {
        CARD_CONNECT_AUTH_HEADER = "Basic " + Base64.encodeToString(CARD_CONNECT_CREDENTIALS.getBytes(), Base64.NO_WRAP);
    }

}
