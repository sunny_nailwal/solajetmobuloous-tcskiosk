package utils;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;

import interfaces.ApiInterface;
import interfaces.GuestLoginAvailabilityListener;
import models.LoginModel.LoginModel;
import network.RestApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by mobulous1 on 12/5/17.
 */

public class GuestAvailabilityThread extends HandlerThread {
    private static final int MESSAGE_PRIMARY_UPDATED = 0;
    private static final String TAG = "IconUpdateThread";
    private final Context context;
    private Handler requestHandler;
    private Handler responseHandler;
    private GuestLoginAvailabilityListener listener;
    private ApiInterface service;


    public GuestAvailabilityThread(Handler responseHandler, GuestLoginAvailabilityListener listener, Context context) {
        super(TAG);
        this.responseHandler = responseHandler;
        this.listener = listener;
        this.context = context;
    }


    private void handleRequest(final LoginModel model) {
        if (model != null && model.getVALUE() != null && model.getVALUE().getStatus() != null) {
            responseHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (model.getCODE().toString().equals("200")) {
                        listener.onCheckAvailability(Integer.parseInt(model.getVALUE().getStatus()));
                    } else if (model.getCODE().toString().equals("401")) {
                        listener.onCheckAvailabilityOnFailure();
                    } else {
                        listener.onCheckAvailabilityOnFailure();
                    }
                }
            });
        } else {
            listener.onCheckAvailabilityOnFailure();
        }

    }


    @Override
    protected void onLooperPrepared() {

        requestHandler = new GuestAvailabilityThread.RequestHandler(this);
    }

    public void updateInstructions() {
        guestAvailabilityService();
    }


    public void guestAvailabilityService() {
        if (service == null) {
            service = RestApi.createService(ApiInterface.class);
        }


        HashMap<String, String> params = new HashMap<>();

        params.put(Constant.OWNER_ID, AppSharedPreference.getString(context, Constant.OWNER_ID));
        params.put(Constant.ACTION, "guestmassage");

        Call<ResponseBody> call = service.guestAvailability(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String str = "";
                try {
                    if (response != null && response.body() != null) {

                        str = response.body().string();
                        ObjectMapper mapper = new ObjectMapper();
                        LoginModel loginModel = mapper.readValue(str, LoginModel.class);

                        if (loginModel.getCODE().toString().equals("200")) {
                            requestHandler.obtainMessage(MESSAGE_PRIMARY_UPDATED, loginModel).sendToTarget();
                        } else if (loginModel.getCODE().toString().equals("401")) {
                            requestHandler.obtainMessage(MESSAGE_PRIMARY_UPDATED, loginModel).sendToTarget();
                        } else {
                            requestHandler.obtainMessage(MESSAGE_PRIMARY_UPDATED, loginModel).sendToTarget();
                        }
                    } else {
                        requestHandler.obtainMessage(MESSAGE_PRIMARY_UPDATED, null).sendToTarget();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                requestHandler.obtainMessage(MESSAGE_PRIMARY_UPDATED, null).sendToTarget();
            }
        });

    }


    private static class RequestHandler extends Handler {
        //Using a weak reference means you won't prevent garbage collection
        private final WeakReference<GuestAvailabilityThread> weakReference;

        RequestHandler(GuestAvailabilityThread thread) {
            weakReference = new WeakReference<>(thread);
        }

        @Override
        public void handleMessage(Message msg) {
            GuestAvailabilityThread updateThread = this.weakReference.get();
            if (updateThread != null) {
                if (msg.what == MESSAGE_PRIMARY_UPDATED) {
                    LoginModel model = (LoginModel) msg.obj;
                    updateThread.handleRequest(model);

                }
            }
        }
    }

}
