package utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

/**
 * Created by Administrator on 2015/11/8.
 */
public class SPUtils {
    public static final String FILE_NAME = "Solajet";

    /*-
     * SP保存数据方法封装
     */
    public static void put(Context context, String key, Object value) {

        SharedPreferences sharedpreference
                = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreference.edit();

        if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else {
            editor.putString(key, value.toString());
        }

        editor.commit();
    }

    /*-
     * 获取键值封装
     */
    public static Object get(Context context, String key, Object value) {

        SharedPreferences sp
                = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        if (value instanceof String) {
            return sp.getString(key, (String) value);
        } else if (value instanceof Integer) {
            return sp.getInt(key, (Integer) value);
        } else if (value instanceof Boolean) {
            return sp.getBoolean(key, (Boolean) value);
        } else if (value instanceof Float) {
            return sp.getFloat(key, (Float) value);
        } else if (value instanceof Long) {
            return sp.getLong(key, (Long) value);
        }

        return sp.getString(key, (String) value);
    }

    //移除某一键值
    public static void remove(Context context, String key) {
        SharedPreferences sharedpreference
                = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreference.edit();

        editor.remove(key);
    }

    //清楚所有数据
    public static void clear(Context context) {
        SharedPreferences sharedpreference
                = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreference.edit();

        editor.clear();
    }

    //是否包含某一数据
    public static boolean contains(Context context, String key) {
        SharedPreferences sharedpreference
                = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        return sharedpreference.contains(key);
    }

    //获取全部数据
    public static Map<String, ?> getAll(Context context) {
        SharedPreferences sharedpreference
                = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        return sharedpreference.getAll();
    }
}
