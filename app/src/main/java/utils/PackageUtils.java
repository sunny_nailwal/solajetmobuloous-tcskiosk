package utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by Administrator on 2015/11/8.
 */
public class PackageUtils {
    public static int getAppVersionCode(Context context) {

        if (null != context) {
            PackageManager pm = context.getPackageManager();
            if (null != pm) {
                PackageInfo pi;
                try {
                    pi = pm.getPackageInfo(context.getPackageName(), 0);
                    if (null != pi) {
                        return pi.versionCode;
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return -1;
    }

    public static String getAppVersionName(Context context) {
        if (null != context) {
            PackageManager pm = context.getPackageManager();
            if (null != pm) {
                PackageInfo pi;
                try {
                    pi = pm.getPackageInfo(context.getPackageName(), 0);
                    if (null != pi) {
                        return pi.versionName;
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
