package models.CardConnectModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(BlockJUnit4ClassRunner.class)
public class CardSwipeDataTest {

    public static final String TRACK_1_AND_2_READ = "%B4111111111111111^Card Holder/DI^210800000000000000000000000?*;4111111111111111=21080000000000000000?*";
    public static final String TRACK_1_READ = "%B4111111111111111^Card Holder/DI^210800000000000000000000000?*";
    public static final String TRACK_2_READ = ";4111111111111111=21080000000000000000?*";

    @Test
    public void test_ExtractCardResullsFromTrack1And2Read() {
        final CardSwipeData cardSwipeData = CardSwipeData.parseSwipe(TRACK_1_AND_2_READ);

        assertEquals("4111111111111111", cardSwipeData.cardNumber);
        assertEquals("Card Holder/DI", cardSwipeData.cardHolderName);
        assertEquals("08", cardSwipeData.expirationMonth);
        assertEquals("21", cardSwipeData.expirationYear);
        assertEquals("%B4111111111111111^Card Holder/DI^210800000000000000000000000?", cardSwipeData.track1);
        assertEquals(";4111111111111111=21080000000000000000?", cardSwipeData.track2);
    }

    @Test
    public void test_ExtractCardResullsFromTrack1Read() {
        final CardSwipeData cardSwipeData = CardSwipeData.parseSwipe(TRACK_1_READ);

        assertEquals("4111111111111111", cardSwipeData.cardNumber);
        assertEquals("Card Holder/DI", cardSwipeData.cardHolderName);
        assertEquals("08", cardSwipeData.expirationMonth);
        assertEquals("21", cardSwipeData.expirationYear);
        assertEquals("%B4111111111111111^Card Holder/DI^210800000000000000000000000?", cardSwipeData.track1);
        assertNull(cardSwipeData.track2);
    }

    @Test
    public void test_ExtractCardResullsFromTrack2Read() {
        final CardSwipeData cardSwipeData = CardSwipeData.parseSwipe(TRACK_2_READ);

        assertEquals("4111111111111111", cardSwipeData.cardNumber);
        assertEquals("08", cardSwipeData.expirationMonth);
        assertEquals("21", cardSwipeData.expirationYear);
        assertEquals(";4111111111111111=21080000000000000000?", cardSwipeData.track2);
        assertNull(cardSwipeData.cardHolderName);
        assertNull(cardSwipeData.track1);
    }
}