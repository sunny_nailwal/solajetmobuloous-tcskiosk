/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.solajet.tcskiosk.tcskiosk;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.solajet.tcskiosk.tcskiosk";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from build type: debug
  public static final String CARD_CONNECT_HOST_NAME = "fts.cardconnect.com:6443";
  public static final String CARD_CONNECT_MERCHANT_ID = "496160873888";
  public static final String CARD_CONNECT_PASSWORD = "testing123";
  public static final String CARD_CONNECT_USERNAME = "testing";
}
